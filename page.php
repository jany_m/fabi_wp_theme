<?php get_header();

global $wp_query;

/*echo get_current_template();
echo $wp_query->query['pagename'];*/

/*if (is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) :
    $col_content = 'col-9';
    $col_item = 'col-md-4 col-sm-6 col-12';
    $title_offset = 'offset-md-3';
else:
    $col_content = 'col-12';
    $col_item = 'col-md-3 col-sm-4 col-xs-2';
    $title_offset = 'center';
endif;*/

// Layout
$content_col = 12;
$sidebar_col = '';
if(is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) {
    $content_col = 9;
    $sidebar_col = 3;
}

/*echo 'xxx '.$wp_query->query['pagename'];
echo '<pre>';
//$queried_object = get_queried_object();
//print_r($queried_object);
var_dump($wp_query);
//echo '<br><br>'.$queried_object->post_name;
echo '</pre>';*/
?>

<main class="full-width landing-page">


    <?php

        if(is_page('video')) {

            // VIDEO SLIDER

            // VIDEO DA FABI.TV
            // http://fabitv.it/video/endpoint/json

            // API YOUTUBE
            // AIzaSyAM8TWHbMNjko1STrGYgcTUCJpnggq2-xE

            // OAUTH
            // Client ID
            // 440110863554-dma8i5lrodsi7b181prvj20sk073dluu.apps.googleusercontent.com
            // Secret
            // f0NwoYkLi8SV8K4VYCOMWGb-

            // YOUTUBE
            // https://www.youtube.com/user/SINDACATOFABI/videos
            // Channel ID: UCEkuqjoD3NVJac3OKTtKWuA

            // https://stackoverflow.com/questions/50097600/youtube-video-list-in-json-format

            $youtube_page_transient = THEME_SLUG.'_youtube_page_6h';
            //delete_transient($youtube_page_transient);

            if(is_null($youtube_json) || get_transient($youtube_page_transient) === false) {
            
                $video_sources = get_field('video_sources', 'options');

                //Youtube
                $google_api_data = get_field('google_api_wrap', 'options');
                $API_key    =  $google_api_data['google_youtube_api'];
                $channelID  = $video_sources['video_source_youtube'];
                $maxResults = '';
                $maxResultsNum = 50;
                if($maxResultsNum != '') {
                    $maxResults = '&maxResults='.$maxResultsNum;
                }
                $youtube_json = json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='.$channelID.$maxResults.'&key='.$API_key.''), TRUE)['items'];
                
                //Cache Results
                set_transient($youtube_page_transient, $youtube_json, 6 * HOUR_IN_SECONDS );
            }
            $youtube_json = get_transient($youtube_page_transient);       
            
            $youtube_list = array();
            $video = '';
            $x=-1;
            foreach($youtube_json as $video) {
                $x++;
                $youtube_list[$x]['ID'] = $video['id']['videoId'];
                //$youtube_list['date'] = $video['snippet']['publishedAt']; // 2019-01-08T15:36:46.000Z
                $youtube_list[$x]['timestamp'] = strtotime($video['snippet']['publishedAt']);
                $youtube_list[$x]['date'] = date_i18n( get_option( 'date_format' ), $youtube_list[$x]['timestamp']);
                $youtube_list[$x]['title'] = $video['snippet']['title'];
                $youtube_list[$x]['descr'] = $video['snippet']['description'];
                $youtube_list[$x]['thumb'] = $video['snippet']['thumbnails']['high']['url'];
                $youtube_list[$x]['url'] = 'https://www.youtube.com/watch?v='.$youtube_list[$x]['ID'];
                $youtube_list[$x]['source'] = 'youtube';
                
                if($maxResultsNum != '' && $x > $maxResults) break;
            }

            // Debug
            /*if(current_user_can('administrator')) {
                echo '<pre>';
                var_dump('https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='.$channelID.$maxResults.'&key='.$API_key);
                echo '</pre>';
            }*/
            // Debug
            /*if(current_user_can('administrator')) {
                echo '<pre>';
                var_dump($youtube_list);
                echo '</pre>';
            }*/

            // FABI list
            $maxResults = '';
            $maxResultsNum = '';
            if($maxResultsNum != '') {
                $maxResults = '?maxResults='.$maxResultsNum;
            }
            $fabitv_json = json_decode(file_get_contents($video_sources['video_source_json'].$maxResults), TRUE)['items'];
            
            // Debug
            /*if(current_user_can('administrator')) {
                echo '<pre>';
                echo '<br><strong>FABI</strong><br/>';
                //echo $video_sources['video_source_json'].$maxResults;
                var_dump($fabitv_json);
                echo '</pre>';
            }*/

            $fabi_list = array();
            $video = '';
            $x=0;
            foreach($fabitv_json as $video) {
                $x++;
                $fabi_list['ID'] = $video['id'];
                //$fabi_list['date'] = $video['publish_at']; // 2012-10-15 14:20:27
                $fabi_list[$x]['timestamp'] = strtotime($video['publish_at']);
                $fabi_list[$x]['date'] = date_i18n( get_option( 'date_format' ), $fabi_list[$x]['timestamp']);
                $fabi_list[$x]['title'] = $video['title'];
                $fabi_list[$x]['descr'] = $video['description'];
                $fabi_list[$x]['thumb'] = $video['video_poster'];
                $fabi_list[$x]['url'] = $video['video_url'][0];
                $fabi_list[$x]['source'] = 'fabitv';

                if($maxResultsNum != '' && $x > $maxResults) break;
            }

            $videoList = array();
            $videoList = array_merge($fabi_list, $youtube_list);

            // Sort video array by timestamp
            function sortArrayBy_Timestamp($a1, $a2){
                if ($a1['timestamp'] == $a2['timestamp']) return 0;
                return ($a1['timestamp'] > $a2['timestamp']) ? -1 : 1;
            }
            usort($videoList, "sortArrayBy_Timestamp");
            
            // Visualizza i video
            if (!empty($videoList)) : ?>

                <div class="fullwidth news-content">
                    <div class="container">
                        <div class="row">

                            <?php if(is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) : ?>
                                <!-- Sidebar -->
                                <div class="col-<?php echo $sidebar_col; ?> sidebar-sx">
                                    <h4 class="visible-xs sidebar_toggle">Menù della Pagina</h4>
                                    <div class="inner_sidebar">
                                        <?php if (is_active_sidebar('sidebar-sx')) : dynamic_sidebar('sidebar-sx'); endif; ?>
                                        <?php if (is_active_sidebar('sidebar-generale-sx')) : dynamic_sidebar('sidebar-generale-sx'); endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            
                            <div class="content col-<?php echo $content_col; ?>">
                                <div class="row">
                                    
                                    <!-- Contenuto -->
                                    <div class="col-12">

                                        <!-- Titolo & Riassunto -->
                                        <div class="col-md-12 top-heading">
                                            <h2><?php the_title(); ?></span></h2>
                                            <?php if(strlen($post->post_excerpt) > 0) {
                                                the_excerpt();
                                            } ?>
                                        </div>

                                        <?php
                                            $video = '';
                                            $z = 0;
                                            foreach($videoList as $video) {

                                                $z++;
                                                // Video
                                                $video_id = $video['ID'];
                                                $video_date = $video['date'];
                                                $video_title = $video['title'];
                                                $video_descr = $video['descr'];
                                                $video_thumb = $video['thumb'];
                                                $video_url = $video['url'];
                                                $video_source = $video['source'];
                                                if($video_source == 'youtube') {
                                                    $source = '<a href="https://www.youtube.com/user/SINDACATOFABI/videos" rel="nofollow" target="_blank">YouTube</a>';
                                                } else {
                                                    $source = '<a href="https://www.fabitv.it" rel="nofollow" target="_blank">FabiTV</a>';
                                                }
                                                ?>

                                                <?php if($z == 1) {  // solo il primo ?>

                                                    <!-- Latest Video -->
                                                        <div class="row">
                                                            <div class="col-md-6 video-single">
                                                                <div class="video-play">
                                                                    <a data-fancybox data-width="640" data-height="360" href="<?php echo $video_url; ?><?php if($video_source == 'youtube') : echo '&amp;rel=0'; endif; ?>">
                                                                        <?php echo wp_imager(640, 360, 1, 'img-fluid', true, $video_thumb); ?>
                                                                        <img class="play-btn" src="<?php echo THEME_URL.'/assets/img/'; ?>play-btn.png" alt="icon" />
                                                                    </a> 
                                                                </div>                           
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="video-single-text">
                                                                    <div class="date"><p><span><?php echo $video_date; ?> - <?php echo $source; ?></span></p></div>
                                                                    <h2><a data-fancybox data-width="640" data-height="360" href="<?php echo $video_url; ?>"><?php echo $video_title; ?></a></h2>
                                                                    <?php
                                                                        if (strlen($video_descr) > 501) {
                                                                            $maxLength = 500;
                                                                            $video_descr = substr($video_descr, 0, $maxLength);
                                                                        }
                                                                        echo strip_tags($video_descr);
                                                                    ?>
                                                                </div>                            
                                                            </div>
                                                        </div>

                                                <?php }
                                                
                                            }
                                        ?>
                                    
                                    </div>

                                    <div class="col-12 youtube_custom_block">
                                        
                                        <?php echo do_shortcode('[elfsight_youtube_gallery id="1"]'); ?>

                                    </div>

                                    <!-- Next Videos -->
                                    <div class="col-12 video-slides">
                                        <div class="row">
                                            <!-- <div class="owl-carousel owl-theme"> -->

                                                        <?php
                                                            $z = 0;
                                                            // Next Videos                                    
                                                            foreach($videoList as $video) {

                                                                $z++;
                                                                if($z == 1) continue;

                                                                // Video
                                                                $video_id = $video['ID'];
                                                                $video_date = $video['date'];
                                                                $video_title = $video['title'];
                                                                $video_descr = $video['descr'];
                                                                $video_thumb = $video['thumb'];
                                                                $video_url = $video['url'];
                                                                $video_source = $video['source'];
                                                                if($video_source == 'youtube') {
                                                                    $source = '<a href="https://www.youtube.com/user/SINDACATOFABI/videos" rel="nofollow" target="_blank">YouTube</a>';
                                                                } else {
                                                                    $source = '<a href="https://www.fabitv.it" rel="nofollow" target="_blank">FabiTV</a>';
                                                                }
                                                                ?>
                                                                
                                                                <div class="item col-md-3">
                                                                    <div class="video-single">
                                                                        <div class="video-thumb">
                                                                            <a data-fancybox data-width="640" data-height="360" href="<?php echo $video_url; ?><?php if($video_source == 'youtube') : echo '&amp;rel=0'; endif; ?>">
                                                                                <?php echo wp_imager(300, 150, 1, 'img-fluid', true, $video_thumb); ?>
                                                                                <img class="play-btn" src="<?php echo THEME_URL.'/assets/img/'; ?>play-btn.png" alt="icon" />
                                                                            </a>
                                                                        </div>
                                                                        <div class="video-single-text">
                                                                            <div class="date"><p><span><?php echo $video_date; ?> - <?php echo $source; ?></span></p></div>
                                                                            <h4><a data-fancybox data-width="640" data-height="360" href="<?php echo $video_url; ?>"><?php echo $video_title; ?></a></h4>
                                                                            <?php
                                                                                if (strlen($video_descr) > 201) {
                                                                                    $maxLength = 200;
                                                                                    $video_descr = substr($video_descr, 0, $maxLength);
                                                                                }
                                                                                echo '<p>'.strip_tags($video_descr).' &hellip;</p>';
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            <?php } // foreach
                                                        ?>
                                                        
                                            <!-- </div> -->
                                        </div>
                                    </div>
                                
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            <?php endif;


        } else {
        // PAGINA NORMALE
    ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="fullwidth breadcrumb-links">
        <div class="container">
            <nav aria-label="breadcrumb">
                <?php breadcrumbs(); ?>
            </nav>
        </div>
    </div>

    <div class="fullwidth about-content">
        <div class="container">
            <div class="row">

                <?php if(is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) : ?>
                    <!-- Sidebar -->
                    <div class="col-<?php echo $sidebar_col; ?> sidebar-sx">
                        <h4 class="visible-xs sidebar_toggle">Menù della Pagina</h4>
                        <div class="inner_sidebar">
                            <?php if (is_active_sidebar('sidebar-sx')) : dynamic_sidebar('sidebar-sx'); endif; ?>
                            <?php if (is_active_sidebar('sidebar-generale-sx')) : dynamic_sidebar('sidebar-generale-sx'); endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                
                <div class="content col-<?php echo $content_col; ?>">
                    <div class="row">

                        <!-- Titolo & Riassunto -->
                        <div class="col-md-12 top-heading">
                            <h2><?php the_title(); ?></span></h2>
                            <?php if(strlen($post->post_excerpt) > 0) {
                                the_excerpt();
                            } ?>
                            <?php if($date != '' || $citta != '') { ?>
                                <p class="date">Data Evento: <?php echo $date; ?> - Città: <?php echo $citta; ?></p>
                            <?php } ?>
                        </div>
                        
                        <!-- Contenuto -->
                        <div class="col-12">
                            <?php the_content(); ?>
                        </div>
                    
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php else : ?>

<p>Questo contenuto non esiste o non è più disponibile.</p>

<?php endif; ?>

    <?php } ?>

</main>
<?php get_footer(); ?>