<?php get_header();

global $wp_query;
$total_results = $wp_query->found_posts;

/*if (is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) :
    $col_content = 'col-9';
    $col_item = 'col-md-4 col-sm-6 col-12';
    $title_offset = 'offset-md-3';
else:
    $col_content = 'col-12';
    $col_item = 'col-md-3 col-sm-4 col-xs-2';
    $title_offset = 'center';
endif;*/

/*echo '<pre>';
//$queried_object = get_queried_object();
//print_r($queried_object);
var_dump($wp_query->query_vars['paged']);
//echo '<br><br>'.$queried_object->post_name;
echo '</pre>';*/
?>

<main class="full-width landing-page">

<div class="fullwidth breadcrumb-links">
    <div class="container">
        <nav aria-label="breadcrumb">
            <?php breadcrumbs(); ?>
        </nav>
    </div>
</div>

<div class="fullwidth about-content">
    <div class="container">
        <div class="row">

            <!-- Titolo & Riassunto -->
            <div class="col-md-12 top-heading">
                <h2><?php if (have_posts()) : ?><span class="results-number"><?php echo $total_results; ?> risultati per "<strong><?php echo get_search_query(); ?></strong>"</span><?php endif; ?></h2>
            </div>
                            
            <!-- Contenuto -->
            <div class="row">

                <?php if (have_posts()) : while (have_posts()) : the_post();

                    $terms = wp_get_post_terms(get_the_ID(), 'tipologia_comunicato', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );;
                    if ( $terms && ! is_wp_error( $terms ) ) :
                    $terms_array = array();
                    foreach ( $terms as $term ) {
                        $terms_array[] = $term;
                    }              
                    $the_terms = join( ", ", $terms_array );
                    endif;

                    // Termini della tassonomia tipologia_comunicato
                    $terms_comunicato = wp_get_post_terms(get_the_ID(), 'tipologia_comunicato', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );;
                    if ( $terms_comunicato && ! is_wp_error( $terms_comunicato ) ) :
                    $terms_comunicato_array = array();
                    foreach ( $terms_comunicato as $term_comunicato ) {
                        $terms_comunicato_array[] = $term_comunicato;
                    }              
                    $the_terms_comunicato = join( ", ", $terms_comunicato_array );
                    endif;

                    // Termini della tassonomia gruppi_bancari
                    $terms_banche = wp_get_post_terms(get_the_ID(), 'gruppi_bancari', array('orderby' => 'name', 'order' => 'ASC'));
                    if ( $terms_banche && ! is_wp_error( $terms_banche ) ) :
                    $terms_banche_array = array();
                    foreach ( $terms_banche as $term_banca ) {
                        if(is_wp_error($term_banca))
                            continue;
                        $terms_banche_array[] = '<a href="'.get_term_link($term_banca).'">'.$term_banca->name.'</a>';
                    }              
                    $the_terms_banche = join( ", ", $terms_banche_array );
                    $the_terms_banche = strtolower($the_terms_banche);
                    endif;

                    // Termini della tassonomia tipologia_soggetto
                    $terms_soggetti = wp_get_post_terms(get_the_ID(), 'tipologia_soggetto', array('orderby' => 'name', 'order' => 'ASC'));
                    if ( $terms_soggetti && ! is_wp_error( $terms_soggetti ) ) :
                    $terms_soggetti_array = array();
                    foreach ( $terms_soggetti as $term_soggetto ) {
                        if(is_wp_error($term_soggetto))
                            continue;
                        $terms_soggetti_array[] = '<a href="'.get_term_link($term_soggetto).'">'.$term_soggetto->name.'</a>';
                    }              
                    $the_terms_soggetti = join( ", ", $terms_banche_array );
                    //$the_terms_banche = strtolower($the_terms_banche);
                    endif;

                    // Join termini
                    if($the_terms_banche == '' && $the_terms_soggetti != '') {
                    $the_other_terms = $the_terms_soggetti;
                    } else {
                    $the_other_terms = $the_terms_banche;
                    }

                    // Logo Banca
                    $banca = get_the_terms(get_the_ID(), 'gruppi_bancari');
                    /*$banca_logo_id = get_term_meta($banca[0]->term_id, 'image', true);
                    $banca_image_data = wp_get_attachment_image_src( $banca_logo_id, 'full' );
                    $banca_logo = $banca_image_data[0];*/
                    $banca_logo = get_field('term_img', 'gruppi_bancari_'.$banca[0]->term_id);
                    
                    // File / URL
                    $upload = get_field('file_upload');
                    $file_url = get_field('file_url');
                    if($upload != '' && !is_null($upload)) {
                        $url = $upload;
                    } elseif($file_url != '' && !is_null($file_url)) {
                        $url = $file_url;
                    } else {
                        $url = get_the_permalink(get_the_ID());
                    }

                    // Has image
                    $has_image = false;
                    //if(has_post_thumbnail()) $has_image = true;
                    ?>

                    <div class="col-xs-12 col-6">
                        <div class="grid-bottom-single fullwidth">               
                            <div class="date">
                                <p>
                                    <span class="float-left">
                                        <?php echo $the_terms_comunicato; ?><?php if($the_other_terms != '') echo '<em class="banca"> - '.$the_other_terms.'</em>'; ?>
                                    </span>
                                    <span class="float-right"><?php the_time('j/m/Y'); ?></span>
                                </p>
                            </div>
                            <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>
                        </div>
                    </div>

                <?php endwhile; else : ?>
                <p>Nessun risultato.</p>
                <?php endif; ?>

            </div>

	        <!--Pagination-->
            <div class="col-12 pagination_wrapper">
                <?php the_posts_pagination( array(
                    'mid_size' => 2,
                    'prev_text' => '&laquo;',
                    'next_text' => '&raquo;',
                ) ); ?>
            </div>
            <!--End Pagination-->

            <?php wp_reset_query(); wp_reset_postdata(); ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>
