<?php get_header();

    global $wp_query;
    $queried_object = get_queried_object();

    if(is_post_type_archive()) {
        $page_term = '';
        $page_name = $queried_object->labels->name;
        //var_dump($queried_object);
    } else {
        $page_term = '<p>'.get_the_archive_title('','').'</p>'; // https://developer.wordpress.org/reference/functions/get_the_archive_title/
        $page_term = '<p>'.single_cat_title( '', false ).'</p>';
        $post_type = get_post_type();
        $page_name = get_post_type_object( $post_type )->labels->name;
        //var_dump($post_type_obj);
    }

    // DEBUG
    /*if(current_user_can('administrator')) {
        echo '<pre>';
        var_dump($wp_query);
        echo '</pre>';
    }*/

?>

<main class="full-width landing-page">

    <!-- ========================================================================================================
        Video
    ========================================================================================================= -->

    <?php
        // Check if values are cached, if not cache them
        $multimedia_video_transient = THEME_SLUG.'_multimedia_video_3h';
        delete_transient($multimedia_video_transient);

        if(get_transient($multimedia_video_transient) === false) {
            $multimedia_video_loop = new WP_Query(array(
                'posts_per_page' => 20,
                'post_type' => array('multimedia'),
                'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'tipologia_media',
                        'field'    => 'slug',
                        'terms'    => 'video',
                    ),
                ),
            ));
            //Cache Results
            set_transient($multimedia_video_transient, $multimedia_video_loop, 3 * HOUR_IN_SECONDS );
        }
        $multimedia_video_loop = get_transient($multimedia_video_transient);
        ?>

        <?php if ($multimedia_video_loop->have_posts()) : ?>

            <div class="fullwidth news-content">
                <div class="container">

                    <?php

                        // Latest Video
                        while ( $multimedia_video_loop->have_posts() ) : $multimedia_video_loop->the_post(); 
                                    
                            // Video
                            $video = get_field('multimedia_video');
                            $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                            //$video_id = custom_get_youtube_data($video)['video_id'];
                            ?>

                                <!-- Latest Video -->
                                <div class="video-bg">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="video-play">
                                                <iframe width="717" height="404" src="https://www.youtube-nocookie.com/embed/<?php echo $video_id; ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            </div>                           
                                        </div>
                                        <div class="col-md-4">
                                            <h2 class="mt-0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                            <p><?php echo custom_excerpt(50); ?></p>                             
                                        </div>
                                    </div>
                                </div>
                            <?php break; // solo il primo

                        endwhile;
                    ?>

                    <!-- Next Videos -->
                    <div class="video-slides"> 
                        <div class="row">
                            <div class="col-12">
                                <div class="owl-carousel owl-theme">

                                    <?php

                                        // Next Videos                                    
                                        while ( $multimedia_video_loop->have_posts() ) : $multimedia_video_loop->the_post(); 

                                            // Video
                                            $video = get_field('multimedia_video');
                                            $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                            //$video_id = custom_get_youtube_data($video)['video_id'];
                                            ?>

                                            <div class="item">
                                                <a class="thumb-wrap">
                                                    <div class="thumb-img">
                                                        <a data-fancybox href="<?php echo $video; ?>&amp;rel=0">
                                                            <?php echo wp_imager(162, 140, 1, 'img-fluid', true, $video_thumb); ?>
                                                            <!-- <img class="play-btn" src="<?php echo THEME_URL.'/assets/img/'; ?>play-btn.png" alt="icon" /> -->
                                                        </a>
                                                    </div>
                                                    <div class="thumb-content">
                                                        <!-- <h4><a data-fancybox href="<?php //echo $video; ?>&amp;rel=0"><?php //the_title(); ?></a></h4> -->
                                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                        <p><?php echo custom_excerpt(10); ?></p>
                                                    </div>
                                                </a>
                                            </div>

                                        <?php endwhile;
                                    ?>
                                    
                                </div>                                 
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        <?php endif; wp_reset_query(); wp_reset_postdata();    
    ?>

    <!-- ========================================================================================================
        Foto
    ========================================================================================================= -->

    <?php
        // Check if values are cached, if not cache them
        $multimedia_foto_transient = THEME_SLUG.'_multimedia_foto_3h';
        delete_transient($multimedia_foto_transient);

        if(get_transient($multimedia_foto_transient) === false) {
            $multimedia_foto_loop = new WP_Query(array(
                'posts_per_page' => 20,
                'post_type' => array('multimedia'),
                'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'tipologia_media',
                        'field'    => 'slug',
                        'terms'    => 'foto',
                    ),
                ),
            ));
            //Cache Results
            set_transient($multimedia_foto_transient, $multimedia_foto_loop, 3 * HOUR_IN_SECONDS );
        }
        $multimedia_foto_loop = get_transient($multimedia_foto_transient);
        ?>

        <?php if ($multimedia_foto_loop->have_posts()) : ?>

            <div class="fullwidth photo-gallery">
                <div class="container">

                    <?php
                        // Latest Gallery
                        while ( $multimedia_foto_loop->have_posts() ) : $multimedia_foto_loop->the_post(); 
                                    
                            // Galleria
                            $galleria = get_field('multimedia_foto');
                            echo '<pre>';
                            var_dump($galleria);
                            echo '</pre>';
                            ?>

                            <!-- Latest Gallery -->
                            <div class="video-slides news-gallery">
                                <div class="row">
                                    <div class="col-12">
                                        <h2><?php the_title(); ?></h2>
                                        <p><?php echo custom_excerpt(30); ?></p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="owl-carousel owl-theme gallery-slide">

                                            <div class="item">

                                                <div class="section_wrap">

                                                    <?php foreach($galleria as $photo) { ?>

                                                        <a class="fancybox" data-fancybox-group="gallery" href="<?php echo $foto->url; ?>">
                                                            <div class="gallery-img">
                                                                <?php echo wp_imager(164, 142, 1 , 'img-responsive', false); ?>   
                                                            </div>
                                                        </a>

                                                    <?php } ?>

                                                </div>

                                                <div class="section_wrap">                                           
                                                    <a class="fancybox" data-fancybox-group="gallery" href="<?php echo THEME_URL.'/assets/img/'; ?>gallery/8thimage.jpg">
                                                        <div class="gallery-img">
                                                            <img src="<?php echo THEME_URL.'/assets/img/'; ?>gallery/8thimage.jpg" class="img-responsive" alt="" />                        
                                                        </div>
                                                    </a>
                                                    <a class="fancybox" data-fancybox-group="gallery" href="<?php echo THEME_URL.'/assets/img/'; ?>gallery/9thimage.jpg">
                                                        <div class="gallery-img">
                                                            <img src="<?php echo THEME_URL.'/assets/img/'; ?>gallery/9thimage.jpg" class="img-responsive" alt="" />                     
                                                        </div>
                                                    </a>
                                                </div>

                                            </div>

                                            <!-- <div class="item">
                                                <div class="section_wrap">                                           
                                                    <a class="fancybox" data-fancybox-group="gallery" href="<?php echo THEME_URL.'/assets/img/'; ?>gallery/4thimage.jpg">
                                                        <div class="gallery-img">
                                                            <img src="<?php echo THEME_URL.'/assets/img/'; ?>gallery/4thimage.jpg" class="img-responsive" alt="" />                        
                                                        </div>
                                                    </a>
                                                    <a class="fancybox" data-fancybox-group="gallery" href="<?php echo THEME_URL.'/assets/img/'; ?>gallery/5thimage.jpg">
                                                        <div class="gallery-img">
                                                            <img src="<?php echo THEME_URL.'/assets/img/'; ?>gallery/5thimage.jpg" class="img-responsive" alt="" />                        
                                                        </div>
                                                    </a>
                                                </div>

                                                <div class="section_wrap">                                           
                                                    <a class="fancybox" data-fancybox-group="gallery" href="<?php echo THEME_URL.'/assets/img/'; ?>gallery/8thimage.jpg">
                                                        <div class="gallery-img">
                                                            <img src="<?php echo THEME_URL.'/assets/img/'; ?>gallery/8thimage.jpg" class="img-responsive" alt="" />                        
                                                        </div>
                                                    </a>
                                                    <a class="fancybox" data-fancybox-group="gallery" href="<?php echo THEME_URL.'/assets/img/'; ?>gallery/9thimage.jpg">
                                                        <div class="gallery-img">
                                                            <img src="<?php echo THEME_URL.'/assets/img/'; ?>gallery/9thimage.jpg" class="img-responsive" alt="" />                     
                                                        </div>
                                                    </a>
                                                </div>

                                            </div> -->

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php break; // solo il primo

                        endwhile;
                        ?>


                    <!-- <div class="video-slides news-slides">
                        <div class="row">
                            <div class="col-12">
                                <div class="owl-carousel owl-theme">
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="thumb-wrap">
                                            <div class="thumb-img">
                                                <img src="<?php echo THEME_URL.'/assets/img/'; ?>9thimage.jpg" alt="about" class="img-fluid" />
                                            </div>
                                            <div class="thumb-content">
                                                <h4>117° Consiglio Nazionale</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>                                 
                            </div>
                        </div>
                    </div> -->

                </div>
            </div>

        <?php endif; wp_reset_query(); wp_reset_postdata();    
    ?>

</main>

<?php get_footer(); ?>