<?php get_header();

    if(current_user_can('administrator')) {
        //echo get_current_template();
    }    

    global $wp_query, $comunicati_lista;
    
    $queried_object = get_queried_object();
    $queried_term = (isset($_GET['term']) ? $_GET['term'] : '');
    $queried_keyword = (isset($_GET['keyword']) ? $_GET['keyword'] : '');
    $queried_tax = $queried_object->taxonomy;
    $queried_tax_term = $queried_object->slug;

    /*echo '<pre>';
    //var_dump($queried_object);
    var_dump($wp_query);
    ////var_dump($post_type_obj);
    echo '</pre>';*/
    //echo get_queried_object()->name;
    $post_type_slug_of_archive = $queried_object->name;

    // Title
    if(isset($_GET['term'])) {
        //$post_type = get_post_type();
        $post_type_label = get_post_type_object( $post_type )->labels->name;
        $queried_tax_term_label = get_term_by( 'slug', $queried_tax_term, $queried_tax)->name;
        $queried_term_label = get_term_by( 'slug', $queried_term, 'tipologia_comunicato')->name;
        if($queried_term_label != '') $queried_term_label = ' - '.$queried_term_label;
        $page_name = $queried_tax_term_label.$queried_term_label;
    } elseif(is_page()) {
        $page_name = $queried_object->post_title;
    } elseif(is_post_type_archive()) {
        $page_term = '';
        $page_name .= $queried_object->labels->name;
        //$page_name .= ' - Regioni';
    } elseif(is_category()) {
        $page_name = single_cat_title( '', false );
        $cat_descr = category_description();
    } elseif(is_tax()) {
        $page_name = $queried_object->name;
    } elseif(is_archive()) {
        $page_name = get_the_archive_title('',''); // https://developer.wordpress.org/reference/functions/get_the_archive_title/
    }

    $has_read_more = false;

    // Banca URL
    $banca_url = get_field('banca_url', get_queried_object());

    // Layout
    $content_col = 12;
    $sidebar_col = '';
    if(is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) {
        $content_col = 9;
        $sidebar_col = 3;
    }

    // DEBUG
    /*if(current_user_can('administrator')) {
        echo '<pre>';
        var_dump($queried_object);
        echo '</pre>';
    }*/

?>

<main class="full-width">

<div class="fullwidth breadcrumb-links">
    <div class="container">
        <nav aria-label="breadcrumb">
            <?php breadcrumbs(); ?>
        </nav>
    </div>
</div>

<div class="fullwidth about-content">
    <div class="container">
        <div class="row">

            <?php if(is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) : ?>
                <!-- Sidebar -->
                <div class="col-<?php echo $sidebar_col; ?> sidebar-sx">
                    <h4 class="visible-xs sidebar_toggle">Menù della Pagina</h4>
                    <div class="inner_sidebar">
                        <?php if (is_active_sidebar('sidebar-sx')) : dynamic_sidebar('sidebar-sx'); endif; ?>
                        <?php if (is_active_sidebar('sidebar-generale-sx')) : dynamic_sidebar('sidebar-generale-sx'); endif; ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="col-<?php echo $content_col; ?> the_grid">
                    
                <!-- ========================================================================================================
                    Grid
                ========================================================================================================= -->
                <div class="grid-bottom fullwidth">
                    <div class="row align-items-stretch">

                        <!-- Titolo & Riassunto -->
                        <div class="fullwidth top-heading">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2><?php echo $page_name; ?></span></h2>
                                        <?php if(strlen($cat_descr) > 0) { ?>
                                            <p><?php $cat_descr; ?></p>
                                        <?php } ?>
                                        <?php if($banca_url !== '' && !is_null($banca_url)) { ?>
                                            <p><a href="<?php echo $banca_url; ?>" rel="nofollow" target="_blank"><i class="fal fa-link"></i> <?php echo $banca_url; ?></a></p>
                                        <?php } ?>
                                    </div> 
                                </div>
                            </div>
                        </div>

                        <?php

                                // The Loop
                                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                $query_args = array(
                                    'post_type' => array('post'),
                                    'category_name' => 'news',
                                    'posts_per_page' => 12,
                                    'paged' => $paged,
                                    //'orderby' => 'modified',
                                    //'order' => 'DESC',
                                    //'taxonomy' => 'gruppi_bancari',
                                );

                                // Check if values are cached, if not cache them
                                $all_news_transient = THEME_SLUG.'_risultati_comunicati_'.$queried_term.'_1g';
				                delete_transient($all_news_transient);
				                if(get_transient($all_news_transient) === false) {
		                            $all_news_query = new WP_Query($query_args);
					                //Cache Results
					                set_transient($all_news_transient, $all_news_query, 24 * HOUR_IN_SECONDS );
				                }
                                $all_news_query = get_transient($all_news_transient);
                
                                // Save Query to Temp query for pagination
                                global $custom_query;
                                $custom_query = $all_news_query;

                                if ($all_news_query->have_posts()) :  while ( $all_news_query->have_posts() ) : $all_news_query->the_post(); 

                                    $terms = wp_get_post_terms(get_the_ID(), 'tipologia_comunicato', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );;
                                    if ( $terms && ! is_wp_error( $terms ) ) :
                                        $terms_array = array();
                                        foreach ( $terms as $term ) {
                                            $terms_array[] = $term;
                                        }              
                                        $the_terms = join( ", ", $terms_array );
                                    endif;

                                    // File
                                    $upload = get_field('file_upload');
                                    $file_url = get_field('file_url');
                                    if($upload != '' && !is_null($upload)) {
                                        $url = $upload;
                                    } elseif($file_url != '' && !is_null($file_url)) {
                                        $url = $file_url;
                                    } else {
                                        $url = get_the_permalink(get_the_ID());
                                    }

                                    // Video
                                    if(get_post_type() == 'multimedia') {
                                        $video = get_field('multimedia_video');
                                        if($video != '') {
                                            $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                        }
                                    } elseif(get_post_type() == 'post') {
                                        $video = get_field('video_feat');
                                        if($video != '') {
                                            $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                        }
                                    }
                                    ?>

                                    <div class="col-md-4 col-sm-6 col-12">
                                        <div class="grid-bottom-single fullwidth">
                                            
                                            <?php //if($has_feat_image) { ?>
                                                <div class="grid-img">
                                                    <?php
                                                        // Replace feat img with video thumb if content is = ''
                                                        if($video != '') {
                                                            echo wp_imager(320, 218, 1, 'img-fluid', false, $video_thumb);
                                                        } else {
                                                            echo wp_imager(320, 218, 1, 'img-fluid');
                                                        }
                                                    ?>
                                                </div>
                                            <?php //} ?>

                                            <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>

                                            <?php if($has_read_more) { ?>
                                                <div class="anchor-div">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <span class="a-img"><i class="far fa-angle-right"></i></span>
                                                        <span class="a-text">approfondisci</span>
                                                    </a>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>

                                <?php endwhile; endif; ?>

                                <!--Pagination-->
                                <div class="col-12 pagination_wrapper">
                                    <?php custom_query_pagination('&laquo;','&raquo;'); ?>
                                </div>
                                <!--End Pagination-->

                                <?php wp_reset_query(); wp_reset_postdata(); ?>

                    </div>
                </div>
                <!-- ========================================================================================================
                    End of Grid
                ========================================================================================================= -->

                
            </div>

        </div>
    </div>
</div>

</main>
<?php get_footer(); ?>