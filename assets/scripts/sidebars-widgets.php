<?php

/* ---------------------------------------------------------------
 .----..-. .-.  .--.  .-.   .-..----. .-..-.  .-.
{ {__  | {_} | / {} \ |  `.'  || {}  }| | \ \/ /
.-._} }| { } |/  /\  \| |\ /| || {}  }| | / /\ \
`----' `-' `-'`-'  `-'`-' ` `-'`----' `-'`-'  `-'
SHAMBIX.COM	(c) 2018
---------------------------------------------------------------- */
if ( ! defined( 'ABSPATH' ) ) exit; // NO DIRECT ACCESS

/* ---------------------------------------------------------------------------
*
* SIDEBARS
*
--------------------------------------------------------------------------- */

function register_widgets_init() {

	register_sidebar(array(
        'name' => 'Home | Blocco Centrale',
        'id' => 'home-sidebar-centrale',
		'description' => 'Lo spazio centrale di blocchi immagine, sotto il Video Slider.',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
	));

    register_sidebar(array(
        'name' => 'Home | Desktop - Laterale DX',
        'id' => 'desktop_home-sidebar-basso-dx',
		'description' => 'Lo spazio laterale a destra, sotto il Video Slider. Contenuti per Desktop.',
        'before_widget' => '<div class="desktop home_sidebar_basso_dx d-none d-md-block">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
	));

	register_sidebar(array(
        'name' => 'Home | Mobile - Laterale DX',
        'id' => 'mobile_home-sidebar-basso-dx',
		'description' => 'Lo spazio laterale a destra, sotto il Video Slider. Contenuti per Mobile (da iPad in giù)',
        'before_widget' => '<div class="mobile home_sidebar_basso_dx d-xs-block d-sm-block d-md-none">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
	));

	register_sidebar(array(
        'name' => 'Home | Footer - Alto',
        'id' => 'home-footer-sidebar-alto',
		'description' => 'Gestione di pulsanti e contenuti subito prima del footer blu, solo in Homepage',
        'before_widget' => '<div class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
	));

	/*register_sidebar(array(
        'name' => 'Home | Footer - Basso',
        'id' => 'home-footer-sidebar-basso',
		'description' => 'Gestione di pulsanti e contenuti subito prima del footer blu. Tutte le pagine.',
        'before_widget' => '<div class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
	));*/

	register_sidebar(array(
        'name' => 'Footer | SX',
        'id' => 'footer-sx',
		'description' => 'Contenuto Footer sinistro',
        'before_widget' => '<div class="footer_sx">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
	));

	register_sidebar(array(
        'name' => 'Footer | DX',
        'id' => 'footer-dx',
		'description' => 'Contenuto Footer destro',
        'before_widget' => '<div class="footer_dx">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
	));

	register_sidebar(array(
        'name' => 'Sidebar Mista SX',
        'id' => 'sidebar-sx',
        'description' => 'NON METTERE NIENTE QUI DENTRO. Questa sidebar serve solo come riferimento di "location" per quelle ad hoc per ogni contenuto.',
        'before_widget' => '<div class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
	));

	register_sidebar(array(
        'name' => 'Sidebar Generale SX',
        'id' => 'sidebar-generale-sx',
        'description' => 'Sidebar fissa, presente in tutti i contenuti (sotto la Sidebar Mista SX), escluse le news, a sinistra',
        'before_widget' => '<div class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
	));

	/*register_sidebar(array(
        'name' => 'Sidebar News DX',
        'id' => 'sidebar-dx',
        'description' => 'Sidebar presente nelle news, a destra',
        'before_widget' => '<div class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget_title">',
        'after_title' => '</h4>',
	));*/

}
add_action('widgets_init', 'register_widgets_init');

/* ---------------------------------------------------------------------------
*
* WIDGETS
*
--------------------------------------------------------------------------- */

// Lista Post
class Shambix_Lista_Post_Widget extends WP_Widget {

    public function __construct() {
        $widget_ops = array('classname' => 'widget_all_cpt_posts', 'description' => __("Lista di tutti i post del Post Type."));
        parent::__construct('widget_all_cpt_posts', 'Shambix - Lista post', $widget_ops);
    }

    public function widget($args, $instance) {
        //$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( '' ) : $instance['title'], $instance, $this->id_base );
        $cpt = !empty($instance['cpt']) ? $instance['cpt'] : '';
        $exclude = ! empty( $instance['exclude'] ) ? $instance['exclude'] : '';

        $all_reg_cpts = get_post_types(array('exclude_from_search' => false, 'public' => true/* ,   '_builtin' => false */), 'objects');

        $args = array(
            'posts_per_page' => -1,
            'post_type' => $cpt,
            'post_status' => 'publish',
            'post_parent' => 0, // otherwise in children, submenu wont show up
            'orderby' => 'menu_order',
            'order' => 'ASC',
			'suppress_filters' => true,
			'post__not_in'	=> explode(",",$exclude)
        );

        $posts_array = new WP_Query($args);

        /* if(current_user_can('activate_plugins')) :
          echo '<pre>';
          //echo $wp_query->queried_object_id;
          //print_r($wp_query);
          //print_r(get_the_ID());
          //var_dump(get_queried_object());
          //var_dump($all_reg_cpts);
          echo '<strong>GET POSTS</strong><br>'; var_dump(get_posts($args)); echo '<br><br><br>------------------------------------------------------<br>';
          echo '<strong>WP QUERY</strong><br>'; var_dump($posts_array);
          echo '</pre>';
          endif; */

        $content;
        $content .='<div class="widget lista_post">';
        $content .='<ul>';

        foreach ($posts_array->posts as $post) {

            $content .='<li>';
            $content .='<a href="' . get_permalink($post->ID) . '">';
            $content .=$post->post_title;
            $content .='</a>';
            $content .="</li>";

        }
        $content .="</ul>";
        $content .="</div>";

        $output = '<div class="widget_list_post">' . $content . '</div>';
        echo $output;

        wp_reset_query();
        wp_reset_postdata();
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        //$instance['title'] = strip_tags($new_instance['title']);
        $instance['cpt'] = strip_tags($new_instance['cpt']);
        $instance['exclude'] = strip_tags($new_instance['exclude']);
        return $instance;
    }

    public function form($instance) {
        //Defaults
        $instance = wp_parse_args((array) $instance, array('title' => ''));
        //$title = esc_attr( $instance['title'] );
        $cpt = esc_attr($instance['cpt']);
        $exclude = esc_attr( $instance['exclude'] );
        ?>
        <!-- <p><label for="<?php //echo $this->get_field_id('title');  ?>"><?php // _e( 'Title:' );  ?></label>
        <input class="widefat" id="<?php //echo $this->get_field_id('title');  ?>" name="<?php //echo $this->get_field_name('title');  ?>" type="text" value="<?php //echo $title;  ?>" /></p> -->

        <label for="<?php echo $this->get_field_id('cpt'); ?>"><?php _e('Post Type'); ?></label><br />
        <select id="<?php echo $this->get_field_id('cpt'); ?>" name="<?php echo $this->get_field_name('cpt'); ?>" class="widefat">
            <option <?php selected($instance['cpt'], ''); ?> value="">-----</option>
        <?php foreach (get_post_types(array('exclude_from_search' => false, 'public' => true/* ,   '_builtin' => false */), 'objects') as $reg_cpts) { ?>
                <option <?php selected($instance['cpt'], $reg_cpts->name); ?> value="<?php echo $reg_cpts->name; ?>"><?php echo $reg_cpts->labels->singular_name; ?></option>
        <?php } ?>
        </select>
        <p>Lasciare vuoto se il Widget viene usato già dentro una sidebar di contenuti singoli, verrà usato il post type del contenuto stesso.</p>

		<p><label for="<?php echo $this->get_field_id('exclude');  ?>">ID Post da escludere (separati da virgola):</label>
        <input class="widefat" id="<?php echo $this->get_field_id('exclude');  ?>" name="<?php echo $this->get_field_name('exclude');  ?>" type="text" value="<?php echo $exclude;  ?>" /></p>

        <?php
    }

}

// List Custom Taxonomy Terms
class Shambix_List_Tax_Terms extends WP_Widget {

    function __construct() {
        $widget_ops = array(
            'classname' => 'widget_list_tax_terms widget',
            'description' => "Lista di termini di una tassonomia custom."
        );
		parent::__construct( 'widget_list_tax_terms', 'Shambix - List Taxonomy Terms', $widget_ops);
    }
    
	function widget( $args, $instance ) {
		global $post;
		extract($args);

		// Widget options
		if ( array_key_exists( 'title', $instance ) ) { 
			$title = apply_filters('widget_title', $instance['title'] ); // Title
		} else {
			$title = '';
		}
		if ( array_key_exists( 'taxonomy', $instance ) ) {
			$this_taxonomy = $instance['taxonomy']; // Taxonomy to show
		} else {
			$this_taxonomy = '';
		}
		$hierarchical = !empty( $instance['hierarchical'] ) ? '1' : '0';
		$inv_empty = !empty( $instance['empty'] ) ? '0' : '1'; // invert to go from UI's "show empty" to WP's "hide empty"
		$showcount = !empty( $instance['count'] ) ? '1' : '0';
		if( array_key_exists('orderby',$instance) ){
			$orderby = $instance['orderby'];
		}
		else{
			$orderby = 'count';
		}
		if( array_key_exists('ascdsc',$instance) ){
			$ascdsc = $instance['ascdsc'];
		}
		else{
			$ascdsc = 'desc';
		}
		if( array_key_exists('exclude',$instance) ){
			$exclude = $instance['exclude'];
		}
		else {
			$exclude = '';
		}
		if( array_key_exists('childof',$instance) ){
			$childof = $instance['childof'];
		}
		else {
			$childof = '';
        }
        if( array_key_exists('parent',$instance) ){
			$parent = $instance['parent'];
		}
		else {
			$parent = '';
		}
		if( array_key_exists('dropdown',$instance) ){
			$dropdown = $instance['dropdown'];
		}
		else {
			$dropdown = false;
		}
		// Dropdown doesn't work for built-in taxonomies.
		$builtin = array( 'post_tag', 'post_format', 'category' );
		if ( $dropdown && in_array( $this_taxonomy, $builtin ) ) {
			$dropdown = false;
		}
        // Output
		$tax = $this_taxonomy;
		echo $before_widget;

		if ( $title ) echo $before_title . $title . $after_title;

		echo '<div id="lct-widget-'.$tax.'-container" class="list-custom-taxonomy-widget">';
        
		if($dropdown){
			$taxonomy_object = get_taxonomy( $tax );
			if( in_array( $tax, array( 'category', 'post_tag', 'post_format' ) ) )
				$walker = '';
			else
				$walker = new Shambix_Tax_Terms_List_Walker();
			$args = array(
				'show_option_all'    => false,
				'show_option_none'   => '',
				'orderby'            => 'RANDOM()',//$orderby,
				'order'              => $ascdsc,
				'show_count'         => $showcount,
				'hide_empty'         => $inv_empty,
				'child_of'           => $childof,
				'exclude'            => $exclude,
				'echo'               => 1,
				//'selected'           => 0,
				'hierarchical'       => $hierarchical,
				'name'               => $taxonomy_object->query_var,
				'id'                 => 'lct-widget-'.$tax,
				//'class'              => 'postform',
				'depth'              => 0,
				//'tab_index'          => 0,
				'taxonomy'           => $tax,
				'hide_if_empty'      => true,
                'walker'			=> $walker,
                'parent'             => $parent,
                'childless'          => $childless,
			);
			echo '<form action="'. get_bloginfo('url'). '" method="get">';
            wp_dropdown_categories($args);
            
			echo '<input type="submit" value="go &raquo;" /></form>';
        
        } else {
			$args = array(
					'orderby'            => $orderby,
					'order'              => $ascdsc,
					'style'              => 'list',
					'count'              => $showcount,
					'hide_empty'         => $inv_empty,
					'child_of'           => $childof,
					//'feed'               => '',
					//'feed_type'          => '',
					//'feed_image'         => '',
					'exclude'            => $exclude,
					//'exclude_tree'       => '',
					//'include'            => '',
					'hierarchical'       => $hierarchical,
					'title_li'           => '',
					'show_option_none'   => 'No Categories',
					'number'             => null,
					//'echo'               => 1,
					//'depth'              => 0,
					//'pad_counts'         => 0,
					'taxonomy'           => $tax,
                    'parent'             => $parent,
                    'childless'          => $childless
				);
			echo '<ul id="lct-widget-'.$tax.'">';
            wp_list_categories($args);

            // Shambix
            /*$terms = get_terms($args);

            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
                <ul class="lista_termini">

                    <?php foreach($terms as $term) {
                        $term_link = get_term_link($term->term_id, $tax);
                        echo '<li class="cat-item">';
                        echo '<a href="'.$term_link.'">'.$term->name.'</a>';
                        echo '</li>';

                    } ?>

                </ul>
            <?php }*/        

			echo '</ul>';
		}
		echo '</div>';
		echo $after_widget;
	}

    function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title']  = strip_tags( $new_instance['title'] );
		$instance['taxonomy'] = strip_tags( $new_instance['taxonomy'] );
		$instance['orderby'] = $new_instance['orderby'];
		$instance['ascdsc'] = $new_instance['ascdsc'];
		$instance['exclude'] = $new_instance['exclude'];
		$instance['expandoptions'] = $new_instance['expandoptions'];
        $instance['childof'] = $new_instance['childof'];
        $instance['parent'] = $new_instance['parent'];
        $instance['childless'] = $new_instance['childless'];
		$instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
		$instance['empty'] = !empty($new_instance['empty']) ? 1 : 0;
        $instance['count'] = !empty($new_instance['count']) ? 1 : 0;
        $instance['dropdown'] = !empty($new_instance['dropdown']) ? 1 : 0;

		return $instance;
	}

	function form( $instance ) {
		//for showing/hiding advanced options; wordpress moves this script to where it needs to go
		wp_enqueue_script('jquery');
        ?>
        <script>
			jQuery(document).ready(function(){
				var status = jQuery('#<?php echo $this->get_field_id('expandoptions'); ?>').val();
				if ( status === 'expand' ) {
					jQuery('.lctw-expand-options').hide();
					jQuery('.lctw-all-options').show();
				} else {
					jQuery('.lctw-all-options').hide();
				}
			});
			function lctwExpand(id){
				jQuery('#' + id).val('expand');
				jQuery('.lctw-all-options').show(500); 
				jQuery('.lctw-expand-options').hide(500);
			}
			function lctwContract(id){
				jQuery('#' + id).val('contract');
				jQuery('.lctw-all-options').hide(500); 
				jQuery('.lctw-expand-options').show(500);
			}
        </script>
        <?php
		// instance exist? if not set defaults
		if ( $instance ) {
				$title  = $instance['title'];
				$this_taxonomy = $instance['taxonomy'];
				$orderby = $instance['orderby'];
				$ascdsc = $instance['ascdsc'];
				$exclude = $instance['exclude'];
				$expandoptions = $instance['expandoptions'];
                $childof = $instance['childof'];
                $parent = $instance['parent'];
				//$childless = $instance['childless'];
				$childless = isset($instance['childless']) ? (bool) $instance['childless'] :false;
                $showcount = isset($instance['count']) ? (bool) $instance['count'] :false;
                $hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
                $empty = isset( $instance['empty'] ) ? (bool) $instance['empty'] : false;
                $dropdown = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
		} else {
			    //These are our defaults
				$title  = '';
				$orderby  = 'count';
				$ascdsc  = 'desc';
				$exclude  = '';
				$expandoptions  = 'contract';
				$childof  = '';
				$this_taxonomy = 'category';//this will display the category taxonomy, which is used for normal, built-in posts
				$hierarchical = true;
				$showcount = true;
				$empty = false;
                $dropdown = false;
                $parent = '';
                $childless = true;
		}
			
		// The widget form ?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:' ); ?></label>
				<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php echo __( 'Select Taxonomy:' ); ?></label>
				<select name="<?php echo $this->get_field_name('taxonomy'); ?>" id="<?php echo $this->get_field_id('taxonomy'); ?>" class="widefat" style="height: auto;" size="4">
			<?php 
			$args=array(
			  'public'   => true,
			  '_builtin' => false //these are manually added to the array later
			); 
			$output = 'names'; // or objects
			$operator = 'and'; // 'and' or 'or'
			$taxonomies=get_taxonomies($args,$output,$operator); 
			$taxonomies[] = 'category';
			$taxonomies[] = 'post_tag';
			$taxonomies[] = 'post_format';
			foreach ($taxonomies as $taxonomy ) { ?>
				<option value="<?php echo $taxonomy; ?>" <?php if( $taxonomy == $this_taxonomy ) { echo 'selected="selected"'; } ?>><?php echo $taxonomy;?></option>
			<?php }	?>
			</select>
			</p>
			<h4 class="lctw-expand-options"><a href="javascript:void(0)" onclick="lctwExpand('<?php echo $this->get_field_id('expandoptions'); ?>')" >More Options...</a></h4>
			<div class="lctw-all-options">
				<h4 class="lctw-contract-options"><a href="javascript:void(0)" onclick="lctwContract('<?php echo $this->get_field_id('expandoptions'); ?>')" >Hide Extended Options</a></h4>
                <input type="hidden" value="<?php echo $expandoptions; ?>" id="<?php echo $this->get_field_id('expandoptions'); ?>" name="<?php echo $this->get_field_name('expandoptions'); ?>" />

                <?php // Shambix
                    //$parent = 0;
                    //$childless = false;
                ?>
                
                <p>
                    <label for="<?php echo $this->get_field_id('parent'); ?>">Parent Term ID (only list its direct children):</label>
                    <input id="<?php echo $this->get_field_id('parent'); ?>" name="<?php echo $this->get_field_name('parent'); ?>" type="text" value="<?php echo $parent; ?>" class="widefat" />
                </p>
                <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('childless'); ?>" name="<?php echo $this->get_field_name('childless'); ?>"<?php checked( $childless ); ?> />
                <label for="<?php echo $this->get_field_id('childless'); ?>"><?php _e( 'Only list Parent terms' ); ?></label><br />
				
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $showcount ); ?> />
				<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show Post Counts' ); ?></label><br />
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked( $hierarchical ); ?> />
				<label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show Hierarchy' ); ?></label><br/>
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('empty'); ?>" name="<?php echo $this->get_field_name('empty'); ?>"<?php checked( $empty ); ?> />
				<label for="<?php echo $this->get_field_id('empty'); ?>"><?php _e( 'Show Empty Terms' ); ?></label></p>
				
				<p>
					<label for="<?php echo $this->get_field_id('orderby'); ?>"><?php echo __( 'Order By:' ); ?></label>
					<select name="<?php echo $this->get_field_name('orderby'); ?>" id="<?php echo $this->get_field_id('orderby'); ?>" class="widefat" >
						<option value="ID" <?php if( $orderby == 'ID' ) { echo 'selected="selected"'; } ?>>ID</option>
						<option value="name" <?php if( $orderby == 'name' ) { echo 'selected="selected"'; } ?>>Name</option>
						<option value="slug" <?php if( $orderby == 'slug' ) { echo 'selected="selected"'; } ?>>Slug</option>
						<option value="count" <?php if( $orderby == 'count' ) { echo 'selected="selected"'; } ?>>Count</option>
						<option value="term_group" <?php if( $orderby == 'term_group' ) { echo 'selected="selected"'; } ?>>Term Group</option>
					</select>
				</p>
				<p>
					<label><input type="radio" name="<?php echo $this->get_field_name('ascdsc'); ?>" value="asc" <?php if( $ascdsc == 'asc' ) { echo 'checked'; } ?>/> Ascending</label><br/>
					<label><input type="radio" name="<?php echo $this->get_field_name('ascdsc'); ?>" value="desc" <?php if( $ascdsc == 'desc' ) { echo 'checked'; } ?>/> Descending</label>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('exclude'); ?>">Exclude (comma-separated list of ids to exclude)</label><br/>
					<input type="text" class="widefat" name="<?php echo $this->get_field_name('exclude'); ?>" value="<?php echo $exclude; ?>" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('exclude'); ?>">Only Show Children of (category id)</label><br/>
					<input type="text" class="widefat" name="<?php echo $this->get_field_name('childof'); ?>" value="<?php echo $childof; ?>" />
				</p>
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('dropdown'); ?>" name="<?php echo $this->get_field_name('dropdown'); ?>"<?php checked( $dropdown ); ?> />
				<label for="<?php echo $this->get_field_id('dropdown'); ?>"><?php _e( 'Display as Dropdown' ); ?></label></p>
			</div>
    <?php 
	}

}

/* Custom version of Walker_CategoryDropdown */
class Shambix_Tax_Terms_List_Walker extends Walker {
	var $tree_type = 'category';
	var $db_fields = array ( 'id' => 'term_id', 'parent' => 'parent' );

	function start_el( &$output, $term, $depth = 0, $args = array(), $current_object_id = 0 ) {
		$term = get_term( $term, $term->taxonomy );
		$term_slug = $term->slug;

		$text = str_repeat( '&nbsp;', $depth * 3 ) . $term->name;
		if ( $args['show_count'] ) {
			$text .= '&nbsp;('. $term->count .')';
		}

		$class_name = 'level-' . $depth;

		$output.= "\t" . '<option' . ' class="' . esc_attr( $class_name ) . '" value="' . esc_attr( $term_slug ) . '">' . esc_html( $text ) . '</option>' . "\n";
	}
}

// Strutture
class Shambix_Strutture extends WP_Widget {

    function __construct() {
        $widget_ops = array(
            'classname' => 'widget_fabi_strutture widget',
            'description' => "Lista delle Strutture Nazionali con Organico, Comunicati ed Eventi collegati"
        );
		parent::__construct( 'widget_fabi_strutture', 'FABI - Strutture Nazionali', $widget_ops);
    }
    
	function widget( $args, $instance ) {
		global $post;
		extract($args);

		// Widget options
		if ( array_key_exists( 'title', $instance ) ) { 
			$title = apply_filters('widget_title', $instance['title'] ); // Title
		} else {
			$title = '';
		}

		// Post Name
		//global $wp_query;
		$queried_object = get_queried_object();
		//var_dump($queried_object->post_title);
		$current = $queried_object->post_title;


        // Output

		echo $before_widget;

		echo '<div id="widget-strutture" class="list-custom-strutture-widget">';
        if ( $title ) echo $before_title . $title . $after_title;
        
		
        $strutture = new WP_Query(array(
            'posts_per_page' => -1,
            'post_type' => array('struttura'),    
			'post_status' => 'publish',
			'orderby' => 'menu_order',
			'order'   => 'ASC',
			'post_parent' => 0
            /*'tax_query' => array(
                array(
                    'taxonomy' => 'organizzazione',
                    'field'    => 'slug',
                    'terms'    => 'video',
                ),
            ),*/
        ));

		if ($strutture->have_posts()) : ?>

			<div class="menu-strutture-nazionali-container">
				<ul id="menu-strutture-nazionali" class="menu">
			
					<?php while ( $strutture->have_posts() ) : $strutture->the_post(); 

						//if($post->post_parent > 0) continue;
		
						$id = get_the_ID();
						//$name = '';
						$visibile = get_field('widget_struttura_nazionale');
						//var_dump($strutture);
						//echo 'ok';

						// Solo quelle collegate ad un'org
						if(has_term('', 'organizzazione_struttura') || $visibile) {

							// Quale org?
							$org = get_the_terms( $id, 'organizzazione_struttura' )[0]->slug;
							$active_class = '';
							if(get_the_title() == $current) {
								$active_class = 'current-menu-item';
							}
							?>
							
							<li id="menu-item-<?php echo $id; ?>" class="menu-item menu-item-type-post_type menu-item-object-struttura menu-item-<?php echo $id; ?> <?php echo $active_class; ?>">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<!-- </li> -->

							<?php

								if(!is_null($org) && !empty($org)) {

									// Organico
									$organico = new WP_Query(array(
										'posts_per_page' => 1,
										'post_type' => array('organico'),    
										'post_status' => 'publish',
										'tax_query' => array(
											array(
												'taxonomy' => 'organizzazione_struttura',
												'field'    => 'slug',
												'terms'    => $org,
											),
										),
									));

									if ($organico->have_posts()) : ?>

										<ul class="sub-menu">
											<li><a href="<?php the_permalink(); ?>?org=<?php echo $org; ?>&type=organico">Chi Siamo</a></li>
										</ul>

									<?php endif; wp_reset_query(); wp_reset_postdata();

									// Comunicati
									$comunicati = new WP_Query(array(
										'posts_per_page' => 1,
										'post_type' => array('comunicato'),    
										'post_status' => 'publish',
										'tax_query' => array(
											array(
												'taxonomy' => 'organizzazione_struttura',
												'field'    => 'slug',
												'terms'    => $org,
											),
										),
									));

									if ($comunicati->have_posts()) : ?>

										<ul class="sub-menu">
											<li><a href="<?php the_permalink(); ?>?org=<?php echo $org; ?>&type=comunicato">Comunicati</a></li>
										</ul>

									<?php endif; wp_reset_query(); wp_reset_postdata();

									// Sottopagine Struttura
									$sottopagine = new WP_Query(array(
										'post_type'      => 'struttura',
										'posts_per_page' => -1,
										'post_parent'    => $id,
										'order'          => 'ASC',
										'orderby'        => 'menu_order'
									));
									
									/*echo '<pre>';
									var_dump($sottopagine->posts);
									echo '</pre>';*/

									if(!empty($sottopagine->posts)) {
										foreach($sottopagine->posts as $subpage) { ?>
											<ul class="sub-menu">
												<li><a href="<?php echo get_the_permalink($subpage->ID); ?>"><?php echo get_the_title($subpage->ID); ?></a></li>
											</ul>
										<?php }
									}
									wp_reset_query(); wp_reset_postdata();

									// Termini Post, Comunicati e Pubblicazioni Associate
									$term_posts = get_field('struttura_nazionale_post', $id);
									$term_com = get_field('struttura_nazionale_comunicati', $id);
									$term_pub = get_field('struttura_nazionale_pubblicazioni', $id);

									if ($term_posts != '') : ?>

										<?php foreach($term_posts as $term) {
											/*echo '<pre>';
											var_dump($term);
											echo '</pre>';*/
											?>
											<ul class="sub-menu">
												<li><a href="<?php echo get_category_link($term->term_id); ?>"><?php echo $term->name; ?></a></li>
											</ul>
										<?php } ?>

									<?php endif;

									if ($term_com != '') : ?>

									<?php foreach($term_com as $term) {
										/*echo '<pre>';
										var_dump($term);
										echo '</pre>';*/
										?>
										<ul class="sub-menu">
											<li><a href="<?php echo get_category_link($term->term_id); ?>"><?php echo $term->name; ?></a></li>
										</ul>
									<?php } ?>

									<?php endif;

									if ($term_pub != '') : ?>

										<?php foreach($term_pub as $term) { ?>
											<ul class="sub-menu">
												<li><a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a></li>
											</ul>
										<?php } ?>

									<?php endif;
								
									// Eventi
									$eventi = new WP_Query(array(
										'posts_per_page' => 1,
										'post_type' => array('evento'),    
										'post_status' => 'publish',
										'tax_query' => array(
											array(
												'taxonomy' => 'organizzazione_struttura',
												'field'    => 'slug',
												'terms'    => $org,
											),
										),
									));

									if ($eventi->have_posts()) : ?>

										<ul class="sub-menu">
											<li><a href="<?php the_permalink(); ?>?org=<?php echo $org; ?>&type=evento">Eventi</a></li>
										</ul>

									<?php endif; wp_reset_query(); wp_reset_postdata();

									// Termini Multimnedia associati
									$term_multim = get_field('struttura_nazionale_multimedia', $id);

									if ($term_multim != '') : ?>

										<?php foreach($term_multim as $term) { ?>
											<ul class="sub-menu">
												<li><a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a></li>
											</ul>
										<?php } ?>

									<?php endif;
									

								}
							?>



							</li>

						<?php }
	   
					endwhile; ?>

				</ul>
			</div>
			
		<?php endif; wp_reset_query(); wp_reset_postdata();

		echo '</div>';
		echo $after_widget;
	}

    function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']  = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		// instance exist? if not set defaults
		if ( $instance ) {
				$title  = $instance['title'];
		} else {
			    //These are our defaults
				$title  = '';
		} ?>
		<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:' ); ?></label>
				<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
			</p>
		<?php 
	}

}

// Register Custom Widgets & Un-register default WP Widgets
function register_general_widgets() {
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Tag_Cloud');
    //unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_RSS');

    //register_widget('Shambix_Parents_Children_Widget');
    register_widget('Shambix_Lista_Post_Widget');
    register_widget('Shambix_List_Tax_Terms');
	//register_widget('Shambix_Feat_Image_Widget');
	register_widget('Shambix_Strutture');
}
add_action('widgets_init', 'register_general_widgets');