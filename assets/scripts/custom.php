<?php
/* ---------------------------------------------------------------
 .----..-. .-.  .--.  .-.   .-..----. .-..-.  .-.
{ {__  | {_} | / {} \ |  `.'  || {}  }| | \ \/ /
.-._} }| { } |/  /\  \| |\ /| || {}  }| | / /\ \
`----' `-' `-'`-'  `-'`-' ` `-'`----' `-'`-'  `-'
SHAMBIX.COM	(c) 2018
---------------------------------------------------------------- */
if ( ! defined( 'ABSPATH' ) ) exit; // NO DIRECT ACCESS

/* ---------------------------------------------------------------------------
*
* DEBUG
*
--------------------------------------------------------------------------- */

ini_set('log_errors', true);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
ini_set('error_reporting', E_ALL & ~(E_STRICT|E_NOTICE|E_WARNING|E_DEPRECATED));
ini_set('error_log', ABSPATH. '/error.log');

//define('WP_DEBUG_LOG', true);
if(current_user_can('administrator')) {
	define('WP_DEBUG', true);
	define('WP_DEBUG_DISPLAY', true);
	ini_set('display_errors', true);
	ini_set('display_startup_errors', true);
	/*define('SAVEQUERIES', true);
	define('SCRIPT_DEBUG', true);*/
} else {
	error_reporting(0);
	define('WP_DEBUG', false);
	define('WP_DEBUG_DISPLAY', false);
	define('SAVEQUERIES', false);
	define('SCRIPT_DEBUG', false);
	ini_set('display_errors', false);
	ini_set('display_startup_errors', false);
}
//define('WP_DEBUG_LOG', true);

/*if(current_user_can('administrator')) {
	global $wp_query, $post, $wp;
	echo '<pre>';
	var_dump($wp);
	echo '</pre>';
}*/

/* ---------------------------------------------------------------------------
*
* OPZIONI TEMA & SETUP
*
--------------------------------------------------------------------------- */

// Opzioni sito
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Variabili di Sistema',
		'menu_title'	=> 'Sistema',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'manage_options',
		'redirect'		=> true
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Configurazione URLs',
		'menu_title'	=> 'Area Riservata',
		'parent_slug'	=> 'theme-general-settings',
	));
}
// the_field('pag_login', 'option');
// the_field('pag_reg', 'option');
//the_field('pag_redirect', 'option');

// Setup costanti
define('GRUPPI_BANCARI', 'gruppi-bancari-e-banche'); // Pagina per archivio tassomia
define('AGENTI_RISC', 'agenti-della-riscossione'); // Pagina per archivio tassomia
define('FEDERAZIONI', 'federazioni-regionali');
define('COMUNICATI_NAZIONALI', 'comunicati-nazionali-abi');
define('EVENTI', 'evento');

// Setup contenuti sito - Per gestione layout e archivi
global $contenuti_con_img, $pagine_lista, $comunicati_lista, $archivi_lista;
$contenuti_con_img = array('post', 'pubblicazione', 'multimedia'); // post type
$comunicati_lista = array('gruppi_bancari', 'organizzazione_struttura', 'federazioni_regionali', 'agenti_riscossione', 'consulenti-finanziari', 'banca-italia', 'comunicati-nazionali-abi', 'comunicati-stampa', 'tipologia_soggetto', 'tipologia_comunicato'); // Tassonomie o Termini
// Tutti i termini della tassonomia "tipologia_comunicato" devono avere formato lista
/*$tipologia_comunicato_terms = get_terms( array(
    'taxonomy' => 'tipologia_comunicato',
    'hide_empty' => false,
));
foreach($tipologia_comunicato_terms as $term) {
	$comunicati_lista[] = $term->slug;
}*/
$pagine_lista = array('gruppi-bancari-e-banche', 'agenti-della-riscossione', 'federazioni-regionali'); // Pagine // c'era Rassegna Stampa prima
$archivi_lista = array('regione'); // Tassonomie o Termini

// Logo img
if(!defined('THEME_LOGO')) {
    define('THEME_LOGO', get_stylesheet_directory_uri()."/assets/img/logo.png");
}

/* ---------------------------------------------------------------------------
*
* DEFAULTS
*
* Non modificare da qui in poi, a meno che non si sia sicuri di quello che si sta facendo
*
--------------------------------------------------------------------------- */

// Theme defaults, Constants and Utils
require get_template_directory().'/assets/scripts/setup.php';

// Sidebars & Widgets
require get_template_directory().'/assets/scripts/sidebars-widgets.php';

// Necessary Plugins
if (is_admin()) {
    require_once THEME_PATH.'/assets/plugins/plugins.php';
}

// Libraries

// WP-Imager
// https://github.com/Jany-M/wp-imager
if(is_file(THEME_PATH.'/vendor/shambix/wp-imager/wp-imager.php') && file_exists(THEME_PATH.'/vendor/shambix/wp-imager/wp-imager.php')) {
	require_once THEME_PATH.'/vendor/shambix/wp-imager/wp-imager.php';
} elseif (is_file(THEME_PATH.'/assets/libs/wp-imager.php') && file_exists(THEME_PATH.'/assets/libs/wp-imager.php')) {
	require_once THEME_PATH.'/assets/libs/wp-imager.php';
} else {
	return new WP_Error( 'error_msg', 'E\' necessario installare la libreria WP-Imager, tramite composer o inclusione diretta.' );
}

// Bootstrap Walker
// https://wp-bootstrap.github.io/wp-bootstrap-navwalker/
if(is_file(THEME_PATH.'/vendor/wp-bootstrap/wp-bootstrap-navwalker/class-wp-bootstrap-navwalker.php') && file_exists(THEME_PATH.'/vendor/wp-bootstrap/wp-bootstrap-navwalker/class-wp-bootstrap-navwalker.php')) {
	require_once THEME_PATH.'/vendor/wp-bootstrap/wp-bootstrap-navwalker/class-wp-bootstrap-navwalker.php';
} elseif (is_file(THEME_PATH.'/assets/libs/class-wp-bootstrap-navwalker.php') && file_exists(THEME_PATH.'/assets/libs/class-wp-bootstrap-navwalker.php')) {
	require_once THEME_PATH.'/assets/libs/class-wp-bootstrap-navwalker.php';
} else {
	return new WP_Error( 'error_msg', 'E\' necessario installare la libreria WP Bootstrap Walker, tramite composer o inclusione diretta.' );
}

// Google Data API
if(is_file(THEME_PATH.'/vendor/autoload.php') && file_exists(THEME_PATH.'/vendor/autoload.php')) {
	require_once THEME_PATH.'/vendor/autoload.php';
}

// Jetpacks and Embeds
if ( ! isset( $content_width ) ) $content_width = '1270';

// Google Maps key for ACF
global $g_key;
$g_key = 'AIzaSyAM8TWHbMNjko1STrGYgcTUCJpnggq2-xE';
function acf_gmap_key( $api ){
	global $g_key;
	$api['key'] = $g_key;
	return $api;
}
add_filter('acf/fields/google_map/api', 'acf_gmap_key');
function acf_gmap_key_update() {
	global $g_key;
	acf_update_setting('google_api_key', $g_key);
}
add_action('acf/init', 'acf_gmap_key_update');

// Custom template per Image Widget
function image_widget_custom_template($template) {
    return get_template_directory() . '/templates/template-widget_image.php';
}
add_filter('sp_template_image-widget_widget.php', 'image_widget_custom_template');

// Custom template per pagine lista
function custom_template_pagine_lista($template) {
	global $wp_query, $pagine_lista, $archivi_lista;
	$tax = $wp_query->query_vars['taxonomy'];
	$term = $wp_query->query_vars['term'];
	$nome_page = get_queried_object()->post_name;
    if(in_array($nome_page, $pagine_lista) || is_category($pagine_lista) || in_array($tax, $archivi_lista) || in_array($term, $archivi_lista)) {
        $template_lista = locate_template( array( 'templates/template-lista_archivio.php' ) );
        if ( '' != $template_lista ) {
            return $template_lista ;
        }
    }
    return $template;
}
if(!is_admin()) {
    add_filter( 'template_include', 'custom_template_pagine_lista', 99 );
}

// Custom template per comunicati archivi
function custom_template_comunicati($template) {
	global $wp_query, $comunicati_lista;
	$tax = $wp_query->query_vars['taxonomy'];
	$term = $wp_query->query_vars['term'];
	$page_name = $wp_query->query['pagename'];
    if(in_array($tax, $comunicati_lista) || in_array($term, $comunicati_lista) || in_array($page_name, $comunicati_lista) || is_category($comunicati_lista) || isset($_GET['term'])) {
        $template_lista = locate_template( array( 'templates/template-comunicati_archivio.php' ) );
        if ( '' != $template_lista ) {
            return $template_lista ;
        }
    }
    return $template;
}
if(!is_admin()) {
    add_filter( 'template_include', 'custom_template_comunicati', 99 );
}

// Custom template per categorie
function ppp_categorie( $query ) {
	if(is_category()) {
		//set_query_var('paged', 1);
		set_query_var('posts_per_page', 21);
	}
}
add_action( 'pre_get_posts', 'ppp_categorie' );

// Custom order per eventi
function fabi_ordine_eventi( $query ) {
	if( is_admin() ) {
		return $query;
	}
	if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'evento' ) {
		$query->set('orderby', 'meta_value');	
		$query->set('meta_key', 'evento_inizio');	 
		$query->set('order', 'DESC'); 
	}
	return $query;
}
add_action( 'pre_get_posts', 'fabi_ordine_eventi' );

// Custom template per pag Video
/*function custom_template_video($template) {
	global $wp_query, $post;
	$page_name = $wp_query->query['pagename'];
	$video_page_array = array('video', 'media', 'tutti-i-video');
    if(is_page('video') || is_page($video_page_array) || in_array($page_name, $video_page_array)) {
        $template_video = locate_template( array( 'archive-multimedia.php' ) );
        if ( '' != $template_lista ) {
            return $template_lista ;
        }
    }
    return $template;
}
if(!is_admin()) {
    add_filter( 'template_include', 'custom_template_video', 99 );
}*/

/* ---------------------------------------------------------------------------
*
* MENUS
*
--------------------------------------------------------------------------- */

function custom_register_nav_menus(){
	register_nav_menus( array(
		'header-main' => 'Header | Menu Principale',
		'header-top'  => 'Header | Menu Secondario',
		'footer-blu-buttons'  => 'Home | Pre-Footer | Tre Pulsanti Blu',
		'footer-bottom'  => 'Pre-Footer | Bottom',
		'footer-sx'   => 'Footer | Sinistra',
	) );
}
add_action( 'after_setup_theme', 'custom_register_nav_menus', 0 );

/* ---------------------------------------------------------------------------
*
* ENQUEUES
*
--------------------------------------------------------------------------- */

function load_child_files() {

	// CSS
	wp_register_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), '4.1.3', 'all');
	wp_enqueue_style('bootstrap');
	wp_register_style('fontaswesome', '//pro.fontawesome.com/releases/v5.5.0/css/all.css', array(), '5.5.0', 'all');
	wp_enqueue_style('fontaswesome');

    // JS
    wp_register_script('modernizr_custom', get_stylesheet_directory_uri().'/assets/libs/modernizr-2.8.3-respond-1.4.2.min.js', array(), false, false);
    wp_enqueue_script('modernizr_custom');
    wp_deregister_script('jquery');
	wp_register_script('jquery', '//code.jquery.com/jquery-3.3.1.min.js', array(), '3.3.1', false);
	//wp_register_script('jquery', '//code.jquery.com/jquery-1.11.2.min.js', array(), '1.11.2', false);
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery_migrate', '//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.min.js', array('jquery'), '3.0.1', true);
    wp_enqueue_script('jquery_migrate');
    wp_enqueue_script('popper', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), '1.14.3', true);
    wp_enqueue_script('popper');
    wp_register_script('bootstrap_js', '//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array('popper'), '4.1.3', true);
	wp_enqueue_script('bootstrap_js');

	// Libraries
	//if(is_front_page() || is_home()) {
		// CSS
		wp_register_style('owl_carousel', get_stylesheet_directory_uri().'/assets/libs/owl-carousel/owl.carousel.min.css', array('bootstrap'), '2.2.0', 'all');
        wp_enqueue_style('owl_carousel');
        wp_register_style('owl_carousel_theme', get_stylesheet_directory_uri().'/assets/libs/owl-carousel/owl.theme.default.min.css', array('owl_carousel'), '2.2.0', 'all');
		wp_enqueue_style('owl_carousel_theme');
		wp_register_style('fancybox', get_stylesheet_directory_uri().'/assets/libs/fancybox/jquery.fancybox.min.css', array('bootstrap'), '3.3.5', 'all');
		wp_enqueue_style('fancybox');

		// JS
		wp_register_script('owl_carousel_js', get_stylesheet_directory_uri().'/assets/libs/owl-carousel/owl.carousel.min.js', array('jquery'), '2.2.0', true);
		wp_enqueue_script('owl_carousel_js');
		wp_register_script('fancybox_js', get_stylesheet_directory_uri().'/assets/libs/fancybox/jquery.fancybox.min.js', array('jquery'), '3.3.5', true);
    	wp_enqueue_script('fancybox_js');
	//}

	// THEME
    wp_register_style('fabi', get_stylesheet_directory_uri().'/assets/css/style.css', array('owl_carousel_theme'), rand(1,50), 'all');
    wp_enqueue_style('fabi');
	wp_register_style('fabi_responsive', get_stylesheet_directory_uri().'/assets/css/responsive.css', array('fabi'), rand(1,50), 'all');
	wp_enqueue_style('fabi_responsive');
}
if (!is_admin()) {
    add_action('wp_enqueue_scripts', 'load_child_files', 9999);
}

// Async & Defer
global $scripts_to_defer, $scripts_to_async, $scripts_origin;
$scripts_to_defer = array('bootstrap_js');
$scripts_to_async = array();
$scripts_origin = array('fontawesome');

/* ---------------------------------------------------------------------------
*
* FOOTER SCRIPTS
*
--------------------------------------------------------------------------- */

function custom_footer_scripts(){ ?>
	<script type="text/javascript">

		jQuery(function($) {
			"use strict";

			$(window).load(function(){
				
			});

			$(document).ready(function () {

				// Se iscritto logged-in...
				<?php
					global $logged_in;
					if($logged_in) {
						$nome = $_SESSION['nome'].' '.$_SESSION['cognome'];
						?>

						$('.hide_on_login').html('<p class="welcome">Ciao, <?php echo $nome;?>!</p>');

					<?php }
				?>

				// Menus & submenus
				$('.widget li').has('ul').addClass('menu-item-has-children');
				$('.widget li ul').addClass('sub-menu');

				// Menu Children - Strutture
				$('#widget-strutture li.menu-item-has-children').not( ".current-menu-item, .current-menu-ancestor" ).hover(function() {
					$(this).find('.sub-menu').slideToggle();
				});
				if ($('#widget-strutture  li').hasClass("current-menu-item")) {
					$('#widget-strutture .current-menu-item .sub-menu').show();
				};
				if ($('#widget-strutture  li').hasClass("current-menu-parent")) {
					$('#widget-strutture .current-menu-parent .sub-menu').show();
				};

				// Fancybox
				$('.fancybox').fancybox({
					autoSize: false,
					fitToView: false,
					width: '60%',
					maxWidth: 700
				});

				// Inline Popup
				$(".open_popup").fancybox({type: 'inline'});
				$(".open_ajax").fancybox({
					type: 'ajax',
				});

				// Mobile sidebar
				$(".sidebar_toggle").click(function(){
					$(".inner_sidebar").toggle();
				});
				
				<?php if(is_home() || is_front_page()) { ?>
					// Menu Pulsanti footer-blu-buttons - Rimuovere i wrapper
					$('.footer_alto .menu').wrap('<div class="row"></div>');
					$('.footer_alto .menu a').unwrap();
					$('.footer_alto .menu a').unwrap();
					$('.footer_alto .row a').wrap('<div class="col-xs-12 col-sm-12 col-md-4 col-12 pulsante-blu"></div>');
				<?php } ?>

				<?php if(is_single() || is_page()) { ?>
					// Add to Any - Replace print button
					//$('.a2a_button_printfriendly').html('<i class="fal fa-print"></i>');
					$('.a2a_button_printfriendly').html('<img src="<?php echo THEME_URL."/assets/img/"; ?>printer.png" alt="Stampa" />');
				<?php } ?>

				// OWL Carousel
				var owl = $('.main-silder');
				owl.owlCarousel({
					items: 1,
					loop: true,
					margin: 0,
					autoplay: true,
					autoplayTimeout: 5000,
					autoplayHoverPause: true,
					dots: true
				});

				var owl2 = $('#video-owl');
				owl2.owlCarousel({
					loop: true,
					margin: 15,
					autoplay: true,
					autoplayTimeout: 8000,
					autoplayHoverPause: true,
					responsiveClass: true,
					dots: false,
					responsive: {
						0: {
							items: 1,
						},
						768: {
							items: 3,
						}
					}
				});

				var owl2 = $('#video-owl').owlCarousel();
				$("#pt-back").click(function () {
					owl2.trigger('prev.owl.carousel');
				});

				$("#pt-next").click(function () {
					owl2.trigger('next.owl.carousel');
				});

				$('.gallery-slide').owlCarousel({
					loop: true,
					margin: 0,
					responsiveClass: true,
					responsive: {
						0: {
							items: 1,
							loop: false,
							nav: true
						},
						768: {
							items: 1,
							loop: false,
							nav: true
						},
						1000: {
							items: 1,
							nav: true,
							loop: false,
							margin: 20
						}
					}
				});

				$('.owl-carousel').owlCarousel({
					loop: true,
					margin: 10,
					responsiveClass: true,
					responsive: {
						0: {
							items: 1,
							loop: false,
							nav: true
						},
						576: {
							items: 1,
							loop: false,
							nav: true
						}, 
						720: {
							items: 2,
							loop: false,
							nav: true
						},
						1000: {
							items: 3,
							nav: true,
							loop: false,
							margin: 20
						}
					}
				});

			});

		});
	</script>

	
    <?php if(!is_user_logged_in() && !is_admin()) { ?>
        <!-- Inizio Codice ShinyStat -->
		<script type="text/javascript" src="//codicebusiness.shinystat.com/cgi-bin/getcod.cgi?USER=SITOFABI"></script>
		<noscript>
		<a href="http://www.shinystat.com/it/" target="_top">
		<img src="//www.shinystat.com/cgi-bin/shinystat.cgi?USER=SITOFABI" alt="Statistiche web" style="border:0px" /></a>
		</noscript>
		<!-- Fine Codice ShinyStat -->
    <?php } ?>
    

    <?php
}
if(!is_admin()) {
	add_action('wp_print_footer_scripts', 'custom_footer_scripts', 999);
}

/* ---------------------------------------------------------------------------
*
* CUSTOM FUNCTIONS
*
--------------------------------------------------------------------------- */

// Enable Links
add_filter( 'pre_option_link_manager_enabled', '__return_true' );

// Add description editor to Taxonomies
function add_form_fields_to_tax($term, $taxonomy){
    ?>
    <tr valign="top">
        <th scope="row">Descrizione</th>
        <td>
            <?php wp_editor(html_entity_decode($term->description), 'description', array('media_buttons' => false)); ?>
            <script>
                jQuery(window).ready(function(){
                    jQuery('label[for=description]').parent().parent().remove();
                });
            </script>
        </td>
    </tr>
    <?php
}
function add_form_fields_to_tax_init() {
	$taxonomies = array('federazioni_regionali', 'organizzazione_struttura');
	foreach($taxonomies as $tax) {
		add_action("{$tax}_edit_form_fields", 'add_form_fields_to_tax', 10, 2);
	}
}
add_action('init', 'add_form_fields_to_tax_init', 999);

// Remove tags on posts
function custom_unregister_tags() {
    unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}
add_action( 'init', 'custom_unregister_tags' );

// Get YouTube video thumb
function custom_get_youtube_data( $youtube_url ) {

	$url_vars = array('v');
	$get_youtube_id= parse_str( parse_url( $youtube_url, PHP_URL_QUERY ), $url_vars );
	$youtube_id = $url_vars['v'];

	$video = array();
	$video_thumbnail_url_string = 'http://img.youtube.com/vi/%s/%s';

	$video_check = wp_remote_head( 'https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v=' . $youtube_id );
	if ( 200 === wp_remote_retrieve_response_code( $video_check ) ) {
		$remote_headers               = wp_remote_head(
			sprintf(
				$video_thumbnail_url_string,
				$youtube_id,
				'maxresdefault.jpg'
			)
		);
		$video['video_thumbnail_url'] = ( 404 === wp_remote_retrieve_response_code( $remote_headers ) ) ?
			sprintf(
				$video_thumbnail_url_string,
				$youtube_id,
				'hqdefault.jpg'
			) :
			sprintf(
				$video_thumbnail_url_string,
				$youtube_id,
				'maxresdefault.jpg'
			);
		$video['video_url']           = 'https://www.youtube.com/watch?v=' . $youtube_id;
		$video['video_embed_url']     = 'https://www.youtube.com/embed/' . $youtube_id;
		$video['video_id'] = $youtube_id;
	}

	/*$video_thumbnail_url = $youtube_details['video_thumbnail_url'];
	$video_url           = $youtube_details['video_url'];
	$video_embed_url     = $video['video_embed_url'];
	$video_id			 = $video['video_id']*/
	
	return $video;
}

// Breadcrumbs
// https://gist.github.com/melissacabral/4032941
// http://www.html.it/articoli/breadcrumb-wordpress-senza-plugin/
function breadcrumbs() {
		global $post;

		$text['home']     = 'Home'; // text for the 'Home' link
		$text['category'] = 'Categoria "%s"'; // text for a category page
		$text['tax'] 	  = 'Archivio di "%s"'; // text for a taxonomy page
		$text['search']   = 'Risultati ricerca per "%s"'; // text for a search results page
		$text['tag']      = 'Post taggati "%s"'; // text for a tag page
		$text['author']   = 'Contenuti inseriti da %s'; // text for an author page
		$text['404']      = 'Errore 404'; // text for the 404 page

		$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
		$showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
		$delimiter   = ' / '; // delimiter between crumbs
		$before      = '<li class="active">'; // tag before the current crumb
		$after       = '</li>'; // tag after the current crumb

		$homeLink = get_bloginfo('url') . '/';
		$linkBefore = '<li typeof="v:Breadcrumb">';
		$linkAfter = '</li>';
		$linkAttr = ' rel="v:url" property="v:title"';
		$link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

		if (is_home() || is_front_page()) {

			if ($showOnHome == 1) echo '<ol class="breadcrumb"><a href="' . $homeLink . '">' . $text['home'] . '</a></ol>';

		} else {

			echo '<ol class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;

			if ( is_category() ) {
				$thisCat = get_category(get_query_var('cat'), false);
				if ($thisCat->parent != 0) {
					$cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
					$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
					$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
					echo $cats;
				}
				echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

			} elseif( is_tax() ){
				$thisCat = get_category(get_query_var('cat'), false);
				if ($thisCat->parent != 0) {
					$cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
					$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
					$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
					echo $cats;
				}
				echo $before . sprintf($text['tax'], single_cat_title('', false)) . $after;

			}elseif ( is_search() ) {
				echo $before . sprintf($text['search'], get_search_query()) . $after;

			} elseif ( is_day() ) {
				echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
				echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
				echo $before . get_the_time('d') . $after;

			} elseif ( is_month() ) {
				echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
				echo $before . get_the_time('F') . $after;

			} elseif ( is_year() ) {
				echo $before . get_the_time('Y') . $after;

			} elseif ( is_single() && !is_attachment() ) {
				if ( get_post_type() != 'post' ) {
					$post_type = get_post_type_object(get_post_type());
					$slug = $post_type->rewrite;
					printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
					if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
				} else {
					$cat = get_the_category(); $cat = $cat[0];
					$cats = get_category_parents($cat, TRUE, $delimiter);
					if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
					$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
					$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
					echo $cats;
					if ($showCurrent == 1) echo $before . get_the_title() . $after;
				}

			} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
				$post_type = get_post_type_object(get_post_type());
				echo $before . $post_type->labels->name . $after;

			} elseif ( is_attachment() ) {
				$parent = get_post($post->post_parent);
				$cat = get_the_category($parent->ID); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $delimiter);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
				printf($link, get_permalink($parent), $parent->post_title);
				if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

			} elseif ( is_page() && !$post->post_parent ) {
				if ($showCurrent == 1) echo $before . get_the_title() . $after;

			} elseif ( is_page() && $post->post_parent ) {
				$parent_id  = $post->post_parent;
				$breadcrumbs = array();
				while ($parent_id) {
					$page = get_page($parent_id);
					$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
					$parent_id  = $page->post_parent;
				}
				$breadcrumbs = array_reverse($breadcrumbs);
				for ($i = 0; $i < count($breadcrumbs); $i++) {
					echo $breadcrumbs[$i];
					if ($i != count($breadcrumbs)-1) echo $delimiter;
				}
				if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

			} elseif ( is_tag() ) {
				echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

			} elseif ( is_author() ) {
				global $author;
				$userdata = get_userdata($author);
				echo $before . sprintf($text['author'], $userdata->display_name) . $after;

			} elseif ( is_404() ) {
				echo $before . $text['404'] . $after;
			}

			if ( get_query_var('paged') ) {
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
				echo __('Page') . ' ' . get_query_var('paged');
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
			}

			echo '</ol>';

		}
}

// Custom Loop Pagination
if (!function_exists('custom_query_pagination')) {
	function custom_query_pagination($prev = 'Previous', $next = 'Next') {
		/* --------------------------------------------------------------------------
        If the query is on a STATIC FRONT page, then add before the query:
        $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
        otherwise use:
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        Then, inside your query add this argument:
        'paged' => $paged,
        Finally, once the query is done, add this:
		global $custom_query;
		$custom_query = $your_query_name;
        --------------------------------------------------------------------------- */
		global $custom_query;
		$big = 99999999;
		$pagination = paginate_links(array(
			'base' => str_replace($big, '%#%', get_pagenum_link($big)),
			'format' => '?page=%#%',
			'total' => $custom_query->max_num_pages,
			'current' => max(1, get_query_var('paged')),
			'show_all' => false,
			'end_size' => 2,
			'mid_size' => 3,
			'prev_next' => true,
			'prev_text' => __($prev, TEXT_DOMAIN),
			'next_text' => __($next, TEXT_DOMAIN),
			'type' => 'list'
		));
		$pagination = str_replace('page-numbers', 'pagination', $pagination);
		echo $pagination;
		// RESET THE TEMP QUERY
		$custom_query = NULL;
	}
}

// Search all public CPT
function my_pre_get_posts($query) {
	if( is_admin() ) 
		return;
	if( is_search() && $query->is_main_query() ) {
		$args = array(
			'public'   => true,
			'_builtin' => false
		);
		$post_types = get_post_types($args, 'names');
		$query->set('post_type', $post_types);
	} 
}
add_action( 'pre_get_posts', 'my_pre_get_posts' );

// Meta tags for icons
function fabi_head_icons() { ?>
	<link rel="icon" type="image/x-icon" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>fabi_favicon.png">
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo THEME_URL.'/assets/img/icons/'; ?>android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>favicon-16x16.png">
        <link rel="manifest" href="<?php echo THEME_URL.'/assets/img/icons/'; ?>manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo THEME_URL.'/assets/img/icons/'; ?>ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
<?php }
add_action('wp_head', 'fabi_head_icons', 999);

/* ---------------------------------------------------------------------------
*
* SHORTCODES
*
--------------------------------------------------------------------------- */

// Shortcode - Show Taxonomy Terms of Taxonomy - TODO
/*function shortcode_tax_terms($atts, $content = null ) {
    extract( shortcode_atts( array(
	    'tax' => 'category'
	), $atts ) );
	
	$terms = get_terms( array(
		'taxonomy' => $tax,
		'hide_empty' => false,
	) );

	// DEBUG
	echo '<pre>';
	var_dump($terms);
	echo '</pre>';
	
	return $output;
}
add_shortcode('lista_termini', 'shortcode_tax_terms');*/

// TODO

/*

STRUTTURE - news, eventi ecc
Agganciare CF al post, invece che al tax term di organizzazioni

BANCHE - comunicati ecc
Sono in comunicati... ma un term lo usano tutt

EVENTI - single

Agganciare nel menu strutture i comunicati relativi a news e gli eventi



*/