<?php
/* ---------------------------------------------------------------
 .----..-. .-.  .--.  .-.   .-..----. .-..-.  .-.
{ {__  | {_} | / {} \ |  `.'  || {}  }| | \ \/ /
.-._} }| { } |/  /\  \| |\ /| || {}  }| | / /\ \
`----' `-' `-'`-'  `-'`-' ` `-'`----' `-'`-'  `-'
SHAMBIX.COM	(c) 2018
---------------------------------------------------------------- */
if ( ! defined( 'ABSPATH' ) ) exit; // NO DIRECT ACCESS

/* ---------------------------------------------------------------------------
*
* NON MODIFICARE QUESTO FILE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*
* Per eventuali customizzazioni globali, usare il file custom.php
*
--------------------------------------------------------------------------- */

/* ---------------------------------------------------------------------------
*
* SETUP
*
--------------------------------------------------------------------------- */

// PATHS
if(!defined('THEME_URL')) {
	define('THEME_URL', get_template_directory_uri());
}
if(!defined('THEME_PATH')) {
	define('THEME_PATH', get_template_directory());
}

// THEME
$current_theme = wp_get_theme();
if ($current_theme->exists()) {
	// Current Theme Name
	if(!defined('THEME_NAME')) {
		define('THEME_NAME', $current_theme->get('Name'));
	}
	// Current Theme General Var
	if(!defined('THEME_SLUG')) {
		define('THEME_SLUG', strtolower($current_theme->get('Name')));
	}
	// Text Domain
	$text_domain = esc_html($current_theme->get('TextDomain'));
	if(!defined('TEXT_DOMAIN')) {
		define('TEXT_DOMAIN', $text_domain);
	}
	// Theme Parent
	$check_parent = $current_theme->get('Template');
	if(empty($check_parent)) $has_parent = false;
	if($has_parent) {
		// Parent Theme Name
		$parent_theme = wp_get_theme($check_parent);
		if(!defined('PARENT_THEME')) {
			define('PARENT_THEME', $parent_theme);
		}
	}
}

/* ---------------------------------------------------------------------------
*
* EXTRAS
*
--------------------------------------------------------------------------- */

function add_feat_imgs() {
	add_theme_support( 'post-thumbnails' );
}
add_action('init', 'add_feat_imgs');

// Remove all emoji BS
function disable_wp_emojicons() {
    // all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
      return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
      return array();
    }
}
add_filter( 'emoji_svg_url', '__return_false' );

// First, make sure Jetpack doesn't concatenate all its CSS
add_filter( 'jetpack_implode_frontend_css', '__return_false' );

// Then, remove each CSS file, one at a time
function remove_all_jp_css() {
    wp_deregister_style( 'AtD_style' ); // After the Deadline
    wp_deregister_style( 'jetpack_likes' ); // Likes
    wp_deregister_style( 'jetpack_related-posts' ); //Related Posts
    wp_deregister_style( 'jetpack-carousel' ); // Carousel
    wp_deregister_style( 'grunion.css' ); // Grunion contact form
    wp_deregister_style( 'the-neverending-homepage' ); // Infinite Scroll
    wp_deregister_style( 'infinity-twentyten' ); // Infinite Scroll - Twentyten Theme
    wp_deregister_style( 'infinity-twentyeleven' ); // Infinite Scroll - Twentyeleven Theme
    wp_deregister_style( 'infinity-twentytwelve' ); // Infinite Scroll - Twentytwelve Theme
    wp_deregister_style( 'noticons' ); // Notes
    wp_deregister_style( 'post-by-email' ); // Post by Email
    wp_deregister_style( 'publicize' ); // Publicize
    wp_deregister_style( 'sharedaddy' ); // Sharedaddy
    wp_deregister_style( 'sharing' ); // Sharedaddy Sharing
    wp_deregister_style( 'stats_reports_css' ); // Stats
    wp_deregister_style( 'jetpack-widgets' ); // Widgets
    wp_deregister_style( 'jetpack-slideshow' ); // Slideshows
    wp_deregister_style( 'presentations' ); // Presentation shortcode
    wp_deregister_style( 'jetpack-subscriptions' ); // Subscriptions
    wp_deregister_style( 'tiled-gallery' ); // Tiled Galleries
    wp_deregister_style( 'widget-conditions' ); // Widget Visibility
    wp_deregister_style( 'jetpack_display_posts_widget' ); // Display Posts Widget
    wp_deregister_style( 'gravatar-profile-widget' ); // Gravatar Widget
    wp_deregister_style( 'widget-grid-and-list' ); // Top Posts widget
	wp_deregister_style( 'jetpack-widgets' ); // Widgets
	wp_deregister_style( 'jetpack-widget-social-icons-styles' ); // Social Icons
}
add_action('wp_print_styles', 'remove_all_jp_css', 9999 );

function wp_starter_head_cleanup() {
      //remove_action( 'wp_head', 'feed_links_extra', 3 );
      remove_action( 'wp_head', 'feed_links', 2 );
      remove_action( 'wp_head', 'rsd_link' );
      remove_action( 'wp_head', 'wlwmanifest_link' );
      //remove_action( 'wp_head', 'index_rel_link' );
      remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
      remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
      remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
      remove_action( 'wp_head', 'wp_generator', 999 );
      remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
}
add_action('init', 'wp_starter_head_cleanup');

// remove WP version from RSS
function wp_rss_version() {
      return '';
}
add_filter('the_generator', 'wp_rss_version');

// Remove Comment Feed
function remove_comments_rss( $for_comments ) {
      return;
}
add_filter('post_comments_feed_link','remove_comments_rss');

// Remove wp version from scripts
/*function remove_wp_ver_css_js( $src ) {
      if(current_user_can('administrator'))
          return $src;
      if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
          $src = remove_query_arg( 'ver', $src );
      return $src;
}
add_filter( 'style_loader_src', 'remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'remove_wp_ver_css_js', 9999 );*/

// add classes to body based on custom taxonomy ('sections')
function add_classes_to_body($classes) {
    global $post;
	//$classes = array();
	// User
	$user_info = wp_get_current_user();
	$classes[] = implode(' ', $user_info->roles);
	// WPML
	if ( function_exists('icl_object_id') ) {
		$classes[] = ICL_LANGUAGE_CODE.' '.ICL_LANGUAGE_CODE.'lang';
	}
	// Post Name
	if (is_page() || is_single()) {
        $classes[] = $post->post_name;
	}
    // Prod Cats
    $section_terms = get_the_terms($post->ID, 'product_cat');
    if ($section_terms && !is_wp_error($section_terms)) {
		$classes[] = "is_term term";
        foreach ($section_terms as $term) {
            if ($term->parent > 0) {
                $parentTerm = get_term($term->parent, 'product_cat');
                $classes[] = "term-parent term-" . $parentTerm->slug;
            } else {
                $classes[] = 'term-' . $term->slug;
			}
			if ( count( get_term_children( $section_terms->term_id, 'product_cat' ) ) > 0 ) {
				$classes[] = 'has-child';
			} else {
				$classes[] = 'no-child';
			}
        }
	}
	// Tax
    if (is_tax()) {
        $qObj = get_queried_object();
        $parentTermId = $qObj->parent;
        if ($parentTermId) {
            $parentTerm = get_term($parentTermId);
            $classes[] = 'term-' . $parentTerm->slug;
        } else {
            $classes[] = 'term-' . $qObj->slug;
        }
	}
	// Custom template name
	if(get_current_template() != '') {
		$classes[] = get_current_template();
	}
    // MU
    /*$id = get_current_blog_id();
    $slug = strtolower(str_replace(' ', '-', trim(get_bloginfo('name'))));
    $classes[] = $slug;
	$classes[] = 'site-id-' . $id;*/
    return $classes;
}
add_filter('body_class', 'add_classes_to_body');

// Head Title meta
function shambix_title_meta() {
	global $wp_query;
	?>
	<title>
	<?php
	if (is_home() || is_front_page()) {
		bloginfo('name'); echo ' | '; bloginfo('description'); }
	elseif (!(is_404()) && (is_single()) || (is_page())) {
		the_title(); echo ' | '; bloginfo('name'); }
	elseif (function_exists('is_tag') && is_tag()) {
		_e('Tag', TEXT_DOMAIN); echo ' &quot;'; single_tag_title("", false); echo '&quot; | '; bloginfo('name');}
	elseif (is_archive()) {
		wp_title(''); echo ' | '; bloginfo('name'); }
	elseif (is_search()) {
		echo '&quot; '.wp_specialchars($wp_query->query['s']).' &quot; | '; bloginfo('name');}
	elseif (is_404()) {
		_e('Not Found: Error 404', TEXT_DOMAIN); }
	else {
		bloginfo('name'); bloginfo('name');}
	if ($wp_query->query_vars['paged'] > 1) {
		_e('Page', TEXT_DOMAIN); echo ' '.$wp_query->query_vars['paged'];
	}
	?>
	</title>
	<?php 
}
add_action('wp_head', 'shambix_title_meta', 1);

// Social meta tags for header
function shambix_social_meta() {
	// If YOAST isnt on
	if(!class_exists( 'WPSEO_Frontend' )) {
		
		global $wp_query, $post;
		$socials = get_field('socials_wrap','options');
		$img_fabi = $socials['social_img'];

		if(is_singular() && !is_page()) {
			$title = get_the_title().' | '.get_bloginfo('name');
			$content = get_the_excerpt();
			$post_id = get_the_ID();
			if(has_post_thumbnail($post_id)) {
				$img_id = get_post_thumbnail_id($post_id);
				$full_img_src = get_the_post_thumbnail_url($post_id,'full'); 
				$img = wp_imager(1200,630, 1, $class=null, $link=false, $full_img_src, $nohtml=true, $post_id=null, $bg_color=null, $original_url=null);
			} else {
				$img = wp_imager(1200,630, 1, $class=null, $link=false, $exturl = false, $nohtml=true, $post_id=null, $bg_color=null, $original_url=null);
			}
			$url = get_the_permalink($post->ID);
			$type = 'article';
		} elseif(is_page()) {
			$title = get_the_title().' | '.get_bloginfo('name');
			$content = get_bloginfo('description');
			$img = wp_imager(1200,630, 1, $class=null, $link=false, $img_fabi, $nohtml=true, $post_id=null, $bg_color=null, $original_url=null);
			$url = get_the_permalink($post->ID);
			$type = 'article';
		} elseif(is_post_type_archive()) {
			$obj_id = get_queried_object();
			$title =  $obj_id->label.' | '.get_bloginfo('name');
			$content = get_bloginfo('description');
			$img = wp_imager(1200,630, 1, $class=null, $link=false, $img_fabi, $nohtml=true, $post_id=null, $bg_color=null, $original_url=null);
			// Debug
			/*if(current_user_can('administrator')) {
				echo '<pre>';
				var_dump($obj_id);
				echo '</pre>';
			}*/
			$url = get_post_type_archive_link( $obj_id->name );
			$type = 'website';
		} elseif(is_archive()) {
			$obj_id = get_queried_object();
			$title =  $obj_id->name.' | '.get_bloginfo('name');
			$content = get_bloginfo('description');
			$img = wp_imager(1200,630, 1, $class=null, $link=false, $img_fabi, $nohtml=true, $post_id=null, $bg_color=null, $original_url=null);
			$url = get_term_link( $obj_id->term_id);
			$type = 'website';
			// Debug
			/*if(current_user_can('administrator')) {
				echo '<pre>';
				var_dump(get_term_link( $obj_id->term_id));
				echo '</pre>';
			}*/
		} else {
			$title = get_bloginfo('name');
			$content = get_bloginfo('description');
			$img = wp_imager(1200,630, 1, $class=null, $link=false, $img_fabi, $nohtml=true, $post_id=null, $bg_color=null, $original_url=null);
			$url = get_home_url();
			$type = 'website';
		}

		// Open Graph (FB + Others)
		?>
		<meta property="og:locale" content="it_IT">
		<meta property="og:site_name" content="<?php bloginfo('name'); ?>">
		<meta property="og:type" content="<?php echo $type; ?>">

		<meta property="og:title" content="<?php echo $title; ?>">
		<meta property="og:description" content="<?php echo $content; ?>">
		<meta property="og:image" content="<?php echo $img; ?>">
		<meta property="og:url" content="<?php echo $url; ?>">

		<meta property="fb:app_id" content="<?php echo FB_APP_ID; ?>"/>

		<?php
		// Twitter
		?>

		<meta name="twitter:card" content="summary_large_image">

		<?php
	}
}
add_action('wp_head', 'shambix_social_meta', 2);

// Async & Defer & Origin scripts
function add_defer_attribute($tag, $handle) {
	global $scripts_to_defer;
	//$scripts_to_defer = array('my-js-handle', 'another-handle');
	
	foreach($scripts_to_defer as $defer_script) {
	   if ($defer_script === $handle) {
		  return str_replace(' src', ' defer="defer" src', $tag);
	   }
	}
	return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);

function add_async_attribute($tag, $handle) {
	global $scripts_to_async;
	//$scripts_to_async = array('my-js-handle', 'another-handle');
	
	foreach($scripts_to_async as $async_script) {
	   if ($async_script === $handle) {
		  return str_replace(' src', ' async="async" src', $tag);
	   }
	}
	return $tag;
}
add_filter('script_loader_tag', 'add_async_attribute', 11, 2);

function add_origin_attribute($tag, $handle) {
	global $scripts_origin;
	//$scripts_origin = array('my-js-handle', 'another-handle');
	
	foreach($scripts_origin as $origin) {
	   if ($origin === $handle) {
		  return str_replace(' src', ' origin="anonymous" src', $tag);
	   }
	}
	return $tag;
}
add_filter('script_loader_tag', 'add_origin_attribute', 12, 2);

// Get custom template name
function var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}
add_filter( 'template_include', 'var_template_include', 1000 );

function get_current_template( $echo = false, $ext = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
	if( $echo )
		if($ext)
			echo $GLOBALS['current_theme_template'];
		else
			echo str_replace('.php', '', $GLOBALS['current_theme_template']);
	else
		if($ext)
			return $GLOBALS['current_theme_template'];
		else
			return str_replace('.php', '', $GLOBALS['current_theme_template']);
}

// Shortcode for siteurl
function siteurl_shortcode() {
	return get_bloginfo('url');
}
add_shortcode('siteurl','siteurl_shortcode');

// Custom Excerpt
if (!function_exists('custom_excerpt')) {
	function custom_excerpt($num, $after = ' &hellip;') {
		global $post;
		$limit = $num+1;
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		array_pop($excerpt);
		$excerpt = implode(" ", $excerpt).$after;
		$excerpt = strip_tags($excerpt);
		$excerpt = wpautop($excerpt, false);
		return $excerpt;
	}
}

// Custom Excerpt Advanced
function custom_content($num, $after = ' &hellip;') {
	global $post;
	$string = $post->post_excerpt;
	if($string == '') {
		$string = $post->post_content;
	}
	if (strlen($string) >= $num) {
		$string = strip_tags($string);
		$string = wpautop($string, false);
	    return substr($string, 0, $num).$after;
	} else {
		$string = strip_tags($string);
		$string = wpautop($string, false);
	    return $string;
	}
}

// Shorten any text
function shorten($string, $lenght) {
	if (strlen($string) >= $lenght) {
		//echo substr($string, 0, 10). " ... " . substr($string, -5); // this is in case you wanted to shorten, add ... and then the last 5 letters of the sentence eg. "This is a ...script"
		$string = strip_tags($string);
		$string = wpautop($string, false);
	    return substr($string, 0, $lenght)."&hellip;";
	} else {
		$string = strip_tags($string);
		$string = wpautop($string."&hellip;", false);
	    return $string;
	}
}

// WPML
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);

/* --------------------------------------------------------------------------------
*
* LOGIN SCREEN
*
-------------------------------------------------------------------------------- */
function custom_login_logo() {
	$login_img_w = '278px';
	$login_img_h = '150px';
	?>
	<style type="text/css">
		body.login div#login h1 a {
			background-image: url(<?php echo THEME_LOGO; ?>);
			background-size: auto auto;
			padding-bottom: 0px;
			width: <?php echo $login_img_w; ?>!important;
			height: <?php echo $login_img_h; ?>!important;
		}
		#login {
			padding: 3% 0 0;
		}
	</style>
<?php }
add_action('login_head', 'custom_login_logo');

// Custom Login form CSS
/*function wp_starter_login_css() {
    echo '<link rel="stylesheet" href="' . get_stylesheet_directory_uri() . '/library/css/login.css">';
}
add_action('login_head', 'wp_starter_login_css');*/

/* ---------------------------------------------------------------------------
*
* YOAST
*
--------------------------------------------------------------------------- */
// Remove HTML comments
// Plugin URI: https://wordpress.org/plugins/remove-yoast-seo-comments/
// Author URI: https://profiles.wordpress.org/lowest
class Remove_YOAST_Comments {
	private $debug_marker_removed = false;
	private $head_marker_removed = false;
	private $backup_plan_active = false;
	public function __construct() {
		add_action( 'init', array( $this, 'bundle' ), 1);
	}
	public function bundle() {
		if(defined( 'WPSEO_VERSION' )) {
			$debug_marker = ( version_compare( WPSEO_VERSION, '4.4', '>=' ) ) ? 'debug_mark' : 'debug_marker';
			// main function to unhook the debug msg
			if(class_exists( 'WPSEO_Frontend' ) && method_exists( 'WPSEO_Frontend', $debug_marker )) {
				remove_action( 'wpseo_head', array( WPSEO_Frontend::get_instance(), $debug_marker ) , 2);
				$this->debug_marker_removed = true;
				// also removes the end debug msg as of 5.9
				if(version_compare( WPSEO_VERSION, '5.9', '>=' )) $this->head_marker_removed = true;
			}
			// compatible solution for everything below 5.8
			if(class_exists( 'WPSEO_Frontend' ) && method_exists( 'WPSEO_Frontend', 'head' ) && version_compare( WPSEO_VERSION, '5.8', '<' )) {
				remove_action( 'wp_head', array( WPSEO_Frontend::get_instance(), 'head' ) , 1);
				add_action( 'wp_head', array($this, 'rewrite'), 1);
				$this->head_marker_removed = true;
			}
			// temp solution for all installations on 5.8
			if(version_compare( WPSEO_VERSION, '5.8', '==' )) {
				add_action('get_header', array( $this, 'buffer_header' ));
				add_action('wp_head', array( $this, 'buffer_head' ), 999);
				$this->head_marker_removed = true;
			}
			// backup solution
			if($this->operating_status() == 2) {
				add_action('get_header', array( $this, 'buffer_header' ));
				add_action('wp_head', array( $this, 'buffer_head' ), 999);
			}
		}
	}
	public function operating_status() {
		if($this->debug_marker_removed && $this->head_marker_removed) {
			return 1;
		} elseif(!$this->debug_marker_removed && $this->head_marker_removed || $this->debug_marker_removed && !$this->head_marker_removed) {
			return 2;
		} else {
			return 3;
		}
	}
	// compatible solution for everything below 5.8
	public function rewrite() {
		$rewrite = new ReflectionMethod( 'WPSEO_Frontend', 'head' );
		$filename = $rewrite->getFileName();
		$start_line = $rewrite->getStartLine();
		$end_line = $rewrite->getEndLine()-1;
		$length = $end_line - $start_line;
		$source = file( $filename );
		$body = implode( '', array_slice($source, $start_line, $length) );
		$body = preg_replace( '/echo \'\<\!(.*?)\n/', '', $body);
		eval($body);
	}
	// temp solution for all installations on 5.8, and also the backup solution in the future
	public function buffer_header() {
		ob_start(function ($o) {
			return preg_replace('/\n?<.*?yoast.*?>/mi','',$o);
		});
	}
	public function buffer_head() {
		ob_end_flush();
	}
}
new Remove_YOAST_Comments;