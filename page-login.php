<?php

/* ---------------------------------------------------------------
 .----..-. .-.  .--.  .-.   .-..----. .-..-.  .-.
{ {__  | {_} | / {} \ |  `.'  || {}  }| | \ \/ /
.-._} }| { } |/  /\  \| |\ /| || {}  }| | / /\ \
`----' `-' `-'`-'  `-'`-' ` `-'`----' `-'`-'  `-'

SHAMBIX.COM	(c) 2018

Theme Name: Fabi
Version: 1.0.0

Author: Jany Martelli @ Shambix
Web: http://www.shambix.com
E-Mail: info@shambix.com

Credits: Andrea Romaldo - Team informatico FABI

---------------------------------------------------------------- */

session_start();

// Globali
global  $logged_in,
        $nome,
        $cognome,
        $tipologia,
        $idnews;

// Sistema
// the_field('pag_login', 'option');
// the_field('pag_reg', 'option');
//the_field('pag_redirect', 'option');

// Check login
if($logged_in == true) {
    //header("location: ".$_SERVER["HTTP_REFERER"]);
    $esito == 'OK';
} elseif(!isset($_COOKIE['fabi_user_logged_in']) && $logged_in == false) {
    $logged_in = false;
    if($area_iscritti) {
        header("location: ".PAG_LOGIN);
    }
    echo "<!-- Cookie 'fabi_user_logged_in' NON presente -->";
} elseif(isset($_COOKIE['fabi_user_logged_in']) || $logged_in == true) {
    $logged_in = true;
    echo "<!-- Cookie 'fabi_user_logged_in' E' presente -->";
    //echo "Cookie is:  " . $_COOKIE['fabi_user_logged_in'];
}

//Recupero i dati inviati via post dalla Login Iscritti FABI https://www.ssl.fabi.it/iscritti/login_iscritti.asp
$verifica = "ERFGTRESNFKITF01FR";
$esito = $_POST['esito']; // OK iscritto - KO non iscritto che deve ritornare una pagina di errore.
$chk = $_POST['chk']; //(codice univoco di controllo alfanumerico che valida la provenienza dei dati='ERFGTRESNFKITF01FR')

// Verifica Chiave chk positiva
if ($logged_in || $verifica = $chk) {

    // Esito = OK
    if ($esito == 'OK') {

        $tipologia = $_POST['tipologia']; // per ora fisso = 0 per una eventuale differenziazione iscritto/dirigente
        $idnews = $_POST['idnews']; // per ora fisso = 0 per una eventuale differenziazione di news da far vedere
        $nome = $_POST['nome']; // il nome dell'iscrizione_iscritto
        $cognome = $_POST['cognome']; // il cognome dell'iscritto
        $href = $_POST['url_redirect'];

        //Salvo eventualmente i dati in una variabile di sessione per condividerli con alre pagine php
        $_SESSION['tipologia'] = $tipologia;
        $_SESSION['idnews'] = $idnews;
        $_SESSION['nome'] = $nome;
        $_SESSION['cognome'] = $cognome;
        
        // https://premium.wpmudev.org/blog/set-get-delete-cookies/
        // setcookie(name, value, expire, path, domain, secure, httponly);
        if(!isset($_COOKIE['fabi_user_logged_in'])) {
            setcookie('fabi_user_logged_in', 'loggato', 30 * DAY_IN_SECONDS); 
        }

        $logged_in = true;
        $_SESSION['logged_in'] = true;
        if($href == '') {
            $href = REDIRECT_AREA_RISERVATA;
        }

        $success_msg = "Accesso effettuato correttamente da " . $_SESSION['nome'] . "  " . $_SESSION['cognome'];

        header( "refresh:3;url=".$href);
        echo '<img style="clear:both; text-align:center; margin:10px auto; display:block; max-width:200px" src="'.get_stylesheet_directory_uri().'/assets/img/logo.png" />';
        wp_die( '<p style="width:100%; display: block; text-align:center; margin-top:30px;"><b style="padding:10px 0; font-size:20px; margin-bottom: 30px;">'.$success_msg.'</b><br>Vi stiamo reindirizzando alla home.</p><br /><br />');    

    // Esito = KO
    } else {

        // TOGLIERE DOPO TEST
        // https://premium.wpmudev.org/blog/set-get-delete-cookies/
        // setcookie(name, value, expire, path, domain, secure, httponly);
        /*if(!isset($_COOKIE['fabi_user_logged_in'])) {
            setcookie('fabi_user_logged_in', 'loggato', 30 * DAY_IN_SECONDS); 
        }*/

        $error_msg = "Iscritto non registrato." ;
        $error_code = 'KO';

        header( "refresh:5;url=".PAG_REG);
        echo '<img style="clear:both; text-align:center; margin:10px auto; display:block; max-width:200px" src="'.get_stylesheet_directory_uri().'/assets/img/logo.png" />';
        wp_die( '<p style="width:100%; display: block; text-align:center; margin-top:30px;"><b style="padding:10px 0; font-size:20px; margin-bottom: 30px;">'.$error_msg.'</b><br />Per visualizzare questo contenuto, è necessario <a href="'.PAG_REG.'">registrarsi al portale FABI</a> o attendere di essere reindirizzato alla pagina di registrazione, tra 5 secondi.</p><br /><br /><pre style=text-align:center; ">Codice Errore: '.$error_code.'</pre>');

    }

// Verifica Chiave chk negativa
} else {

    $error_msg = "Utente non autorizzato ad accedere alla pagina." ;
    $error_code = 'NO CHK';

    header( "refresh:5;url=".PAG_LOGIN);
	echo '<img style="clear:both; text-align:center; margin:10px auto; display:block; max-width:200px" src="'.get_stylesheet_directory_uri().'/assets/img/logo.png" />';
    wp_die( '<p style="width:100%; display: block; text-align:center; margin-top:30px;"><b style="padding:10px 0; font-size:20px;">'.$error_msg.'</b><br />Effettuare il <a href="'.PAG_LOGIN.'">login qui</a> o attendere di esservi reindirizzato tra 5 secondi.</p><br /><br /><pre style=text-align:center; ">Codice Errore: '.$error_code.'</pre>');

}

?>