<?php if(is_singular('post')) {
    
    $excerpt_lenght = 15;
    ?>

    <main class="full-width landing-page">
        
        <!-- ========================================================================================================
        Tabs
        ========================================================================================================= -->
        <div class="tab-section fullwidth home_tabs">
            <div class="container">

                <ul class="nav tab-ul fullwidth">
                    <li>
                        <a class="active" data-toggle="tab" href="#tab-1">Ultimissime</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab-2">Comunicati</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab-3">Appuntamenti</a>
                    </li>
                </ul>

                <div class="tab-content fullwidth">
                    
                    <!-- Ultimissime -->
                    <div class="tab-pane fade show active fullwidth ultimissime" id="tab-1">    
                        <div class="row"> 
                        
                            <?php
                                // ULTIMISSIME

                                // Check if values are cached, if not cache them
                                $home_tabs_1_transient = THEME_SLUG.'_home_tabs_1_3h';
                                delete_transient($home_tabs_1_transient);

                                if(get_transient($home_tabs_1_transient) === false) {
                                    $home_tabs_1_loop = new WP_Query(array(
                                        'posts_per_page' => 3,
                                        'post_type' => array('pubblicazione', 'post', 'multimedia'), // ma SOLO comunicati FABI
                                        'post_status' => 'publish',
                                    ));
                                    //Cache Results
                                    set_transient($home_tabs_1_transient, $home_tabs_1_loop, 3 * HOUR_IN_SECONDS );
                                }
                                $home_tabs_1_loop = get_transient($home_tabs_1_transient);
                                ?>

                                <?php if ($home_tabs_1_loop->have_posts()) : while ( $home_tabs_1_loop->have_posts() ) : $home_tabs_1_loop->the_post();
                                        
                                    // Post Data
                                    $taxonomy = get_post_taxonomies();
                                    foreach ($taxonomy as $tax ) {
                                        $terms = get_the_terms( get_the_ID(), $tax);
                                    }
                                    if ( $terms && ! is_wp_error( $terms ) ) :
                                        $terms_array = array();
                                        foreach ( $terms as $term ) {
                                            $term_link = get_term_link($term->term_id);
                                            $terms_array[] = '<a href="'.$term_link.'">'.$term->name.'</a>';
                                        }          
                                    endif;    
                                    $the_terms = join( ", ", $terms_array );

                                    // Video
                                    if(get_post_type() == 'multimedia') {
                                        $video = get_field('multimedia_video');
                                        if($video != '') {
                                            $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                        }
                                    } elseif(get_post_type() == 'post') {
                                        $video = get_field('video_feat');
                                        if($video != '') {
                                            $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                        }
                                    }

                                    // Image
                                    $image = wp_imager(570, 350, 1, 'img-fluid', false, false, true);
                                    //var_dump($image);

                                    ?>

                                    <div class="col-xs-6 col-sm-6 col-md-4 col-12 item">

                                            <div class="tab-img-div fullwidth">
                                                <?php
                                                    // Replace feat img with video thumb if content is = ''
                                                    if($video_thumb != '') {
                                                        echo wp_imager(570, 350, 1, 'img-fluid', false, $video_thumb);
                                                    } elseif($image != '' || !is_null($image)) {
                                                        echo wp_imager(570, 350, 1, 'img-fluid');
                                                    } else {

                                                    }
                                                ?>
                                            </div>

                                            <div class="date"><p><span><?php the_time('j/m/Y'); ?></span> | <?php echo $the_terms; ?></p></div>
                                            <h2>
                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                            </h2>
                                            <?php if(strlen(get_the_content()) > 0) { ?>
                                                <?php echo custom_excerpt($excerpt_lenght); ?>
                                            <?php } ?>
                                            <div class="anchor-div">
                                                <a href="<?php the_permalink(); ?>">
                                                    <span class="a-img"><i class="far fa-angle-right"></i></span>
                                                    <span class="a-text">approfondisci</span>
                                                </a>
                                            </div>
                                    </div>
                                <?php endwhile; endif; wp_reset_query(); wp_reset_postdata(); 
                                    
                            ?>

                        </div>

                        <!-- <div class="text-center fullwidth my-3">
                            <a href="#" class="tab-bottom-btn">Leggi tutte</a>
                        </div> -->

                    </div>
                    
                    <!-- Comunicati -->
                    <div class="tab-pane fade comunicati" id="tab-2">
                        <div class="row">
                            
                            <?php
                                // COMUNICATI

                                // Check if values are cached, if not cache them
                                $home_tabs_2_transient = THEME_SLUG.'_home_tabs_2_3h';
                                delete_transient($home_tabs_2_transient);

                                if(get_transient($home_tabs_2_transient) === false) {
                                    $home_tabs_2_loop = new WP_Query(array(
                                        'posts_per_page' => 6,
                                        'post_type' => array('comunicato'),    
                                        'post_status' => 'publish',
                                        /*'tax_query' => array(
                                            array(
                                                'taxonomy' => 'tipologia_comunicato',
                                                'field'    => 'slug',
                                                'terms'    => 'stampa',
                                            ),
                                        ),*/
                                    ));
                                    //Cache Results
                                    set_transient($home_tabs_2_transient, $home_tabs_2_loop, 3 * HOUR_IN_SECONDS );
                                }
                                $home_tabs_2_loop = get_transient($home_tabs_2_transient);
                                ?>

                                <?php if ($home_tabs_2_loop->have_posts()) : while ( $home_tabs_2_loop->have_posts() ) : $home_tabs_2_loop->the_post();
                                        
                                    // Post Data
                                    $taxonomy = get_post_taxonomies();
                                    foreach ($taxonomy as $tax ) {
                                        $terms = get_the_terms( get_the_ID(), $tax);
                                    }
                                    if ( $terms && ! is_wp_error( $terms ) ) :
                                        $terms_array = array();
                                        foreach ( $terms as $term ) {
                                            $term_link = get_term_link($term->term_id);
                                            $terms_array[] = '<a href="'.$term_link.'">'.$term->name.'</a>';
                                        }          
                                    endif;    
                                    $the_terms = join( ", ", $terms_array );

                                    // File
                                    $upload = get_field('file_upload');
                                    $file_url = get_field('file_url');
                                    if($upload != '' && !is_null($upload)) {
                                        $url = $upload;
                                    } elseif($file_url != '' && !is_null($file_url)) {
                                        $url = $file_url;
                                    } else {
                                        $url = get_the_permalink(get_the_ID());
                                    }
                                    ?>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-12 item">
                                        <div class="date"><p><span><?php the_time('j/m/Y'); ?></span> | <?php echo $the_terms; ?></p></div>
                                        <h2>
                                            <a href="<?php echo $url; ?>"><?php the_title(); ?></a>
                                        </h2>
                                        <!-- <p><?php //echo custom_excerpt($excerpt_lenght, ''); ?></p> -->
                                        <!-- <div class="anchor-div">
                                            <a href="<?php //echo $url; ?>">
                                                <span class="a-img"><i class="far fa-angle-right"></i></span>
                                                <span class="a-text">approfondisci</span>
                                            </a>
                                        </div> -->
                                    </div>
                                <?php endwhile; endif; wp_reset_query(); wp_reset_postdata(); 
                                    
                            ?>

                        </div>

                        <div class="text-center fullwidth my-3">
                            <a href="<?php bloginfo('url'); ?>/comunicato" class="tab-bottom-btn">Vedi tutti</a>
                        </div>

                    </div>
                    
                    <!-- Appuntamenti -->
                    <div class="tab-pane fade appuntamenti" id="tab-3">
                        <div class="row">
                            
                            <?php
                                // EVENTI

                                // Check if values are cached, if not cache them
                                $home_tabs_3_transient = THEME_SLUG.'_home_tabs_3_3h';
                                delete_transient($home_tabs_3_transient);

                                if(get_transient($home_tabs_3_transient) === false) {
                                    $home_tabs_3_loop = new WP_Query(array(
                                        'posts_per_page' => 6,
                                        'post_type' => array('evento'),
                                        'post_status' => 'publish',
                                        /*'tax_query' => array(
                                                array(
                                                    'taxonomy' => 'tipologia_evento',
                                                    'field'    => 'slug',
                                                    'terms'    => 'stampa',
                                                )
                                            )*/
                                    ));
                                    //Cache Results
                                    set_transient($home_tabs_3_transient, $home_tabs_3_loop, 3 * HOUR_IN_SECONDS );
                                }
                                $home_tabs_3_loop = get_transient($home_tabs_3_transient);
                                ?>

                                <?php if ($home_tabs_3_loop->have_posts()) : while ( $home_tabs_3_loop->have_posts() ) : $home_tabs_3_loop->the_post();
                                        
                                    // Post Data
                                    /*$taxonomy = get_post_taxonomies();
                                    foreach ($taxonomy as $tax ) {
                                        $terms = get_the_terms( get_the_ID(), $tax);
                                    }
                                    if ( $terms && ! is_wp_error( $terms ) ) :
                                        $terms_array = array();
                                        foreach ( $terms as $term ) {
                                            $term_link = get_term_link($term->term_id);
                                            $terms_array[] = '<a href="'.$term_link.'">'.$term->name.'</a>';
                                        }          
                                    endif;    
                                    $the_terms = join( ", ", $terms_array );*/

                                    // Event date
                                    $event_start = get_field('evento_inizio');
                                    if($event_start != '') {
                                        $event_start = str_replace('00:00', '', $event_start);
                                    } else {
                                        $event_start = get_the_time('j/m/Y');
                                    }

                                    // Event Città
                                    $event_citta = get_field('evento_citta');
                                    if($event_citta != '') {
                                        $citta = ' | '.$event_citta;
                                    } else {
                                        $citta = '';
                                    }

                                    // File
                                    $upload = get_field('file_upload');
                                    $file_url = get_field('file_url');
                                    if($upload != '' && !is_null($upload)) {
                                        $url = $upload;
                                    } elseif($file_url != '' && !is_null($file_url)) {
                                        $url = $file_url;
                                    } else {
                                        $url = get_the_permalink(get_the_ID());
                                    }

                                    // Image
                                    $image = wp_imager(570, 350, 1, 'img-fluid', false, false, true);
                                    ?>

                                    <div class="col-xs-6 col-sm-6 col-md-4 col-12 item">
                                        <div class="tab-img-div fullwidth">
                                            <?php
                                                if($image != '' || !is_null($image)) {
                                                        echo wp_imager(570, 350, 1, 'img-fluid');
                                                } else {

                                                }
                                            ?>
                                        </div>
                                        <div class="date"><i class="fal fa-calendar-alt"></i><p><span><?php echo $event_start ?><?php echo $citta; ?></span></p> <!-- | <?php //echo $the_terms; ?> --></div>
                                        <h2><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h2>
                                        <?php
                                            $the_content = get_the_content();
                                            if($the_content != '') {
                                                echo custom_excerpt(16);
                                            }
                                        ?>
                                        <!-- <p><?php //echo custom_excerpt($excerpt_lenght); ?></p> -->
                                        <!-- <div class="anchor-div">
                                            <a href="<?php //the_permalink(); ?>">
                                                <span class="a-img"><i class="far fa-angle-right"></i></span>
                                                <span class="a-text">approfondisci</span>
                                            </a>
                                        </div> -->
                                    </div>

                                <?php endwhile; endif; wp_reset_query(); wp_reset_postdata(); 
                                    
                            ?>  

                        </div>

                        <div class="text-center fullwidth my-3">
                            <a href="<?php bloginfo('url'); ?>/evento" class="tab-bottom-btn">Vedi tutti</a>
                        </div>

                    </div>

                </div>

                <!-- <div class="text-center fullwidth my-3">
                    <a href="#" class="tab-bottom-btn">Leggi tutte</a>
                </div> -->
                
            </div>
        </div>
        <!-- ========================================================================================================
            End of Tabs
        ========================================================================================================= -->

    </main>

<?php } ?>

<!-- =========== Footer starts here ========== -->
<footer id="footer" class="footer fullwidth">
    <div class="container">

        <?php //if(is_home() || is_front_page()) { ?>

            <!-- Linea Diretta -->
            <div class="footer_basso">
                <div class="menu-footer-bottom-container">
                    <div class="row">
                        <?php 
                            $menu_name = 'footer-bottom';
        
                            if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
                                $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                                $menu_items = wp_get_nav_menu_items($menu->term_id);
                                                      
                                foreach ( (array) $menu_items as $key => $menu_item ) {

                                    // DEBUG
                                    /*echo '<pre>';
                                    var_dump($menu_item->classes);
                                    echo '</pre>';*/

                                    // Color
                                    $menu_class = $menu_item->classes[0];
                                    $color = '';
                                    //$color = get_term_meta($menu_item->object_id, 'color', true);
                                    $color = get_field('term_colore', $queried_tax.'_'.$menu_item->object_id);
                                    if($color != '') {
                                        $color_html = '<span style="background-color:'.$color.'!important" class="a-img"><i class="far fa-angle-right"></i></span>';
                                    } elseif($menu_class != '') {
                                        $color_html = '<span class="a-img '.$menu_class.'"><i class="far fa-angle-right"></i></span>';
                                    } else {
                                        $color_html = '<span class="a-img"><i class="far fa-angle-right"></i></span>';
                                    }

                                    $title = $menu_item->title;
                                    $url = $menu_item->url;
                                    $menu_list .= '<div class="col-md-3 col-sm-6 col-12 button"><a href="' . $url . '">' .$color_html . $title . '</a></div>';
                                }
                            } else {
                                $menu_list = '<ul><li>Menu "' . $menu_name . '" non presente.</li></ul>';
                            }
                            echo $menu_list;
                        ?>
                    </div>
                </div>
            </div>

        <?php //} ?>
    
        <div class="footmenu fullwidth">
            <div class="float-left left-menu">
                <?php if (is_active_sidebar('footer-sx')) : dynamic_sidebar('footer-sx'); endif; ?>
            </div>
            <div class="float-right right-menu">
                <?php if (is_active_sidebar('footer-dx')) : dynamic_sidebar('footer-dx'); endif; ?>
            </div>
        </div>

    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>