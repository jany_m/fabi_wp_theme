<?php
// Exit if accessed directly
//if ( !defined( 'ABSPATH' ) ) exit;

/* ---------------------------------------------------------------------------
 .----..-. .-.  .--.  .-.   .-..----. .-..-.  .-.
{ {__  | {_} | / {} \ |  `.'  || {}  }| | \ \/ /
.-._} }| { } |/  /\  \| |\ /| || {}  }| | / /\ \
`----' `-' `-'`-'  `-'`-' ` `-'`----' `-'`-'  `-'

SHAMBIX.COM	(c) 2018
--------------------------------------------------------------------------- */

/* ---------------------------------------------------------------------------
*
* SETUP
*
* E' possibile modificare queste costanti
*
--------------------------------------------------------------------------- */

// Se utente non loggato, fai redirect qui per login
//define('PAG_LOGIN', 'https://www.ssl.fabi.it/iscritti/login_iscritti.asp');
define('PAG_LOGIN', get_field('pag_login', 'option'));
// Se utente non è autorizzato o non registrato
define('PAG_REG', get_field('pag_reg', 'option'));
// Pagine dove mandare utente dopo login (usato solo nel caso che il form login non passi già $_POST['url_redirect'])
//define('REDIRECT_AREA_RISERVATA', get_bloginfo('url').'tipologia_pubblicazioni/centro-studi/');
define('REDIRECT_AREA_RISERVATA', get_field('pag_redirect', 'option'));

// Logo sopra al form di login per backend di WordPress
define('THEME_LOGO', get_stylesheet_directory_uri()."/assets/img/logo.png");

// Misure Loghi Banche
define('BANCA_LOGO_W', 60);
define('BANCA_LOGO_H', 60);

// Social
define('FB_APP_ID', 2398150773803835);
define('FB_PAGE_ADMIN','');

/* ---------------------------------------------------------------------------
*
* REQUIRES & INCLUDES
*
* Non modificare i file sottostanti
*
--------------------------------------------------------------------------- */

require_once('assets/scripts/custom.php');
