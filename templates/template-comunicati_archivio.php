<?php get_header();

    /*if(current_user_can('administrator')) {
        echo get_current_template();
    }*/   

    global $wp_query, $comunicati_lista;
    
    $queried_object = get_queried_object();
    $queried_term = (isset($_GET['term']) ? $_GET['term'] : '');
    $queried_keyword = (isset($_GET['keyword']) ? $_GET['keyword'] : '');
    $queried_tax = $queried_object->taxonomy;
    $queried_tax_term = $queried_object->slug;

    // Title
    if(isset($_GET['term'])) {
        //$post_type = get_post_type();
        $post_type_label = get_post_type_object( $post_type )->labels->name;
        $queried_tax_term_label = get_term_by( 'slug', $queried_tax_term, $queried_tax)->name;
        $queried_term_label = get_term_by( 'slug', $queried_term, 'tipologia_comunicato')->name;
        if($queried_term_label != '') $queried_term_label = ' - '.$queried_term_label;
        $page_name = $queried_tax_term_label.$queried_term_label;
    } elseif(is_page()) {
        $page_name = $queried_object->post_title;
    } elseif(is_post_type_archive()) {
        $page_term = '';
        $page_name .= $queried_object->labels->name;
        //$page_name .= ' - Regioni';
    } elseif(is_category()) {
        $page_name = single_cat_title( '', false );
        $cat_descr = category_description();
    } elseif(is_tax()) {
        $page_name = $queried_object->name;
    } elseif(is_archive()) {
        $page_name = get_the_archive_title('',''); // https://developer.wordpress.org/reference/functions/get_the_archive_title/
    }

    // Banca URL
    $banca_url = get_field('banca_url', get_queried_object());

    // Banca Logo
    if(is_tax( 'gruppi_bancari')) {
        /*$banca_logo_term_id = get_term_meta($queried_object->term_id, 'image', true);
        $banca_image_term_data = wp_get_attachment_image_src( $banca_logo_term_id, 'full' );
        $banca_term_logo = $banca_image_term_data[0];*/
        $banca_logo = get_field('term_img', $queried_tax.'_'.$queried_object->term_id);
    }

    // Layout colonne
    if (is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) :
        $col_sidebar = 'col-3';
        $col_content = 'col-9';
        $col_item = 'col-xs-12 col-6';
    else:
        $col_sidebar = '';
        $col_content = 'col-12';
        $col_item = 'col-xs-12 col-4';
    endif;

    // DEBUG
    /*if(current_user_can('administrator')) {
        echo '<pre>';
        //var_dump($queried_object);
        //var_dump($queried_object);
        var_dump($wp_query);
        //var_dump($post_type_obj);
        echo '</pre>';
        //echo get_queried_object()->name;
        //$post_type_slug_of_archive = $queried_object->name;
    }*/

?>

<main class="full-width">

<div class="fullwidth breadcrumb-links">
    <div class="container">
        <nav aria-label="breadcrumb">
            <?php breadcrumbs(); ?>
        </nav>
    </div>
</div>

<div class="fullwidth about-content">
    <div class="container">
        <div class="row">

            <?php
                // Sidebar
                if (is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) :?>
                    <div class="<?php echo $col_sidebar; ?> sidebar-sx">
                        <h4 class="visible-xs sidebar_toggle">Menù della Pagina</h4>
                        <div class="inner_sidebar">
                            <?php if (is_active_sidebar('sidebar-sx')) : dynamic_sidebar('sidebar-sx'); endif; ?>
                            <?php if (is_active_sidebar('sidebar-generale-sx')) : dynamic_sidebar('sidebar-generale-sx'); endif; ?>
                        </div>
                    </div>
                <?php
                endif;
            ?>

            <div class="<?php echo $col_content; ?> the_grid the_list">

                <!-- Titolo & Riassunto -->
                <div class="top-heading">
                    <div class="row">
                        <div class="col-12">
                            <?php if($banca_term_logo != '') { ?>
                                <div class="logo">
                                    <img src="<?php echo wp_imager(90, 90, 1, 'img-fluid', false, $banca_term_logo, true); ?>" alt="<?php echo $page_name;?>" title="<?php echo $page_name;?>" />
                                </div>
                            <?php } ?>
                            <h2><?php echo $page_name; ?></span></h2>
                            <?php if(strlen($cat_descr) > 0) { ?>
                                <p><?php $cat_descr; ?></p>
                            <?php } ?>
                            <?php if($banca_url !== '' && !is_null($banca_url)) { ?>
                                <p><a href="<?php echo $banca_url; ?>" rel="nofollow" target="_blank"><i class="fal fa-link"></i> <?php echo $banca_url; ?></a></p>
                            <?php } ?>
                        </div>
                    </div>
                </div> 
                    
                <?php if(!is_page() && !is_category($comunicati_lista)) { ?>
                    <!-- Form -->
                    <div class="search_form">
                        <form method="get">
                            <input type="hidden" name="site-url" value="<?php echo get_bloginfo('url'); ?>" />
                            <input type="hidden" name="post_type" value="comunicato" />
                            <?php
                                        // Check if values are cached, if not cache them
                                        $tipologia_comunicato_terms_transient = THEME_SLUG.'_comunicati_terms_1g';
                                        delete_transient($tipologia_comunicato_terms_transient);
                                        if(get_transient($tipologia_comunicato_terms_transient) === false) {
                                            $terms = get_terms( array(
                                                'taxonomy' => 'tipologia_comunicato',
                                                'hide_empty' => false,
                                            ));
                                            //Cache Results
                                            set_transient($tipologia_comunicato_terms_transient, $terms, 24 * HOUR_IN_SECONDS );
                                        }
                                        $terms = get_transient($tipologia_comunicato_terms_transient);

                                        /*echo '<pre>';
                                        var_dump($terms);
                                        echo '</pre>';*/

                                        // Check if values are cached, if not cache them
                                        $tipologia_comunicato_term_names_transient = THEME_SLUG.'_comunicati_term_names_1g';
                                        delete_transient($tipologia_comunicato_term_names_transient);
                                        if(get_transient($tipologia_comunicato_term_names_transient) === false) {
                                            $term_names = array();
                                            foreach($terms as $index => $term) :
                                                $term_names[] = $term->name;
                                            endforeach;
                                            //Cache Results
                                            set_transient($tipologia_comunicato_term_names_transient, $term_names, 24 * HOUR_IN_SECONDS );
                                        }
                                        $term_names = get_transient($tipologia_comunicato_term_names_transient);
                                        
                                        /*echo '<pre>';
                                        var_dump($term_names);
                                        echo '</pre>';*/

                                        // Check if values are cached, if not cache them
                                        $tipologia_comunicato_term_slugs_transient = THEME_SLUG.'_comunicati_term_slugs_1g';
                                        delete_transient($tipologia_comunicato_term_slugs_transient);
                                        if(get_transient($tipologia_comunicato_term_slugs_transient) === false) {
                                            $term_slugs = array();
                                            foreach($terms as $index => $term) :
                                                $term_slugs[] = $term->slug;
                                            endforeach;
                                            //Cache Results
                                            set_transient($tipologia_comunicato_term_slugs_transient, $term_slugs, 24 * HOUR_IN_SECONDS );
                                        }
                                        $term_slugs = get_transient($tipologia_comunicato_term_slugs_transient);

                                        /*echo '<pre>';
                                        var_dump($term_slugs);
                                        echo '</pre>';*/
                                        
                                        // Debug
                                        /*foreach($term_names as $key => $value) :
                                            echo $term_slugs[$key].' '.$value.'<br>';
                                        endforeach;*/
                            ?>

                            <div class="form-row">
                        
                                <div class="col">
                                        <?php
                                            $selected = 0;
                                            echo '<select class="form-control" name="term">';
                                            echo '<option value="" >- Tipologie Comunicati -</option>';
                                            foreach($term_names as $key => $value) :
                                                echo '<option value="', $term_slugs[$key], '"', $queried_term == $term_slugs[$key]  ? ' selected="selected"' : '', '>', $value, '</option>';
                                            endforeach;
                                            echo '</select><i></i>';
                                        ?>
                                </div>
                                <div class="col">
                                        <?php
                                            echo '<input class="form-control" name="keyword" value="'.$queried_keyword.'" placeholder="Parola chiave..." />';
                                        ?>
                                </div>
                                <div class="col">
                                    <button class="btn btn-primary btn-block" type="submit">Cerca</button>
                                </div>

                            </div>
                        </form>
                    </div>
                <?php } ?>

                <!-- ========================================================================================================
                    Grid
                ========================================================================================================= -->
                <div class="grid-bottom fullwidth">
                    <div class="row align-items-stretch">

                        <?php
                            // Risultati ricerca comunicati
                            if(isset($_GET['term']) && $_GET['term'] != '') {

                                // Custom Pagination
                                /*if ( get_query_var('paged') ) {
                                    $paged = get_query_var('paged');
                                } elseif ( get_query_var('page') ) {
                                    $paged = get_query_var('page');
                                } else {
                                    $paged = 1;
                                }*/

                                // The Loop
                                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                $search_query_args = array(
                                    'posts_per_page' => 100,
                                    'paged' => $paged,
                                    'post_type' => 'comunicato',
                                    'orderby' => 'modified',
                                    'order' => 'DESC',
                                    's' => $queried_keyword,
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'tipologia_comunicato',
                                            'field' => 'slug',
                                            'terms' => $queried_term
                                        ),
                                        'relation' => 'AND',
                                        array(
                                            'taxonomy' => $queried_tax,
                                            'field' => 'slug',
                                            'terms' => $queried_tax_term,
                                        ),
                                    ),
                                );
                                    
                                // Check if values are cached, if not cache them
                                $risultati_comunicati_transient = THEME_SLUG.'_risultati_comunicati_'.$queried_term.'_1g';
				                delete_transient($risultati_comunicati_transient);
				                if(get_transient($risultati_comunicati_transient) === false) {
		                            $search_query = new WP_Query($search_query_args);
					                //Cache Results
					                set_transient($risultati_comunicati_transient, $search_query, 24 * HOUR_IN_SECONDS );
				                }
                                $search_query = get_transient($risultati_comunicati_transient);
                
                                // Save Query to Temp query for pagination
                                global $custom_query;
                                $custom_query = $search_query;

                                if ($search_query->have_posts()) :  while ( $search_query->have_posts() ) : $search_query->the_post(); 
                                     
                                    /*$terms = wp_get_post_terms(get_the_ID(), 'tipologia_comunicato', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );;
                                    if ( $terms && ! is_wp_error( $terms ) ) :
                                        $terms_array = array();
                                        foreach ( $terms as $term ) {
                                            $terms_array[] = $term;
                                        }              
                                        $the_terms = join( ", ", $terms_array );
                                    endif;*/

                                    // RESET
                                    $banca = '';
                                    $banca_logo = '';
                                    $upload = '';
                                    $file_url = '';
                                    $url = '';

                                    // Logo
                                    $banca = get_the_terms(get_the_ID(), 'gruppi_bancari');
                                    /*$banca_logo_id = get_term_meta($banca[0]->term_id, 'image', true);
                                    $banca_image_data = wp_get_attachment_image_src( $banca_logo_id, 'full' );
                                    $banca_logo = $banca_image_data[0];*/
                                    $banca_logo = get_field('term_img', 'gruppi_bancari_'.$banca[0]->term_id);

                                    // File
                                    $upload = get_field('file_upload');
                                    $file_url = get_field('file_url');
                                    if($upload != '' && !is_null($upload)) {
                                        $url = $upload;
                                    } elseif($file_url != '' && !is_null($file_url)) {
                                        $url = $file_url;
                                    } else {
                                        $url = get_the_permalink(get_the_ID());
                                    }
                                    ?>

                                    <div class="<?php echo $col_item; ?>">
                                        <div class="grid-bottom-single risultato fullwidth">

                                            <div class="date"><p><span class="float-right"><?php the_time('j/m/Y'); ?></span></p></div>
                                            <?php if($banca_logo != '') { ?>
                                                <div class="logo">
                                                    <img src="<?php echo wp_imager(BANCA_LOGO_W, BANCA_LOGO_H, 1, 'img-fluid', false, $banca_logo, true); ?>" alt="<?php echo $banca[0]->name;?>" title="<?php echo $banca[0]->name;?>" />
                                                </div>
                                            <?php } ?>
                                            <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>
                                            <!-- <div class="date"><p><?php //echo $the_terms; ?></span></p></div> -->

                                        </div>
                                    </div>

                                    <?php
                                endwhile;
                            
                                else:

                                    echo '<p>Non ci sono comunicati di questo tipo, per il momento.</p>';

                                endif;
                                ?>

                                <!--Pagination-->
                                <div class="col-12 pagination_wrapper">
                                    <?php custom_query_pagination('&laquo;','&raquo;'); ?>
                                </div>
                                <!--End Pagination-->

			                    <?php wp_reset_query(); wp_reset_postdata(); ?>
                
                                <?php

                            // Comunicati Nazionali
                            } elseif(is_page(COMUNICATI_NAZIONALI)) {

                                // Custom Pagination
                                /*if ( get_query_var('paged') ) {
                                    $paged = get_query_var('paged');
                                } elseif ( get_query_var('page') ) {
                                    $paged = get_query_var('page');
                                } else {
                                    $paged = 1;
                                }*/

                                // The Loop
                                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                $query_args = array(
                                    'posts_per_page' => 100,
                                    'paged' => $paged,
                                    'post_type' => 'comunicato',
                                    'orderby' => 'modified',
                                    'order' => 'DESC',
                                    'taxonomy' => 'gruppi_bancari',
                                );

                                // Check if values are cached, if not cache them
                                $all_comunicati_transient = THEME_SLUG.'_risultati_comunicati_'.$queried_term.'_1g';
				                delete_transient($all_comunicati_transient);
				                if(get_transient($all_comunicati_transient) === false) {
		                            $all_comunicati_query = new WP_Query($query_args);
					                //Cache Results
					                set_transient($all_comunicati_transient, $all_comunicati_query, 24 * HOUR_IN_SECONDS );
				                }
                                $all_comunicati_query = get_transient($all_comunicati_transient);
                
                                // Save Query to Temp query for pagination
                                global $custom_query;
                                $custom_query = $all_comunicati_query;

                                if ($all_comunicati_query->have_posts()) :  while ( $all_comunicati_query->have_posts() ) : $all_comunicati_query->the_post();

                                    // RESET
                                    $terms_comunicato = '';
                                    $the_terms_comunicato = '';
                                    $terms_comunicato_array = '';
                                    $terms_banche = '';
                                    $the_terms_banche = '';
                                    $terms_banche_array = '';
                                    $terms_soggetti = '';
                                    $the_terms_soggetti = '';
                                    $terms_soggetti_array = '';
                                    $the_other_terms = '';
                                    $banca = '';
                                    $banca_logo = '';
                                    $upload = '';
                                    $file_url = '';
                                    $url = '';

                                    // Termini della tassonomia tipologia_comunicato
                                    $terms_comunicato = wp_get_post_terms(get_the_ID(), 'tipologia_comunicato', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );;
                                    if ( $terms_comunicato && ! is_wp_error( $terms_comunicato ) ) :
                                        $terms_comunicato_array = array();
                                        foreach ( $terms_comunicato as $term_comunicato ) {
                                            $terms_comunicato_array[] = $term_comunicato;
                                        }              
                                        $the_terms_comunicato = join( ", ", $terms_comunicato_array );
                                    endif;

                                    // Termini della tassonomia gruppi_bancari
                                    $terms_banche = wp_get_post_terms(get_the_ID(), 'gruppi_bancari', array('orderby' => 'name', 'order' => 'ASC'));
                                    if ( $terms_banche && ! is_wp_error( $terms_banche ) ) :
                                        $terms_banche_array = array();
                                        foreach ( $terms_banche as $term_banca ) {
                                            if(is_wp_error($term_banca))
                                                continue;
                                            $terms_banche_array[] = '<a href="'.get_term_link($term_banca).'">'.$term_banca->name.'</a>';
                                        }              
                                        $the_terms_banche = join( ", ", $terms_banche_array );
                                        $the_terms_banche = strtolower($the_terms_banche);
                                    endif;

                                    // Termini della tassonomia tipologia_soggetto
                                    $terms_soggetti = wp_get_post_terms(get_the_ID(), 'tipologia_soggetto', array('orderby' => 'name', 'order' => 'ASC'));
                                    if ( $terms_soggetti && ! is_wp_error( $terms_soggetti ) ) :
                                        $terms_soggetti_array = array();
                                        foreach ( $terms_soggetti as $term_soggetto ) {
                                            if(is_wp_error($term_soggetto))
                                                continue;
                                            $terms_soggetti_array[] = '<a href="'.get_term_link($term_soggetto).'">'.$term_soggetto->name.'</a>';
                                        }              
                                        $the_terms_soggetti = join( ", ", $terms_banche_array );
                                        //$the_terms_banche = strtolower($the_terms_banche);
                                    endif;

                                    // Join termini
                                    if($the_terms_banche == '' && $the_terms_soggetti != '') {
                                        $the_other_terms = $the_terms_soggetti;
                                    } else {
                                        $the_other_terms = $the_terms_banche;
                                    }

                                    // Logo Banca
                                    $banca = get_the_terms(get_the_ID(), 'gruppi_bancari');
                                    /*$banca_logo_id = get_term_meta($banca[0]->term_id, 'image', true);
                                    $banca_image_data = wp_get_attachment_image_src( $banca_logo_id, 'full' );
                                    $banca_logo = $banca_image_data[0];*/
                                    $banca_logo = get_field('term_img', 'gruppi_bancari_'.$banca[0]->term_id);

                                    // File
                                    $upload = get_field('file_upload');
                                    $file_url = get_field('file_url');
                                    if($upload != '' && !is_null($upload)) {
                                        $url = $upload;
                                    } elseif($file_url != '' && !is_null($file_url)) {
                                        $url = $file_url;
                                    } else {
                                        $url = get_the_permalink(get_the_ID());
                                    }
                                    ?>

                                    <div class="<?php echo $col_item; ?>">
                                        <div class="grid-bottom-single fullwidth">
                                            
                                            <div class="date">
                                                <p>
                                                    <span class="float-left">
                                                        <?php echo $the_terms_comunicato; ?><?php if($the_other_terms != '') echo '<em class="banca"> - '.$the_other_terms.'</em>'; ?>
                                                    </span>
                                                    <span class="float-right"><?php the_time('j/m/Y'); ?></span>
                                                </p>
                                            </div>
                                            <?php if($banca_logo != '') { ?>
                                                <div class="logo">
                                                    <img src="<?php echo wp_imager(BANCA_LOGO_W, BANCA_LOGO_H, 1, 'img-fluid', false, $banca_logo, true); ?>" alt="<?php echo $banca[0]->name;?>" title="<?php echo $banca[0]->name;?>" />
                                                </div>
                                            <?php } ?>
                                            <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>

                                        </div>
                                    </div>

                                <?php endwhile; endif; ?>

                                <!--Pagination-->
                                <div class="col-12 pagination_wrapper">
                                    <?php custom_query_pagination('&laquo;','&raquo;'); ?>
                                </div>
                                <!--End Pagination-->

                                <?php wp_reset_query(); wp_reset_postdata(); ?>

                                <?php
                            
                        

                            // Tutti i comunicati
                            } else {

                                if (have_posts()) : while ( have_posts() ) : the_post();

                                    // RESET
                                    $terms = '';
                                    $the_terms = '';
                                    $terms_array = '';
                                    $terms_comunicato = '';
                                    $the_terms_comunicato = '';
                                    $terms_comunicato_array = '';
                                    $terms_banche = '';
                                    $the_terms_banche = '';
                                    $terms_banche_array = '';
                                    $terms_soggetti = '';
                                    $the_terms_soggetti = '';
                                    $terms_soggetti_array = '';
                                    $the_other_terms = '';
                                    $banca = '';
                                    $banca_logo = '';
                                    $upload = '';
                                    $file_url = '';
                                    $url = '';

                                    $terms = wp_get_post_terms(get_the_ID(), 'tipologia_comunicato', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );;
                                     if ( $terms && ! is_wp_error( $terms ) ) :
                                        $terms_array = array();
                                        foreach ( $terms as $term ) {
                                            $terms_array[] = $term;
                                        }              
                                        $the_terms = join( ", ", $terms_array );
                                    endif;

                                    // Termini della tassonomia tipologia_comunicato
                                    $terms_comunicato = wp_get_post_terms(get_the_ID(), 'tipologia_comunicato', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );;
                                    if ( $terms_comunicato && ! is_wp_error( $terms_comunicato ) ) :
                                        $terms_comunicato_array = array();
                                        foreach ( $terms_comunicato as $term_comunicato ) {
                                            $terms_comunicato_array[] = $term_comunicato;
                                        }              
                                        $the_terms_comunicato = join( ", ", $terms_comunicato_array );
                                    endif;

                                    // Termini della tassonomia gruppi_bancari
                                    $terms_banche = wp_get_post_terms(get_the_ID(), 'gruppi_bancari', array('orderby' => 'name', 'order' => 'ASC'));
                                    if ( $terms_banche && ! is_wp_error( $terms_banche ) ) :
                                        $terms_banche_array = array();
                                        foreach ( $terms_banche as $term_banca ) {
                                            if(is_wp_error($term_banca))
                                                continue;
                                            $terms_banche_array[] = '<a href="'.get_term_link($term_banca).'">'.$term_banca->name.'</a>';
                                        }              
                                        $the_terms_banche = join( ", ", $terms_banche_array );
                                        $the_terms_banche = strtolower($the_terms_banche);
                                    endif;

                                    // Termini della tassonomia tipologia_soggetto
                                    $terms_soggetti = wp_get_post_terms(get_the_ID(), 'tipologia_soggetto', array('orderby' => 'name', 'order' => 'ASC'));
                                    if ( $terms_soggetti && ! is_wp_error( $terms_soggetti ) ) :
                                        $terms_soggetti_array = array();
                                        foreach ( $terms_soggetti as $term_soggetto ) {
                                            if(is_wp_error($term_soggetto))
                                                continue;
                                            $terms_soggetti_array[] = '<a href="'.get_term_link($term_soggetto).'">'.$term_soggetto->name.'</a>';
                                        }              
                                        $the_terms_soggetti = join( ", ", $terms_banche_array );
                                        //$the_terms_banche = strtolower($the_terms_banche);
                                    endif;

                                    // Join termini
                                    if($the_terms_banche == '' && $the_terms_soggetti != '') {
                                        $the_other_terms = $the_terms_soggetti;
                                    } else {
                                        $the_other_terms = $the_terms_banche;
                                    }

                                    // Logo Banca
                                    $banca = get_the_terms(get_the_ID(), 'gruppi_bancari');
                                    /*$banca_logo_id = get_term_meta($banca[0]->term_id, 'image', true);
                                    $banca_image_data = wp_get_attachment_image_src( $banca_logo_id, 'full' );
                                    $banca_logo = $banca_image_data[0];*/
                                    $banca_logo = get_field('term_img', 'gruppi_bancari_'.$banca[0]->term_id);

                                    // File
                                    $upload = get_field('file_upload');
                                    $file_url = get_field('file_url');
                                    if($upload != '' && !is_null($upload)) {
                                        $url = $upload;
                                    } elseif($file_url != '' && !is_null($file_url)) {
                                        $url = $file_url;
                                    } else {
                                        $url = get_the_permalink(get_the_ID());
                                    }
                                    ?>

                                    <div class="<?php echo $col_item; ?>">
                                        <div class="grid-bottom-single fullwidth">
                                            
                                            <div class="date">
                                                <p>
                                                    <span class="float-left">
                                                        <?php echo $the_terms_comunicato; ?><?php //if($the_other_terms != '') echo '<em class="banca"> - '.$the_other_terms.'</em>'; ?>
                                                    </span>
                                                    <span class="float-right"><?php the_time('j/m/Y'); ?></span>
                                                </p>
                                            </div>
                                            <?php if($banca_logo != '') { ?>
                                                <div class="logo">
                                                    <img src="<?php echo wp_imager(BANCA_LOGO_W, BANCA_LOGO_H, 1, 'img-fluid', false, $banca_logo, true); ?>" alt="<?php echo $banca[0]->name;?>" title="<?php echo $banca[0]->name;?>" />
                                                </div>
                                            <?php } ?>
                                            <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>
                                            <!-- <div class="date"><p><?php //echo $the_terms_banche; ?></p></div> -->

                                        </div>
                                    </div>

                                <?php endwhile; endif;
                                
                            }
                        ?>
                        
                        <!--Pagination-->
                        <div class="col-12 pagination_wrapper">
                            <?php
                                the_posts_pagination( array(
                                    'mid_size' => 5,
                                    'prev_text' => '&laquo;',
                                    'next_text' => '&raquo;',
                                ) );
                                //custom_query_pagination('&laquo;','&raquo;');
                            ?>
                        </div>
                        <!--End Pagination-->

                        <?php wp_reset_query(); wp_reset_postdata(); ?>

                    </div>
                </div>
                <!-- ========================================================================================================
                    End of Grid
                ========================================================================================================= -->

                
            </div>

        </div>
    </div>
</div>

</main>
<?php get_footer(); ?>