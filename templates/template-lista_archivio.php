<?php get_header();

    if(current_user_can('administrator')) {
        //echo get_current_template();
    }

    global $wp_query;
    $queried_object = get_queried_object();

    // Custom
    //$tax = 'regione';
    //print_r($queried_object);
    //echo get_queried_object()->name;
    $post_type_slug_of_archive = $queried_object->name;

    // Title
    if(is_page()) {
        $page_name = $queried_object->post_title;
    } elseif(is_post_type_archive()) {
        $page_term = '';
        $page_name .= $queried_object->labels->name;
        //$page_name .= ' - Regioni';
    } elseif(is_category()) {
        $page_name = single_cat_title( '', false );
        $cat_descr = category_description();
        //$post_type = get_post_type();
        //$page_name = get_post_type_object( $post_type )->labels->name;
        //var_dump($post_type_obj);
    } elseif(is_archive()) {
        $page_name = get_the_archive_title('',''); // https://developer.wordpress.org/reference/functions/get_the_archive_title/
    }

    // DEBUG
    /*if(current_user_can('administrator')) {
        echo '<pre>';
        var_dump($wp_query);
        echo '</pre>';
    }*/

?>

<main class="full-width">

<div class="fullwidth breadcrumb-links">
    <div class="container">
        <nav aria-label="breadcrumb">
            <?php breadcrumbs(); ?>
        </nav>
    </div>
</div>

<div class="fullwidth about-content">
    <div class="container">
        <div class="row">

            <?php
                if (is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) :
                    $col_content = 'col-9';
                    $col_item = 'col-md-4 col-sm-6 col-12';
                    ?>
                    <div class="col-3 sidebar-sx">
                        <h4 class="visible-xs sidebar_toggle">Menù della Pagina</h4>
                        <div class="inner_sidebar">
                            <?php if (is_active_sidebar('sidebar-sx')) : dynamic_sidebar('sidebar-sx'); endif; ?>
                            <?php if (is_active_sidebar('sidebar-generale-sx')) : dynamic_sidebar('sidebar-generale-sx'); endif; ?>
                        </div>
                    </div>
                    <?php
                else:
                    $col_content = 'col-12';
                    $col_item = 'col-md-3 col-sm-4 col-xs-2';
                endif;
            ?>

            <div class="<?php echo $col_content; ?> the_grid the_list">

                <!-- Titolo & Riassunto -->
                <div class="fullwidth top-heading">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h2 class="list_title"><?php echo $page_name; ?></span></h2>
                                <?php if(strlen($cat_descr) > 0) { ?>
                                    <p><?php $cat_descr; ?></p>
                                <?php } ?>
                            </div> 
                        </div>
                    </div>
                </div> 

                <!-- ========================================================================================================
                    Grid
                ========================================================================================================= -->
                <div class="grid-bottom fullwidth">
                    <div class="row align-items-stretch">

                                <?php if (have_posts()) : while ( have_posts() ) : the_post();

                                    // Defaults
                                    $has_feat_image = true;
                                    $has_read_more = true;
                                
                                    // Post Data
                                    $post_type = get_post_type();

                                    /*$taxonomy = get_post_taxonomies();
                                    foreach ($taxonomy as $tax ) {
                                        $terms = get_the_terms( get_the_ID(), $tax);
                                    }
                                    if ( $terms && ! is_wp_error( $terms ) ) :
                                        $terms_array = array();
                                        foreach ( $terms as $term ) {
                                            $terms_array[] = $term->name;
                                        }              
                                        $the_terms = join( ", ", $terms_array );
                                    endif;*/


                                    // Comunicati
                                    /*if($post_type_slug_of_archive == 'comunicato') {
                                        $has_feat_image = false;
                                        $has_read_more = false;
                                    }*/
                                    ?>

                                    <?php

                                    //echo 'xxx '.$queried_object->slug;

                                    // Gruppi Bancari
                                    if($queried_object->post_name == GRUPPI_BANCARI) {

                                        ?>
                                        <!-- <div class="col-12 list_title">
                                            <h3>Principali Gruppi Bancari</h3>
                                        </div> -->
                                        <?php
                                        
                                        $terms = get_terms(array( 
                                            'taxonomy' => 'gruppi_bancari',
                                            'hide_empty' => false,
                                            'exclude' => array('91'), // Banca d'Italia
                                            //'orderby' => 'include',
                                            //'fields' => 'ids'
                                        ));

                                        /*echo '<pre>';
                                        var_dump($terms);
                                        echo '</pre>';*/

                                        // Gruppi Bancari
                                        foreach($terms as $term) {

                                            // Solo banca?
                                            $is_banca = get_term_meta($term->term_id, 'is_banca', true);
                                            if($is_banca) continue;

                                            // Logo
                                            /*$logo_id = get_term_meta($term->term_id, 'image', true);
                                            $image_data = wp_get_attachment_image_src( $logo_id, 'full' );
                                            $logo = $image_data[0];*/
                                            $logo = get_field('term_img', $term->taxonomy.'_'.$term->term_id);

                                            // Link
                                            $link = get_term_link($term->term_id);
                                            
                                            ?>

                                            <div class="col-6 item_lista">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <?php
                                                            if($logo != '') {
                                                                echo wp_imager(30, 30, 1, 'img-fluid', false, $logo);
                                                            }
                                                        ?>
                                                    </div>
                                                    <div class="col-10">
                                                        <h4><a href="<?php echo $link; ?>"><?php echo $term->name; ?></a></h4>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }

                                        ?>
                                        <hr class="lista" />
                                        
                                        <div class="col-12">
                                            <h2 class="list_title">Banche</h2>
                                        </div>

                                        <?php

                                        // Banche
                                        foreach($terms as $term) {

                                            // Solo banca?
                                            $is_banca = get_term_meta($term->term_id, 'is_banca', true);
                                            if(!$is_banca) continue;

                                            // Logo
                                            /*$logo_id = get_term_meta($term->term_id, 'image', true);
                                            $image_data = wp_get_attachment_image_src( $logo_id, 'full' );
                                            $logo = $image_data[0];*/
                                            $logo = get_field('term_img', $term->taxonomy.'_'.$term->term_id);

                                            // Link
                                            $link = get_term_link($term->term_id);
                                            
                                            ?>

                                            <div class="col-6 item_lista">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <?php
                                                            if($logo != '') {
                                                                echo wp_imager(30, 30, 1, 'img-fluid', false, $logo);
                                                            }
                                                        ?>
                                                    </div>
                                                    <div class="col-10">
                                                        <h4><a href="<?php echo $link; ?>"><?php echo $term->name; ?></a></h4>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    
                                    // Agenti Riscossione
                                    } elseif($queried_object->post_name == AGENTI_RISC) {

                                        ?>
                                        <!-- <div class="col-12 list_title">
                                            <h3>Riscossione</h3>
                                        </div> -->
                                        <?php
                                        
                                        $terms = get_terms(array( 
                                            'taxonomy' => 'agenti_riscossione',
                                            'hide_empty' => false,
                                            //'exclude' => array(''),
                                            //'orderby' => 'include',
                                            //'fields' => 'ids'
                                        ));

                                        /*echo '<pre>';
                                        var_dump($terms);
                                        echo '</pre>';*/

                                        foreach($terms as $term) {

                                            // Logo
                                            /*$logo_id = get_term_meta($term->term_id, 'image', true);
                                            $image_data = wp_get_attachment_image_src( $logo_id, 'full' );
                                            $logo = $image_data[0];*/
                                            $logo = get_field('term_img', $term->taxonomy.'_'.$term->term_id);

                                            // Link
                                            $link = get_term_link($term->term_id);
                                            
                                            ?>

                                            <div class="col-6 item_lista">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <?php
                                                            if($logo != '') {
                                                                echo wp_imager(30, 30, 1, 'img-fluid', false, $logo);
                                                            }
                                                        ?>
                                                    </div>
                                                    <div class="col-10">
                                                        <h4><a href="<?php echo $link; ?>"><?php echo $term->name; ?></a></h4>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }

                                    // Federazioni Regionali
                                    } elseif($queried_object->post_name == FEDERAZIONI) {

                                    ?>
                                    <!-- <div class="col-12 list_title">
                                        <h3>Riscossione</h3>
                                    </div> -->
                                    <?php
                                    
                                    $terms = get_terms(array( 
                                        'taxonomy' => 'federazioni_regionali',
                                        'hide_empty' => false,
                                        //'exclude' => array(''),
                                        //'orderby' => 'include',
                                        //'fields' => 'ids'
                                    ));

                                    /*echo '<pre>';
                                    var_dump($terms);
                                    echo '</pre>';*/

                                    foreach($terms as $term) {

                                        // Logo
                                        /*$logo_id = get_term_meta($term->term_id, 'image', true);
                                        $image_data = wp_get_attachment_image_src( $logo_id, 'full' );
                                        $logo = $image_data[0];*/
                                        $logo = get_field('term_img', $term->taxonomy.'_'.$term->term_id);

                                        // Link
                                        $link = get_term_link($term->term_id);
                                        
                                        ?>

                                        <div class="col-6 item_lista">
                                            <div class="row">
                                                <div class="col-2">
                                                    <?php
                                                        if($logo != '') {
                                                            echo wp_imager(40, 40, 1, 'img-fluid', false, $logo);
                                                        }
                                                    ?>
                                                </div>
                                                <div class="col-10">
                                                    <h4><a href="<?php echo $link; ?>"><?php echo $term->name; ?></a></h4>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                    }

                                    // Strutture Territoriali                            
                                    } elseif($post_type == 'strutture_territorio') {

                                        $has_feat_image = false;
                                        
                                        // RESET
                                        $resp = $resp_nome = $url = $email = $indirizzo = $citta = $cap = $tel = $fax = $caaf = $csf = '';

                                        // Post Data
                                        $resp = get_field('struttura_nome');
                                        //var_dump($resp);
                                        if($resp != '') {
                                            $resp_nome = get_the_title($resp[0]);
                                        }
                                        $url = get_field('struttura_web');
                                        $email = get_field('struttura_email');
                                        $indirizzo = get_field('struttura_indirizzo');
                                        $citta = get_field('struttura_citta');
                                        $cap = get_field('struttura_cap');
                                        $tel = get_field('struttura_telefono');
                                        $fax = get_field('struttura_fax');
                                        $caaf = get_field('struttura_caaf');
                                        $csf = get_field('struttura_csf');

                                        ?>

                                        <div class="col-6 item_lista">
                                            <h3><?php the_title(); ?></h3>
                                            <div class="struttura_info">
                                                <p><strong><?php echo strtoupper($resp_nome); ?></strong></p>
                                                <p><?php echo $indirizzo; ?></p>
                                                <p>Tel <?php echo $tel; ?></p>
                                                <p>Fax <?php echo $fax; ?></p>
                                                <p>Email <?php echo $email; ?></p>
                                                <p><a href="<?php echo $url; ?>" target="_blank" rel="nofollow"><?php echo $url; ?></a></p>
                                                <p>CAAF <?php echo $caaf; ?></p>
                                                <p>CSF <?php echo $csf; ?></p>
                                            </div>
                                        </div>

                                        <?php
    
                                    }

                                    // Generico
                                    else { 

                                        // File
                                        $upload = get_field('file_upload');
                                        $file_url = get_field('file_url');
                                        if($upload != '' && !is_null($upload)) {
                                            $url = $upload;
                                        } elseif($file_url != '' && !is_null($file_url)) {
                                            $url = $file_url;
                                        } else {
                                            $url = get_the_permalink(get_the_ID());
                                        }


                                    ?>

                                    <div class="col-md-4 col-sm-6 col-12">
                                        <div class="grid-bottom-single fullwidth">

                                            <?php if($has_feat_image) { ?>
                                                <div class="grid-img">
                                                    <?php
                                                        // Replace feat img with video thumb if content is = ''
                                                        if($video != '') {
                                                            echo wp_imager(320, 218, 1, 'img-fluid', false, $video_thumb);
                                                        } else {
                                                            echo wp_imager(320, 218, 1, 'img-fluid');
                                                        }
                                                    ?>
                                                </div>
                                            <?php } ?>

                                            <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>

                                            <?php $has_read_more = false; if($has_read_more) { ?>
                                                <div class="anchor-div">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <span class="a-img"><i class="far fa-angle-right"></i></span>
                                                        <span class="a-text">approfondisci</span>
                                                    </a>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>

                                    <?php } ?>

                                <?php endwhile; endif; ?>

                                <!--Pagination-->
                                <div class="col-12 pagination_wrapper">
                                    <?php 
                                        /*if(is_category()) {
                                            the_posts_pagination( array(
                                                'mid_size' => 5,
                                                'prev_text' => '&laquo;',
                                                'next_text' => '&raquo;',
                                            ));
                                        } else {*/
                                            custom_query_pagination('&laquo;','&raquo;');
                                        //}
                                    ?>
                                </div>
                                <!--End Pagination-->

                                <?php wp_reset_query(); wp_reset_postdata(); ?>

                    </div>
                </div>
                <!-- ========================================================================================================
                    End of Grid
                ========================================================================================================= -->

                
            </div>

        </div>
    </div>
</div>

</main>
<?php get_footer(); ?>