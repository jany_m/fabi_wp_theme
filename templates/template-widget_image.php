<?php
/**
 * Widget template. This template can be overriden using the "sp_template_image-widget_widget.php" filter.
 * See the readme.txt file for more info.
 */

// Block direct requests
if ( ! defined( 'ABSPATH' ) )
	die( '-1' );

echo $before_widget;

echo '<div class="col-md-6 col-sm-6 col-12">
<div class="grid-bottom-single fullwidth">';

echo '<div class="grid-img">';

if($linktitle == '') {
    $linktitle = $title;
}

//echo $this->get_image_html( $instance, true );
echo '<a href="'.$link.'" target="'.$linktarget.'" title="'.$linktitle.'">';
echo '<img src="'.wp_imager(370, 287, 1, 'img-fluid', false, $imageurl, true).'" alt="'.$linktitle.'" />';
echo '</a>';

echo '</div>';

if ( ! empty( $title ) ) { 
    if($link != '') {
        echo '<a href="'.$link.'" target="'.$linktarget.'" title="'.$linktitle.'">';
    }
    echo $before_title . $title . $after_title;
    if($link != '') {
        echo '</a>';
    }

}

if ( ! empty( $description ) ) {
	echo '<div class="' . esc_attr( $this->widget_options['classname'] ) . '-description" >';
	echo wpautop( $description );
	echo '</div>';
}

if($link != '') {
echo '<div class="anchor-div">
    <a href="'.$link.'">
        <span class="a-img"><i class="far fa-angle-right"></i></span>
        <span class="a-text">approfondisci</span>
    </a>';
}
echo '</div>
</div>
</div>';

echo $after_widget;