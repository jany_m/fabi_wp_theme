<!doctype html>
<html class="no-js" lang="it_IT">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">      
        <meta name="description" content="<?php bloginfo('description'); ?>">
        <meta name="author" content="FABI">
       <?php wp_head(); ?>
</head>

<?php
    /* ---------------------------------------------------------------------------
    *
    * AREA RISERVATA
    *
    --------------------------------------------------------------------------- */
    session_start();

    // Check contenuto
    global  $area_iscritti,
            $logged_in,
            $wp_query;

    $area_iscritti = false;
    if(is_null($area_iscritti)) {
        $area_iscritti = false;
    }
    if($_GET['logged_in'] || isset($_GET['logged_in']) || $_SESSION['logged_in'] || current_user_can( 'administrator' )) {
        $logged_in = true;
    } else {
        $logged_in = false;
    }

    $area_iscritti_check = false;
    $area_iscritti_check = get_field('solo_iscritti');
    if(is_category() || is_tax()) {
        if($wp_query->query_vars['term'] != '') {
            $term_id = get_term_by( 'slug', $wp_query->query_vars['term'], $wp_query->query_vars['taxonomy'])->term_id;
            $area_iscritti_check = get_field('solo_iscritti', $wp_query->query_vars['taxonomy'].'_'.$term_id);
        }
    }
    if(!is_null($area_iscritti_check) && $area_iscritti_check !== false) {
        $area_iscritti = true;
    }

    // Check login
    global $logged_in;
    if($area_iscritti && !$logged_in && !is_home() && !is_front_page( )) {
        header("location: ".PAG_LOGIN);
    }
    /*if(!isset($_COOKIE['fabi_user_logged_in']) && $logged_in == false) {
        $logged_in = false;
        if($area_iscritti) {
            header("location: ".PAG_LOGIN);
        }
        echo "<!-- Cookie 'fabi_user_logged_in' NON presente -->";
    } elseif(isset($_COOKIE['fabi_user_logged_in']) || $logged_in == true) {
        $logged_in = true;
        echo "<!-- Cookie 'fabi_user_logged_in' E' presente -->";
    }*/

    // DEBUG
    /*if(current_user_can('administrator')) {
        echo '<pre>';
        //echo 'WP QUERY';
        //echo '<br>';
        //var_dump($wp_query->query_vars);
        echo 'AREA ISCRITTI?';
        echo '<br>';
        var_dump($area_iscritti);
        echo '<br>';
        echo 'LOGGATO?';
        echo '<br>';
        var_dump($logged_in);
        echo '<br>';
        echo '$_SESSION';
        echo '<br>';
        var_dump($_SESSION);
        echo '<br>';
        echo '$_POST';
        echo '<br>';
        var_dump($_POST);
        //echo '<br>';
        //echo '$_SERVER';
        //echo '<br>';
        //var_dump($_SERVER["HTTP_REFERER"]);
        //var_dump($_SERVER);
        echo '</pre>';
    }*/
?>
    
<body <?php body_class(); ?>>

<header class="header fullwidth">

    <div class="container">

        <!-- Top Menu -->
        <div class="top-links w-100">
            <div class="links">
                
                <nav class="navbar-top navbar-expand-lg navbar-light fullwidth mt-4">
               
                        <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'header-top',
                                'depth'             => 2,
                                'container'         => 'div',
                                'container_class'   => 'collapse navbar-collapse',
                                'container_id'      => 'top-nav',
                                'menu_class'        => 'nav navbar-nav',
                                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'            => new WP_Bootstrap_Navwalker(),
                            ) );
                        ?>

                </nav>  

            </div>
        </div>

        <!-- Header -->
        <div class="fullwidth">
            <div class="row">

                <div class="col-md-5 col-sm-12 col-12 align-self-center logo-col">
                    <a href="<?php bloginfo( 'url' ); ?>" title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo THEME_URL.'/assets/img/'; ?>fabi_logo_orizz.png" class="desktop-logo d-none d-md-block" alt="<?php bloginfo('name'); ?>" /></a>
                    <a href="<?php bloginfo( 'url' ); ?>" title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo THEME_URL.'/assets/img/'; ?>logo-mobile.png" class="mobile-logo d-xs-block d-sm-block d-md-none m-auto" alt="<?php bloginfo('name'); ?>" /></a>
                </div>

                <div class="col-md-7 col-sm-12 col-12 search-col align-self-center d-flex justify-content-center justify-content-sm-center justify-content-md-end">
                    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                        <div class="input-group search-box align-self-center">
                            <input type="text" class="form-control" placeholder="Cerca..." value="<?php echo get_search_query(); ?>" name="s" id="s" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <img src="<?php echo THEME_URL.'/assets/img/'; ?>search-icon.png" alt="icon" />
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="social-top align-self-center">
                        <?php
							$socials = get_field('socials_wrap','options');
							$fb = $socials['social_fb'];
							$ig = $socials['social_ig'];
							$tw = $socials['social_tw'];
							$yt = $socials['social_yt'];
							$tg = $socials['social_tg'];
						?>
                        <span>follow us</span>
                        <a href="<?php echo $fb; ?>" target="_blank" rel="nofollow"><i class="fab fa-facebook-f"></i></a>
                        <a href="<?php echo $ig; ?>" target="_blank" rel="nofollow"><i class="fab fa-instagram"></i></a>
                        <a href="<?php echo $tw; ?>" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a>
                        <a href="<?php echo $yt; ?>" target="_blank" rel="nofollow"><i class="fab fa-youtube"></i></a>
						<a href="<?php echo $tg; ?>" target="_blank" rel="nofollow"><i class="fab fa-telegram-plane"></i></a>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <!-- Main Menu -->
    <nav class="navbar navbar-expand-lg navbar-light fullwidth mt-4">
        <div class="container">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav">
                <img src="<?php echo THEME_URL.'/assets/img/'; ?>white-btn.png" class="desktop-toggle-img d-none d-md-block" alt="FABI" />
                <img src="<?php echo THEME_URL.'/assets/img/'; ?>blue-btn.png" class="mobile-toggle-img d-xs-block d-sm-block d-md-none m-auto" alt="FABI" />
            </button>

            <?php
                wp_nav_menu( array(
                    'theme_location'    => 'header-main',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'main-nav',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'            => new WP_Bootstrap_Navwalker(),
                ) );
            ?>

        </div>
    </nav>

</header>    
<!-- =========== Ends Header here ========== -->