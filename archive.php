<?php get_header();

    /*if(current_user_can('administrator')) {
        echo get_current_template();
    }*/

    global $wp_query;
    $queried_object = get_queried_object();

    /*echo '<pre>';
    //var_dump($wp_query);
    //var_dump($wp_query->query_vars['taxonomy']);
    //var_dump($wp_query->query_vars['term']);
    echo '</pre>';*/

    // Custom
    //$tax = 'regione';
    /*$slug = $queried_object->slug;
    echo $slug;*/
    //print_r($queried_object);
    $post_type_slug_of_archive = $queried_object->name;

    // Title
    if(isset($_GET['term'])) {
        //$post_type = get_post_type();
        $post_type_label = get_post_type_object( $post_type )->labels->name;
        $queried_tax_term_label = get_term_by( 'slug', $queried_tax_term, $queried_tax)->name;
        $queried_term_label = get_term_by( 'slug', $queried_term, 'tipologia_comunicato')->name;
        if($queried_term_label != '') $queried_term_label = ' - '.$queried_term_label;
        $page_name = $queried_tax_term_label.$queried_term_label;
    } elseif(is_page()) {
        $page_name = $queried_object->post_title;
    } elseif(is_post_type_archive()) {
        $page_term = '';
        $page_name .= $queried_object->labels->name;
        //$page_name .= ' - Regioni';
    } elseif(is_category()) {
        $page_name = single_cat_title( '', false );
        $cat_descr = category_description();
    } elseif(is_tax()) {
        $page_name = $queried_object->name;
    } elseif(is_archive()) {
        $page_name = get_the_archive_title('',''); // https://developer.wordpress.org/reference/functions/get_the_archive_title/
    }

    // DEBUG
    /*if(current_user_can('administrator')) {
        echo '<pre>';
        var_dump($queried_object);
        echo '</pre>';
    }*/

?>

<main class="full-width">

<div class="fullwidth breadcrumb-links">
    <div class="container">
        <nav aria-label="breadcrumb">
            <?php breadcrumbs(); ?>
        </nav>
    </div>
</div>

<div class="fullwidth about-content">
    <div class="container">
        <div class="row">

            <?php
                if (is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) :
                    $col_content = 'col-9';
                    $col_item = 'col-md-4 col-sm-6 col-12';
                    ?>
                    <div class="col-3 sidebar-sx">
                        <h4 class="visible-xs sidebar_toggle">Menù della Pagina</h4>
                        <div class="inner_sidebar">
                            <?php if (is_active_sidebar('sidebar-sx')) : dynamic_sidebar('sidebar-sx'); endif; ?>
                            <?php if (is_active_sidebar('sidebar-generale-sx')) : dynamic_sidebar('sidebar-generale-sx'); endif; ?>
                        </div>
                    </div>
                    <?php
                else:
                    $col_content = 'col-12';
                    $col_item = 'col-md-3 col-sm-4 col-xs-2';
                endif;
            ?>

            <div class="<?php echo $col_content; ?> the_grid">

                <!-- Titolo & Riassunto -->
                <div class="fullwidth top-heading">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2><?php echo $page_name; ?></span></h2>
                                <?php if(strlen($cat_descr) > 0) { ?>
                                    <p><?php $cat_descr; ?></p>
                                <?php } ?>
                            </div> 
                        </div>
                    </div>
                </div>

                <!-- ========================================================================================================
                    Grid
                ========================================================================================================= -->
                <div class="grid-bottom fullwidth">
                    <div class="row align-items-stretch">

                                <?php if (have_posts()) : while ( have_posts() ) : the_post();

                                    // Defaults
                                    $has_feat_image = true;
                                    $has_read_more = true;
                                
                                    // Post Data
                                    $post_type = get_post_type();

                                    //$taxonomy = get_post_taxonomies();
                                    $taxonomy = array('tipologia_comunicato', 'organizzazione_struttura', 'federazioni_regionali', 'gruppi_bancari', 'agenti_riscossione', 'tipologia_soggetto');
                                    $terms = wp_get_post_terms($post->ID, $taxonomy); 
                                       
                                    if ( $terms && ! is_wp_error( $terms ) ) :
                                        $terms_array = array();
                                        foreach ( $terms as $term ) {
                                            $term_link = get_term_link($term->term_id);
                                            $terms_array[] = '<a href="'.$term_link.'">'.$term->name.'</a>';
                                        }          
                                    endif;    
                                    $the_terms = join( ", ", $terms_array );
                                    
                                    // DEBUG
                                    /*echo '<pre>';
                                    var_dump($taxonomy);
                                    echo '</pre>';*/

                                    // File
                                    $upload = get_field('file_upload');
                                    $file_url = get_field('file_url');
                                    if($upload != '' && !is_null($upload)) {
                                        $url = $upload;
                                    } elseif($file_url != '' && !is_null($file_url)) {
                                        $url = $file_url;
                                    } else {
                                        $url = get_the_permalink(get_the_ID());
                                    }

                                    // Debug
                                    /*if(current_user_can('administrator')) {
                                        echo '<pre>';
                                        var_dump($url);
                                        echo '</pre>';
                                    }*/

                                    // Logo
                                    $banca = get_the_terms(get_the_ID(), 'gruppi_bancari');
                                    /*$banca_logo_id = get_term_meta($banca[0]->term_id, 'image', true);
                                    $banca_image_data = wp_get_attachment_image_src( $banca_logo_id, 'full' );
                                    $banca_logo = $banca_image_data[0];*/
                                    $banca_logo = get_field('term_img', 'gruppi_bancari_'.$banca[0]->term_id);

                                    // Video
                                    if(get_post_type() == 'multimedia') {
                                        $video = get_field('multimedia_video');
                                        if($video != '') {
                                            $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                        }
                                    } elseif(get_post_type() == 'post') {
                                        $video = get_field('video_feat');
                                        if($video != '') {
                                            $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                        }
                                    }
                                    ?>

                                    <?php

                                    // Comunicati
                                    if($post_type == 'comunicato') { ?>

                                        <div class="col-xs-12 col-6">
                                            <div class="grid-bottom-single risultato fullwidth">

                                                <div class="date">
                                                    <p>
                                                        <span class="float-left">
                                                            <?php echo $the_terms; ?>
                                                        </span>
                                                        <span class="float-right"><?php the_time('j/m/Y'); ?></span>
                                                    </p>
                                                </div>
                                                <?php if($banca_logo != '') { ?>
                                                    <div class="logo">
                                                        <img src="<?php echo wp_imager(BANCA_LOGO_W, BANCA_LOGO_H, 1, 'img-fluid', false, $banca_logo, true); ?>" alt="<?php echo $banca[0]->name;?>" title="<?php echo $banca[0]->name;?>" />
                                                    </div>
                                                <?php } ?>
                                                <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>
                                                <!-- <div class="date"><p><?php //echo $the_terms; ?></span></p></div> -->

                                            </div>
                                        </div>
                                    <?php         
                                    
                                    // Eventi
                                    } elseif($post_type == EVENTI) {
                                        
                                        // Event date
                                        $event_start = get_field('evento_inizio');
                                        if($event_start != '') {
                                            $date = str_replace('00:00', '', $event_start);
                                        } else {
                                            $date = get_the_time('j/m/Y');
                                        }

                                        // Event Città
                                        $event_citta = get_field('evento_citta');
                                        if($event_citta != '') {
                                            //$citta = ' | '.$event_citta;
                                            $citta = $event_citta;
                                        } else {
                                            $citta = '';
                                        }

                                        // File
                                        $upload = get_field('file_upload');
                                        $file_url = get_field('file_url');
                                        if($upload != '' && !is_null($upload)) {
                                            $url = $upload;
                                        } elseif($file_url != '' && !is_null($file_url)) {
                                            $url = $file_url;
                                        } else {
                                            //$url = get_the_permalink(get_the_ID());
                                            $url = '';
                                        }

                                        ?>

                                        <div class="col-md-4 col-sm-6 col-12">
                                            <div class="grid-bottom-single risultato fullwidth">

                                                <div class="date"><p><span class="float-left"><?php echo $citta; ?></span><span class="float-right"><?php echo $date; ?></span></p></div>
                                                <?php if($banca_logo != '') { ?>
                                                    <div class="logo">
                                                        <img src="<?php echo wp_imager(BANCA_LOGO_W, BANCA_LOGO_H, 1, 'img-fluid', false, $banca_logo, true); ?>" alt="<?php echo $banca[0]->name;?>" title="<?php echo $banca[0]->name;?>" />
                                                    </div>
                                                <?php } ?>
                                                <h3>
                                                    <?php if($url != '') { ?>
                                                        <a href="<?php echo $url; ?>"><?php the_title(); ?></a>
                                                    <?php } else { ?>
                                                        <?php the_title(); ?>
                                                    <?php } ?>
                                                </h3>
                                                <!-- <div class="date"><p><?php //echo $the_terms; ?></span></p></div> -->

                                            </div>
                                        </div>
                                    <?php 

                                    // Strutture Territorio                                   
                                    } elseif($post_type == 'strutture_territorio') {

                                        $has_feat_image = false;

                                        // Post Data
                                        $resp = get_field('struttura_responsabile');
                                        $url = get_field('struttura_web');
                                        $email = get_field('struttura_email');
                                        $indirizzo = get_field('struttura_indirizzo');
                                        $citta = get_field('struttura_citta');
                                        $cap = get_field('struttura_cap');
                                        $tel = get_field('struttura_telefono');
                                        $fax = get_field('struttura_fax');
                                        $caaf = get_field('struttura_caaf');
                                        $csf = get_field('struttura_csf');

                                        ?>

                                        <div class="col-md-6 col-sm-6 col-12">
                                            <div class="grid-bottom-single fullwidth">
                                                <h3><?php the_title(); ?></h3>
                                                <div class="struttura_info">

                                                </div>
                                            </div>
                                        </div>

                                        <?php


                                    // Generico
                                    } else { 

                                        $archive_click = false;

                                        // Pubblicazioni
                                        if($post_type == 'pubblicazione') {
                                            $pubb_terms = get_the_terms(get_the_ID(), 'tipologia_pubblicazioni');
                                            $archive_click = get_field('archive_click', 'tipologia_pubblicazioni_'.$pubb_terms[0]->term_id);
                                        }

                                        // Post
                                        if($post_type == 'post') {
                                            $post_terms = get_the_terms(get_the_ID(), 'category');
                                            $archive_click = get_field('archive_click', 'category_'.$post_terms[0]->term_id);
                                        }

                                        // Debug
                                        /*if(current_user_can('administrator')) {
                                            echo '<pre>';
                                            //var_dump();
                                            echo 'generic';
                                            echo '</pre>';
                                        }*/

                                        // File
                                        $upload = get_field('file_upload');
                                        $file_url = get_field('file_url');
                                        if($upload != '' && !is_null($upload)) {
                                            $url = $upload;
                                        } elseif($file_url != '' && !is_null($file_url)) {
                                            $url = $file_url;
                                        } else {
                                            $url = get_the_permalink(get_the_ID());
                                        }

                                        ?>
                                        <div class="<?php echo $col_item; ?>">
                                            <div class="grid-bottom-single fullwidth">

                                                <?php if($has_feat_image) { ?>
                                                    <div class="grid-img">

                                                        <?php if($archive_click) { ?>

                                                            <?php
                                                                // Replace feat img with video thumb if content is = ''
                                                                if($video != '') {
                                                                    echo '<a href="'.$url.'" target="_blank"><img src="'.wp_imager(320, 218, 1, 'img-fluid', false, $video_thumb, true).'"></a>';
                                                                } else {
                                                                    echo '<a href="'.$url.'" target="_blank"><img src="'.wp_imager(320, 218, 1, 'img-fluid', false, null, true).'"></a>';
                                                                }
                                                            ?>

                                                            <?php } else { ?>

                                                                <?php
                                                                    // Replace feat img with video thumb if content is = ''
                                                                    if($video != '') {
                                                                        echo wp_imager(320, 218, 1, 'img-fluid', false, $video_thumb);
                                                                    } else {
                                                                        echo wp_imager(320, 218, 1, 'img-fluid');
                                                                    }
                                                                ?>

                                                            <?php } ?>
                                                            
                                                    </div>
                                                <?php } ?>
                                                
                                                <?php
                                                    if($archive_click) { ?>
                                                        <h3><a href="<?php echo $url; ?>" target="_blank"><?php the_title(); ?></a></h3>
                                                    <?php } else { ?>
                                                        <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>
                                                    <?php }
                                                ?>

                                                <?php $has_read_more = false; if($has_read_more) { ?>
                                                    <div class="anchor-div">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <span class="a-img"><i class="far fa-angle-right"></i></span>
                                                            <span class="a-text">approfondisci</span>
                                                        </a>
                                                    </div>
                                                <?php } ?>

                                            </div>
                                        </div>
                                        <?php
                                    
                                    } ?>

                                <?php endwhile; endif; ?>

                                <!--Pagination-->
                                <div class="col-12 pagination_wrapper">
                                    <?php the_posts_pagination( array(
                                        'mid_size' => 2,
                                        'prev_text' => '&laquo;',
                                        'next_text' => '&raquo;',
                                    ) ); ?>
                                </div>
                                <!--End Pagination-->

                    </div>
                </div>
                <!-- ========================================================================================================
                    End of Grid
                ========================================================================================================= -->

                
            </div>

        </div>
    </div>
</div>

</main>
<?php get_footer(); ?>