<?php get_header();

$excerpt_lenght = 200;
?>

<main class="full-width">
    
    <!-- ========================================================================================================
        Slider
    ========================================================================================================= -->
    <?php
        // Check if values are cached, if not cache them
        $home_top_slider_transient = THEME_SLUG.'_home_top_slider_3h';
        delete_transient($home_top_slider_transient);

        if(get_transient($home_top_slider_transient) === false) {
            $home_top_slider_loop = new WP_Query(array(
                'posts_per_page' => 5,
                'post_type' => array('post'),
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key' => 'hp_slider',
                        'value' => '0',
                        'compare' => '>',
                    )
                )
            ));
            //Cache Results
            set_transient($home_top_slider_transient, $home_top_slider_loop, 3 * HOUR_IN_SECONDS );
        }
        $home_top_slider_loop = get_transient($home_top_slider_transient);
        ?>

        <?php if ($home_top_slider_loop->have_posts()) : ?>

            <div class="top-slider fullwidth">
                <div class="container">
                    <div class="top-slider-inner fullwidth">
                        <div class="owl-carousel main-silder owl-theme">
                                                  
                            <?php while ( $home_top_slider_loop->have_posts() ) : $home_top_slider_loop->the_post(); ?>

                                <!-- Slide -->
                                <div class="row d-flex flex-row-reverse m-0">
                                    <div class="col-md-6 col-sm-12 col-12 slide-img-col p-0">
                                        <?php echo wp_imager(700, 450, 1, 'img-fluid'); ?>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-12 slider-text-col">
                                        <div class="slider-text fullwidth">
                                            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                                            <p><?php echo custom_content(300); ?></p>
                                            <div class="anchor-div">
                                                <a href="<?php the_permalink(); ?>">
                                                    <span class="a-img"><i class="far fa-angle-right"></i></span>
                                                    <span class="a-text">approfondisci</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; ?>

                        </div>
                    </div>
                </div>
            </div>
        
        <?php endif; wp_reset_query(); wp_reset_postdata(); ?>
        
    <!-- ========================================================================================================
        End of Slider
    ========================================================================================================= -->

    <!-- ========================================================================================================
        Tabs
    ========================================================================================================= -->
    <div class="tab-section fullwidth home_tabs">
        <div class="container">

            <ul class="nav tab-ul fullwidth">
                <li>
                    <a class="active" data-toggle="tab" href="#tab-1">Altre Notizie</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-2">Comunicati</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-3">Appuntamenti</a>
                </li>
            </ul>

            <div class="tab-content fullwidth">
                
                <!-- Ultimissime -->
                <div class="tab-pane fade show active fullwidth ultimissime" id="tab-1">    
                    <div class="row"> 
                       
                        <?php
                            $i = 0;
                            // ULTIMISSIME - ALTRE NOTIZIE

                            // Check if values are cached, if not cache them
                            $home_tabs_1_transient = THEME_SLUG.'_home_tabs_1_3h';
                            delete_transient($home_tabs_1_transient);
                            if(get_transient($home_tabs_1_transient) === false) {
                                $home_tabs_1_loop = new WP_Query(array(
                                    'posts_per_page' => 8,
                                    'post_type' => 'post',
                                    'category_name' => 'news',
                                    'post_status' => 'publish',
                                    'meta_query' => array(
                                        'relation' => 'OR',
                                        array(
                                            'key' => 'hp_slider',
                                            //'value' => '',
                                            'compare' => 'NOT EXISTS',
                                            //'type' => 'numeric'
                                        ),
                                        array(
                                            'key' => 'hp_slider',
                                            'value' => '0',
                                            'compare' => '=',
                                        )
                                    )
                                ));
                                //Cache Results
                                set_transient($home_tabs_1_transient, $home_tabs_1_loop, 3 * HOUR_IN_SECONDS );
                            }
                            $home_tabs_1_loop = get_transient($home_tabs_1_transient);
                            ?>

                            <?php if ($home_tabs_1_loop->have_posts()) : while ( $home_tabs_1_loop->have_posts() ) : $home_tabs_1_loop->the_post();
                                $i++;
                                if($i == 1 || $i == 2) {
                                    $div_block = 6;
                                } else {
                                    $div_block = 4;
                                }                               
                                       
                                // Post Data
                                $taxonomy = get_post_taxonomies();
                                foreach ($taxonomy as $tax ) {
                                    $terms = get_the_terms( get_the_ID(), $tax);
                                }
                                if ( $terms && ! is_wp_error( $terms ) ) :
                                    $terms_array = array();
                                    foreach ( $terms as $term ) {
                                        $term_link = get_term_link($term->term_id);
                                        $terms_array[] = '<a href="'.$term_link.'">'.$term->name.'</a>';
                                    }          
                                endif;    
                                $the_terms = join( ", ", $terms_array );

                                // Video
                                if(get_post_type() == 'multimedia') {
                                    $video = get_field('multimedia_video');
                                    if($video != '') {
                                        $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                    }
                                } elseif(get_post_type() == 'post') {
                                    $video = get_field('video_feat');
                                    if($video != '') {
                                        $video_thumb = custom_get_youtube_data($video)['video_thumbnail_url'];
                                    }
                                }

                                // Image
                                $image = wp_imager(570, 350, 1, 'img-fluid', false, false, true);
                                //var_dump($image);

                                ?>

                                <div class="col-xs-6 col-sm-6 col-md-<?php echo $div_block; ?> col-12 item">

                                        <div class="tab-img-div fullwidth">
                                            <?php
                                                // Replace feat img with video thumb if content is = ''
                                                if($video_thumb != '') {
                                                    echo wp_imager(570, 350, 1, 'img-fluid', false, $video_thumb);
                                                } elseif($image != '' || !is_null($image)) {
                                                    echo wp_imager(570, 350, 1, 'img-fluid');
                                                } else {

                                                }
                                            ?>
                                        </div>

                                        <div class="date"><p><span><?php the_time('j/m/Y'); ?></span> | <?php echo $the_terms; ?></p></div>
                                        <h2>
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h2>
                                        <?php if(strlen(get_the_excerpt()) > 0) { ?>
                                            <?php echo shorten(get_the_excerpt(), $excerpt_lenght); ?>
                                        <?php } ?>
                                        <div class="anchor-div">
                                            <a href="<?php the_permalink(); ?>">
                                                <span class="a-img"><i class="far fa-angle-right"></i></span>
                                                <span class="a-text">approfondisci</span>
                                            </a>
                                        </div>
                                </div>
                            <?php endwhile; endif; wp_reset_query(); wp_reset_postdata(); 
                                
                        ?>

                    </div>

                    <div class="text-center fullwidth my-3">
                        <a href="<?php bloginfo('url'); ?>/news" class="tab-bottom-btn">Vedi tutte</a>
                    </div>

                </div>
                
                <!-- Comunicati -->
                <div class="tab-pane fade comunicati" id="tab-2">
                    <div class="row">
                        
                        <?php
                            // COMUNICATI

                            // Check if values are cached, if not cache them
                            $home_tabs_2_transient = THEME_SLUG.'_home_tabs_2_3h';
                            delete_transient($home_tabs_2_transient);

                            if(get_transient($home_tabs_2_transient) === false) {
                                $home_tabs_2_loop = new WP_Query(array(
                                    'posts_per_page' => 9,
                                    'post_type' => array('comunicato'),    
                                    'post_status' => 'publish',
                                    /*'tax_query' => array(
                                        array(
                                            'taxonomy' => 'tipologia_comunicato',
                                            'field'    => 'slug',
                                            'terms'    => 'stampa',
                                        ),
                                    ),*/
                                ));
                                //Cache Results
                                set_transient($home_tabs_2_transient, $home_tabs_2_loop, 3 * HOUR_IN_SECONDS );
                            }
                            $home_tabs_2_loop = get_transient($home_tabs_2_transient);
                            ?>

                            <?php if ($home_tabs_2_loop->have_posts()) : while ( $home_tabs_2_loop->have_posts() ) : $home_tabs_2_loop->the_post();
                                       
                                // Post Data
                                $taxonomy = get_post_taxonomies();
                                foreach ($taxonomy as $tax ) {
                                    $terms = get_the_terms( get_the_ID(), $tax);
                                }
                                if ( $terms && ! is_wp_error( $terms ) ) :
                                    $terms_array = array();
                                    foreach ( $terms as $term ) {
                                        $term_link = get_term_link($term->term_id);
                                        $terms_array[] = '<a href="'.$term_link.'">'.$term->name.'</a>';
                                    }          
                                endif;    
                                $the_terms = join( ", ", $terms_array );

                                // File
                                $upload = get_field('file_upload');
                                $file_url = get_field('file_url');
                                if($upload != '' && !is_null($upload)) {
                                    $url = $upload;
                                } elseif($file_url != '' && !is_null($file_url)) {
                                    $url = $file_url;
                                } else {
                                    $url = get_the_permalink(get_the_ID());
                                }
                                ?>
                                <div class="col-xs-6 col-sm-6 col-md-4 col-12 item">
                                    <div class="date"><p><span><?php the_time('j/m/Y'); ?></span> | <?php echo $the_terms; ?></p></div>
                                    <h2>
                                        <a href="<?php echo $url; ?>"><?php the_title(); ?></a>
                                    </h2>
                                    <!-- <p><?php //echo custom_excerpt($excerpt_lenght, ''); ?></p> -->
                                    <!-- <div class="anchor-div">
                                        <a href="<?php //echo $url; ?>">
                                            <span class="a-img"><i class="far fa-angle-right"></i></span>
                                            <span class="a-text">approfondisci</span>
                                        </a>
                                    </div> -->
                                </div>
                            <?php endwhile; endif; wp_reset_query(); wp_reset_postdata(); 
                                
                        ?>

                    </div>

                    <div class="text-center fullwidth my-3">
                        <a href="<?php bloginfo('url'); ?>/comunicato" class="tab-bottom-btn">Vedi tutti</a>
                    </div>

                </div>
                
                <!-- Appuntamenti -->
                <div class="tab-pane fade appuntamenti" id="tab-3">
                    <div class="row">
                        
                        <?php
                            // EVENTI

                            // Check if values are cached, if not cache them
                            $home_tabs_3_transient = THEME_SLUG.'_home_tabs_3_3h';
                            delete_transient($home_tabs_3_transient);

                            if(get_transient($home_tabs_3_transient) === false) {
                                $home_tabs_3_loop = new WP_Query(array(
                                    'posts_per_page' => 9,
                                    'post_type' => array('evento'),
                                    'post_status' => 'publish',
                                    'meta_key' => 'evento_inizio',
                                    'orderby' => 'meta_value',
                                    'order' => 'DESC',
                                    /*'tax_query' => array(
                                            array(
                                                'taxonomy' => 'tipologia_evento',
                                                'field'    => 'slug',
                                                'terms'    => 'stampa',
                                            )
                                    )*/
                                ));
                                //Cache Results
                                set_transient($home_tabs_3_transient, $home_tabs_3_loop, 3 * HOUR_IN_SECONDS );
                            }
                            $home_tabs_3_loop = get_transient($home_tabs_3_transient);
                            ?>

                            <?php if ($home_tabs_3_loop->have_posts()) : while ( $home_tabs_3_loop->have_posts() ) : $home_tabs_3_loop->the_post();
                                    
                                // Post Data
                                /*$taxonomy = get_post_taxonomies();
                                foreach ($taxonomy as $tax ) {
                                    $terms = get_the_terms( get_the_ID(), $tax);
                                }
                                if ( $terms && ! is_wp_error( $terms ) ) :
                                    $terms_array = array();
                                    foreach ( $terms as $term ) {
                                        $term_link = get_term_link($term->term_id);
                                        $terms_array[] = '<a href="'.$term_link.'">'.$term->name.'</a>';
                                    }          
                                endif;    
                                $the_terms = join( ", ", $terms_array );*/

                                // Event date
                                $event_start = get_field('evento_inizio');
                                if($event_start != '') {
                                    $event_start = str_replace('00:00', '', $event_start);
                                } else {
                                    $event_start = get_the_time('j/m/Y');
                                }

                                // Event Città
                                $event_citta = get_field('evento_citta');
                                if($event_citta != '') {
                                    $citta = ' | '.$event_citta;
                                } else {
                                    $citta = '';
                                }

                                // File
                                $upload = get_field('file_upload');
                                $file_url = get_field('file_url');
                                if($upload != '' && !is_null($upload)) {
                                    $url = $upload;
                                } elseif($file_url != '' && !is_null($file_url)) {
                                    $url = $file_url;
                                } else {
                                    $url = get_the_permalink(get_the_ID());
                                }

                                // Image
                                $image = wp_imager(570, 350, 1, 'img-fluid', false, false, true);
                                ?>

                                <div class="col-xs-6 col-sm-6 col-md-4 col-12 item">
                                    <div class="tab-img-div fullwidth">
                                        <?php
                                            if($image != '' || !is_null($image)) {
                                                    echo wp_imager(570, 350, 1, 'img-fluid');
                                            } else {

                                            }
                                        ?>
                                    </div>
                                    <div class="date"><i class="fal fa-calendar-alt"></i><p><span><?php echo $event_start ?><?php echo $citta; ?></span></p> <!-- | <?php //echo $the_terms; ?> --></div>
                                    <h2><!-- <a href="<?php echo $url; ?>"> --><?php the_title(); ?><!-- </a> --></h2>
                                    <?php
                                        $the_content = get_the_content();
                                        if($the_content != '') {
                                            echo custom_excerpt(16);
                                        }
                                    ?>
                                    <!-- <p><?php //echo custom_excerpt($excerpt_lenght); ?></p> -->
                                    <!-- <div class="anchor-div">
                                        <a href="<?php //the_permalink(); ?>">
                                            <span class="a-img"><i class="far fa-angle-right"></i></span>
                                            <span class="a-text">approfondisci</span>
                                        </a>
                                    </div> -->
                                </div>

                            <?php endwhile; endif; wp_reset_query(); wp_reset_postdata(); 
                                
                        ?>  

                    </div>

                    <div class="text-center fullwidth my-3">
                        <a href="<?php bloginfo('url'); ?>/evento" class="tab-bottom-btn">Vedi tutti</a>
                    </div>

                </div>

            </div>

            <!-- <div class="text-center fullwidth my-3">
                <a href="#" class="tab-bottom-btn">Leggi tutte</a>
            </div> -->
            
        </div>
    </div>
    <!-- ========================================================================================================
        End of Tabs
    ========================================================================================================= -->

    <!-- ========================================================================================================
        Video Slider
    ========================================================================================================= -->
    <?php
        // VIDEO SLIDER

        // VIDEO DA FABI.TV
        // http://fabitv.it/video/endpoint/json

        // API YOUTUBE
        // AIzaSyAM8TWHbMNjko1STrGYgcTUCJpnggq2-xE

        // OAUTH
        // Client ID
        // 440110863554-dma8i5lrodsi7b181prvj20sk073dluu.apps.googleusercontent.com
        // Secret
        // f0NwoYkLi8SV8K4VYCOMWGb-

        // YOUTUBE
        // https://www.youtube.com/user/SINDACATOFABI/videos
        // Channel ID: UCEkuqjoD3NVJac3OKTtKWuA

		// https://stackoverflow.com/questions/50097600/youtube-video-list-in-json-format
        
        $youtube_home_transient = THEME_SLUG.'_youtube_home_6h';
        //delete_transient($youtube_home_transient);

        if(is_null($youtube_json) || get_transient($youtube_home_transient) === false) {
            $video_sources = get_field('video_sources', 'options');

            //Youtube
            $google_api_data = get_field('google_api_wrap', 'options');
            $API_key    =  $google_api_data['google_youtube_api'];
            $channelID  = $video_sources['video_source_youtube'];
            $maxResults = '';
            $maxResultsNum = 5;
            if($maxResultsNum != '') {
                $maxResults = '&maxResults='.$maxResultsNum;
            }
            $youtube_json = json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='.$channelID.$maxResults.'&key='.$API_key), TRUE)['items'];
            
            //Cache Results
            set_transient($youtube_home_transient, $youtube_json, 6 * HOUR_IN_SECONDS );
        }
        $youtube_json = get_transient($youtube_home_transient);

        // Debug
        /*if(current_user_can('administrator')) {
            echo '<pre>';
            var_dump('https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='.$channelID.$maxResults.'&key='.$API_key);
            echo '</pre>';
        }*/
		
		$youtube_list = array();
		$video = '';
		$x=0;
		foreach($youtube_json['items'] as $video) {

            // Debug
            /*if(current_user_can('administrator')) {
                echo '<pre>';
                var_dump($video);
                echo '</pre>';
            }*/

			$x++;
			$youtube_list[$x]['ID'] = $video['id']['videoId'];
			//$youtube_list['date'] = $video['snippet']['publishedAt']; // 2019-01-08T15:36:46.000Z
			$youtube_list[$x]['timestamp'] = strtotime($video['snippet']['publishedAt']);
			$youtube_list[$x]['date'] = date_i18n( get_option( 'date_format' ), $youtube_list[$x]['timestamp']);
            $youtube_list[$x]['title'] = $video['snippet']['title'];
            $youtube_list[$x]['descr'] = $video['snippet']['description'];
			$youtube_list[$x]['thumb'] = $video['snippet']['thumbnails']['high']['url'];
			$youtube_list[$x]['url'] = 'https://www.youtube.com/watch?v='.$youtube_list[$x]['ID'];
            $youtube_list[$x]['source'] = 'youtube';
            
            if($x > $maxResults) break;
        }
        
        // Debug
        /*if(current_user_can('administrator')) {
            echo '<pre>';
            echo '<br><strong>YOUTUBE</strong><br/>';
            var_dump($channelID);
            var_dump($maxResults);
            var_dump($API_key);
            var_dump('https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='.$channelID.$maxResults.'&key='.$API_key);
            var_dump($youtube_json);
            echo '</pre>';
        }*/
        /*if(current_user_can('administrator')) {
            echo '<pre>';
            var_dump($youtube_list);
            echo '</pre>';
        }*/

		// FABI list
		$maxResults = 5;
        $fabitv_json = json_decode(file_get_contents($video_sources['video_source_json'].'?maxResults='.$maxResults), TRUE)['items'];
        
        // Debug
        /*if(current_user_can('administrator')) {
            echo '<pre>';
            echo 'FABI TV JSON<br>';
            var_dump($fabitv_json);
            echo '</pre>';
        }*/

		$fabi_list = array();
		$video = '';
		$x=0;
		foreach($fabitv_json as $video) {
			$x++;
			$fabi_list[$x]['ID'] = $video['id'];
			//$fabi_list['date'] = $video['publish_at']; // 2012-10-15 14:20:27
			$fabi_list[$x]['timestamp'] = strtotime($video['publish_at']);
			$fabi_list[$x]['date'] = date_i18n( get_option( 'date_format' ), $fabi_list[$x]['timestamp']);
            $fabi_list[$x]['title'] = $video['title'];
            $fabi_list[$x]['descr'] = $video['description'];
			$fabi_list[$x]['thumb'] = $video['video_poster'];
			$fabi_list[$x]['url'] = $video['video_url'][0];
            $fabi_list[$x]['source'] = 'fabitv';

            /*if(current_user_can('administrator')) {
                echo '<pre style="width:100%;">';
                var_dump($video['video_url'][0]);
                echo '<br> ---------- <br>';
                //var_dump($video);
                echo '</pre>';
            }*/
            
            if($x > $maxResults) break;
        }
        
        foreach ($fabi_list as $key => $node) {
            $timestamps[$key] = $node['timestamp'];
        }
        array_multisort($timestamps, SORT_DESC, $fabi_list);

        // Debug
        /*if(current_user_can('administrator')) {
            echo '<pre>';
            var_dump($fabi_list);
            echo '</pre>';
        }*/

		$videoList = array();
        $videoList = array_merge($fabi_list, $youtube_list);

		// Sort video array by timestamp
		function sortArrayBy_Timestamp($a1, $a2){
			if ($a1['timestamp'] == $a2['timestamp']) return 0;
			return ($a1['timestamp'] > $a2['timestamp']) ? -1 : 1;
		}
		usort($videoList, "sortArrayBy_Timestamp");

		/*if(current_user_can('administrator')) {
			//echo '<pre style="width:100%;">YOUTUBE';
			//var_dump($youtube_list);
			//var_dump($youtube_json['items']);
			//echo '</pre>';
			//echo '<pre style="width:100%;">FABI';
			//var_dump($fabi_list);
			//var_dump($fabitv_json);
			//echo '</pre>';
			echo '<pre style="width:100%;">VIDEOLIST';
			var_dump($videoList);
			echo '</pre>';
		}*/
       
        // Debug
        if(current_user_can('administrator')) {
            echo '<pre>';
            var_dump($videoList);
            echo '</pre>';
        }
        ?>

        <?php if (!empty($videoList)) : ?>
        
            <div class="video-slider-section fullwidth">
                <div class="container">
                    <div class="video-slider fullwidth">
                        <div id="video-owl" class="owl-carousel owl-theme video-owl">
            
                            <?php
								$video = '';
                                foreach($videoList as $video) {

                                    /*if(current_user_can('administrator')) {
                                        echo '<pre style="width:100%;">';
                                        var_dump($video);
                                        echo '</pre>';
                                    }*/

                                    // Video
                                    $video_id = $video['ID'];
                                    $video_date = $video['date'];
                                    $video_title = $video['title'];
                                    $video_descr = $video['descr'];
									$video_thumb = $video['thumb'];
									$video_url = $video['url'];
                                    ?>

                                    <div class="video-single">
                                      <div class="video-thumb">
                                          
                                          <a data-fancybox data-width="640" data-height="360" href="<?php echo $video_url; ?>">
                                          	
											<?php echo wp_imager(570, 350, 1, 'img-fluid video_thumb', false, $video_thumb); ?>
                                          	<img class="play-btn" src="<?php echo THEME_URL.'/assets/img/'; ?>play-btn.png" alt="icon" />
                                          </a>
                                      </div>
                                      <div class="video-single-text">
											<div class="date"><p><span><?php echo $video_date; ?></span></p></div>
											<h2><a data-fancybox data-width="640" data-height="360" href="<?php echo $video_url; ?>"><?php echo $video_title; ?></a></h2>
											<?php
												if (strlen($video_descr) > 101) {
													$maxLength = 100;
													$video_descr = substr($video_descr, 0, $maxLength);
												}
												echo strip_tags($video_descr).' &hellip;';
											?>
                                      </div>
                                  </div>
                                
                                <?php }
                            ?>

                        </div>
                        <span id="pt-back" class="ctrl-btn"></span>
                        <span id="pt-next" class="ctrl-btn"></span>
                    </div>
                </div>
            </div>

            <div class="text-center fullwidth my-3">
                <a href="<?php bloginfo('url'); ?>/video" class="tab-bottom-btn">Vedi tutti</a>
            </div>
                            
        <?php endif; 
    ?>
    <!-- ========================================================================================================
        End of Video Slider
    ========================================================================================================= -->

    <!-- ========================================================================================================
        Bottom Grid
    ========================================================================================================= -->
    <div class="grid-bottom fullwidth">
        <div class="container">
            <div class="row align-items-stretch">
                
                <div class="col-md-8 col-sm-12 col-12">
                    <div class="row">
                        <?php if (is_active_sidebar('home-sidebar-centrale')) : dynamic_sidebar('home-sidebar-centrale'); endif; ?>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-12 side-col">
                    <div class="side-div d-flex align-items-center">
                        <?php if (is_active_sidebar('desktop_home-sidebar-basso-dx')) : dynamic_sidebar('desktop_home-sidebar-basso-dx'); endif; ?>
                        <?php if (is_active_sidebar('mobile_home-sidebar-basso-dx')) : dynamic_sidebar('mobile_home-sidebar-basso-dx'); endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- ========================================================================================================
        End of Bottom Grid
    ========================================================================================================= -->
    
    <!-- Sidebar Pulsanti footer-blu-buttons -->
    <div class="bottom-btns fullwidth footer_alto">
        <div class="container">

            <?php if (is_active_sidebar('home-footer-sidebar-alto')) : dynamic_sidebar('home-footer-sidebar-alto'); endif; ?>

        </div>
    </div>

</main>

<?php get_footer(); ?>