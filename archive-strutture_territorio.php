<?php get_header();

    global $wp_query;
    $queried_object = get_queried_object();

    // Custom
    $tax = 'regione';

    // Title
    if(is_post_type_archive()) {
        $page_term = '';
        $page_name .= $queried_object->labels->name;
        $page_name .= ' - Regioni';
    } else {
        $page_term = '<p>'.get_the_archive_title('','').'</p>'; // https://developer.wordpress.org/reference/functions/get_the_archive_title/
        $page_term = '<p>'.single_cat_title( '', false ).'</p>';
        $post_type = get_post_type();
        $page_name = get_post_type_object( $post_type )->labels->name;
        //var_dump($post_type_obj);
    }

    // Layout
    $content_col = 12;
    $sidebar_col = '';
    if(is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) {
        $content_col = 9;
        $sidebar_col = 3;
    }

    // DEBUG
    /*if(current_user_can('administrator')) {
        echo '<pre>';
        var_dump($queried_object);
        echo '</pre>';
    }*/

?>


<main class="full-width">

    <div class="fullwidth breadcrumb-links">
        <div class="container">
            <nav aria-label="breadcrumb">
                <?php breadcrumbs(); ?>
            </nav>
        </div>
    </div>

    <div class="fullwidth about-content">
        <div class="container">
            <div class="row">

                <?php
                    // Sidebar
                    if (is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) :?>
                        <div class="col-<?php echo $sidebar_col; ?> sidebar-sx">
                            <h4 class="visible-xs sidebar_toggle">Menù della Pagina</h4>
                            <div class="inner_sidebar">
                                <?php if (is_active_sidebar('sidebar-sx')) : dynamic_sidebar('sidebar-sx'); endif; ?>
                                <?php if (is_active_sidebar('sidebar-generale-sx')) : dynamic_sidebar('sidebar-generale-sx'); endif; ?>
                            </div>
                        </div>
                    <?php
                    endif;
                ?>
                
                <div class="content col-<?php echo $content_col; ?>">
                    <div class="row">

                        <!-- Titolo & Riassunto -->
                        <div class="col-md-12 top-heading">
                            <h2><?php echo $page_name; ?></h2>
                            <?php echo $page_term;?>
                        </div>

                         <!-- Contenuto -->
                         <div class="col-12">
                            <?php
                                // Mostra termini
                                $terms = get_terms( array(
                                    'taxonomy'     => $tax,
                                    'hide_empty'   => false,
                                    'orderby'      => 'name',
                                    'order'        => 'ASC',
                                    'parent'       => 0,
                                ) );
                            
                                // DEBUG
                                /*echo '<pre>';
                                var_dump($terms);
                                echo '</pre>';*/

                                if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
                                    <ul class="lista_termini">

                                        <?php foreach($terms as $term) {
                                            $term_link = get_term_link($term->term_id,'regione');
                                            echo '<li>';
                                            echo '<a href="'.$term_link.'">'.$term->name.'</a>';
                                            echo '</li>';

                                        } ?>

                                    </ul>
                                <?php }
                            ?>
                        </div>
                    
                    </div>
                </div>

            </div>
        </div>
    </div>


</main>
<?php get_footer(); ?>