<?php get_header();

if (have_posts()) : while (have_posts()) : the_post();

// Post Data
$post_type = get_post_type();

$video_feat = get_field('video_feat');
if($video_feat != '') {
    //$video_thumb = custom_get_youtube_data($video_feat)['video_thumbnail_url'];
    $video_id = custom_get_youtube_data($video_feat)['video_id'];
}

$multimedia_video = get_field('multimedia_video');
if($multimedia_video != '') {
    //$video_thumb = custom_get_youtube_data($video_feat)['video_thumbnail_url'];
    $video_id = custom_get_youtube_data($multimedia_video)['video_id'];
}

$multimedia_foto = get_field('multimedia_foto');

if($post_type == EVENTI) {
                                        
    // Event date
    $event_start = get_field('evento_inizio');
    if($event_start != '') {
        $date = str_replace('00:00', '', $event_start);
    } else {
        $date = get_the_time('j/m/Y');
    }

    // Event Città
    $event_citta = get_field('evento_citta');
    if($event_citta != '') {
        //$citta = ' | '.$event_citta;
        $citta = $event_citta;
    } else {
        $citta = '';
    }
}

// Layout
$content_col = 12;
$sidebar_col = '';
if(is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) {
    $content_col = 9;
    $sidebar_col = 3;
}

?>  

<main class="full-width landing-page">

    <div class="fullwidth breadcrumb-links">
        <div class="container">
            <nav aria-label="breadcrumb">
                <?php breadcrumbs(); ?>
            </nav>
        </div>
    </div>

    <div class="fullwidth about-content">
        <div class="container">
            <div class="row">

                <?php if(is_active_sidebar('sidebar-sx') || is_active_sidebar('sidebar-generale-sx')) : ?>
                    <!-- Sidebar -->
                    <div class="col-<?php echo $sidebar_col; ?> sidebar-sx">
                        <h4 class="visible-xs sidebar_toggle">Menù della Pagina</h4>
                        <div class="inner_sidebar">
                            <?php if (is_active_sidebar('sidebar-sx')) : dynamic_sidebar('sidebar-sx'); endif; ?>
                            <?php if (is_active_sidebar('sidebar-generale-sx')) : dynamic_sidebar('sidebar-generale-sx'); endif; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="content col-<?php echo $content_col; ?>">
                    <div class="row">

                        <!-- Titolo & Riassunto -->
                        <div class="col-md-12 top-heading">
                            <h2><?php the_title(); ?></span></h2>
                            <?php if(strlen($post->post_excerpt) > 0) {
                                the_excerpt();
                            } ?>
                            <?php if($date != '' || $citta != '') { ?>
                                <p class="date">Data Evento: <?php echo $date; ?> - Città: <?php echo $citta; ?></p>
                            <?php } ?>
                        </div>

                        <!-- Social -->
                        <?php if($post_type == 'post' || $post_type == EVENTI) { ?>
                            <div class="col-12 order2">
                                <div class="print-and-media">
                                    <div class="socialmediaicons">
                                        <?php echo do_shortcode('[addtoany]'); ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php

                            // MULTIMEDIA
                            if($post_type == 'multimedia') { ?>
                    
                                <div class="col-12 col-md-12 col-sm-12 order1 feat_media">

                                    <?php
                                        // Video
                                        if($multimedia_video != '') {
                                            ?>
                                            <iframe width="100%" height="600" src="https://www.youtube-nocookie.com/embed/<?php echo $video_id; ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        <?php }

                                        // Gallery
                                        elseif($multimedia_foto != '') {
                                            ?>
                                            
                                            <div class="video-slides news-gallery row">
                                                <?php
                                                    foreach($multimedia_foto as $photo) {
                                                        $w = $photo['sizes']['large-width'];
                                                        $h = $photo['sizes']['large-height'];
                                                        //print_r($photo['sizes']);
                                                        ?>
                                                        <div class="col-2 col-xs-6 gallery_item">
                                                            <a class="fancybox" data-fancybox-group="gallery" href="<?php echo $photo['url']; ?>" data-width="<?php echo $w; ?>" data-height="<?php echo $h; ?>">
                                                                <div class="gallery-img">
                                                                    <?php echo wp_imager(164, 142, 1 , 'img-fluid', false, $photo['url']); ?>   
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <?php
                                                    } 
                                                ?>                                        
                                            </div>

                                        <?php } 

                                            // Nothing ?
                                            else {
                                            echo wp_imager(700, 332, 1, 'img-about img-fluid', false);
                                        }
                                    ?>
                                </div>

                                <?php
                                    $the_content = get_the_content();
                                    if($the_content != '') { ?>  
                                
                                        <div class="col-12 col-md-12 col-sm-12 order3 the_content">
                                            <?php the_content(); ?>
                                        </div>
                                    <?php }


                            // STRUTTURA NAZIONALW
                            } elseif($post_type == 'struttura') {
                                
                                if(isset($_GET['org']) || isset($_GET['type'] )) {  ?>
                                    <!-- Form -->
                                    <!-- <div class="search_form">
                                        <form method="get">
                                            <input type="hidden" name="site-url" value="<?php echo get_bloginfo('url'); ?>" />
                                            <input type="hidden" name="post_type" value="<?php echo $_GET['type']; ?>" />

                                            <div class="form-row">
                                                <div class="col">
                                                        <?php
                                                            //echo '<input class="form-control" name="keyword" value="'.$queried_keyword.'" placeholder="Parola chiave..." />';
                                                        ?>
                                                </div>
                                                <div class="col">
                                                    <button class="btn btn-primary btn-block" type="submit">Cerca</button>
                                                </div>

                                            </div>
                                        </form>
                                    </div> -->
                                <?php } ?>

                                <div class="col-12 col-md-12 col-sm-12 order4">
                              
                                    <?php

                                        // Risultati ricerca Struttura / Comunicato
                                        if(isset($_GET['type']) && $_GET['type'] != '') {

                                            $type = $_GET['type'];
                                            $org = $_GET['org'];

                                            // Quale org?
                                            if(!isset($_GET['org']) && $_GET['org'] == '') {
                                                $org = get_the_terms( get_the_ID(), 'organizzazione_struttura' )[0]->slug;
                                            }

                                            // Order
                                            if($type == 'comunicato') {
                                                $orderby = 'date';
                                                $order = 'DESC';
                                            } elseif($type == 'evento') {
                                                $orderby = 'date';
                                                $order = 'DESC';
                                            } else {
                                                $orderby = 'name';
                                                $order = 'ASC';
                                            }

                                            // The Loop
                                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                            $search_query_args = array(
                                                'posts_per_page' => 100,
                                                'paged' => $paged,
                                                'post_type' => $type,
                                                'orderby' => $orderby,
                                                'order' => $order,
                                                //'s' => $queried_keyword,
                                                'tax_query' => array(
                                                    array(
                                                        'taxonomy' => 'organizzazione_struttura',
                                                        'field' => 'slug',
                                                        'terms' => $org
                                                    ),
                                                    /*'relation' => 'AND',
                                                    array(
                                                        'taxonomy' => $queried_tax,
                                                        'field' => 'slug',
                                                        'terms' => $queried_tax_term,
                                                    ),*/
                                                ),
                                            );

                                            //var_dump($search_query_args);
                                                        
                                            // Check if values are cached, if not cache them
                                            $risultati_comunicati_transient = THEME_SLUG.'_risultati_comunicati_'.$org.'_1g';
                                            delete_transient($risultati_comunicati_transient);
                                            if(get_transient($risultati_comunicati_transient) === false) {
                                                $search_query = new WP_Query($search_query_args);
                                                //Cache Results
                                                set_transient($risultati_comunicati_transient, $search_query, 24 * HOUR_IN_SECONDS );
                                            }
                                            $search_query = get_transient($risultati_comunicati_transient);
                                    
                                            // Save Query to Temp query for pagination
                                            global $custom_query;
                                            $custom_query = $search_query; 
                                            ?>
                                            <div class="grid-bottom fullwidth">
                                                <div class="row align-items-stretch">

                                                    <?php
                                                        if ($search_query->have_posts()) :  while ( $search_query->have_posts() ) : $search_query->the_post();

                                                            // File
                                                            $upload = get_field('file_upload');
                                                            $file_url = get_field('file_url');
                                                            if($upload != '' && !is_null($upload)) {
                                                                $url = $upload;
                                                            } elseif($file_url != '' && !is_null($file_url)) {
                                                                $url = $file_url;
                                                            } else {
                                                                $url = get_the_permalink(get_the_ID());
                                                            }

                                                            // Event date
                                                            $event_start = get_field('evento_inizio');
                                                            if($event_start != '') {
                                                                $event_start = str_replace('00:00', '', $event_start);
                                                            } else {
                                                                $event_start = get_the_time('j/m/Y');
                                                            }

                                                            // Event Città
                                                            $event_citta = get_field('evento_citta');
                                                            if($event_citta != '') {
                                                                //$citta = ' | '.$event_citta;
                                                                $citta = $event_citta;
                                                            } else {
                                                                $citta = '';
                                                            }
                                                            ?>

                                                            <div class="col-xs-12 col-6">
                                                                <div class="grid-bottom-single risultato fullwidth">

                                                                    <?php if($type == 'comunicato') { ?>

                                                                        <div class="date">
                                                                            <p>
                                                                                <!-- <span class="float-left">
                                                                                    <?php //echo $the_terms_comunicato; ?><?php //if($the_other_terms != '') echo '<em class="banca"> - '.$the_other_terms.'</em>'; ?>
                                                                                </span> -->
                                                                                <span class="float-right"><?php the_time('j/m/Y'); ?></span>
                                                                            </p>
                                                                        </div>
                                                                        <h3><a href="<?php echo $url; ?>"><?php the_title(); ?></a></h3>

                                                                    <?php } elseif($type == 'evento') {
                                                                         // File
                                                                        $upload = get_field('file_upload');
                                                                        $file_url = get_field('file_url');
                                                                        if($upload != '' && !is_null($upload)) {
                                                                            $url = $upload;
                                                                        } elseif($file_url != '' && !is_null($file_url)) {
                                                                            $url = $file_url;
                                                                        } else {
                                                                            $url = '';
                                                                        }   
                                                                        ?>

                                                                        <div class="date">
                                                                            <p><span class="float-left"><?php echo $citta; ?></span><span class="float-right"><?php echo $event_start; ?></span></p>
                                                                        </div>
                                                                        <h3>
                                                                            <?php if($url != '') { ?>
                                                                                <a href="<?php echo $url; ?>"><?php the_title(); ?></a>
                                                                            <?php } else { ?>
                                                                                <?php the_title(); ?>
                                                                            <?php } ?>
                                                                        </h3>

                                                                    <?php } else { // organico ?>

                                                                        <h3><?php the_title(); ?></h3>
                                                                        <p class="ruolo"><?php the_excerpt(); ?></p>
                                                                        <p class="dove"><?php the_content(); ?></p>
                                                                    
                                                                    <?php } ?>

                                                                </div>
                                                            </div>

                                                            <?php
                                                        endwhile;
                                                    
                                                        else:
                                                            echo '<p>Non ci sono contenuti di questo tipo, per il momento.</p>';
                                                        endif;
                                                        ?>

                                                        <?php wp_reset_query(); wp_reset_postdata(); ?>

                                                        </div>
                                                    </div>  
                                        
                                        <?php } else {
                                            // NORMAL STRUTTURA?>
                                            <?php the_content(); ?>
                                        <?php }
                                    ?>

                                </div>

                            <?php

                            } else {

                            // NEWS POST
                            if(has_post_thumbnail() || $video_feat != '') {
                                $col = 6;
                                ?>
                                <!-- Feat Img / Video & split paragraphs / Content -->
                                <div class="col-12 col-md-<?php echo $col;?> col-sm-12 order1 feat_media">
                                    <?php
                                        // Replace feat img with video thumb if content is = ''
                                        if($video_feat != '') {
                                            ?>
                                            <iframe width="640" height="350" src="https://www.youtube-nocookie.com/embed/<?php echo $video_id; ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            <?php } else {
                                        echo wp_imager(700, 380, 1, 'img-about img-fluid', false);
                                        }
                                    ?>
                                </div>
                                            
                                <?php
                                    // First & Second Paragraph
                                    $the_content = wpautop(get_the_content());
                                    $p1 = substr( $the_content, 0, strpos( $the_content, '</p>' ) + 4 );
                                    $the_content = substr( $the_content, strlen($p1));
                                    $p2 = substr( $the_content, 0, strpos( $the_content, '</p>' ) + 4);
                                ?>

                                <div class="col-12 col-md-<?php echo $col;?> col-sm-12 order3 the_content half">
                                    <?php echo $p1; ?>
                                    <?php echo $p2; ?>
                                </div>

                                <?php
                                                // Third paragraph onwards
                                                $the_content = substr( $the_content, strlen($p2));
                                                $allp = substr( $the_content, 0);
                                                $allp = apply_filters('the_content', $allp);
                                ?>

                                <div class="col-12  col-md-12 col-sm-12 order4 the_content"> 
                                                <?php echo $allp; ?>
                                </div>


                            <?php } else {

                                // GENERIC CONTENT
                                $col = 12;
                                ?>
                                <div class="col-12  col-md-12 col-sm-12 order4"> 
                                    <?php the_content(); ?>
                                </div>

                            <?php }
                        

                        } // finished all single types
                        ?>

                    </div>
                </div>

            </div>
        </div>
    </div>

</main>

<?php endwhile; ?>

<?php else : ?>

<p>Questo contenuto non esiste o non è più disponibile.</p>

<?php endif; ?>

<?php get_footer(); ?>